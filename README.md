Smartgrid 421AC
===============

Introduction
------------

According to the Website [www.smartgrid.gov][11], a smartgrid is
> the digital technology that allows for two-way communication between the utility and its customers, and the sensing along the transmission lines is what makes the grid smart. Like the Internet, the Smart Grid will consist of controls, computers, automation, and new technologies and equipment working together, but in this case, these technologies will work with the electrical grid to respond digitally to our quickly changing electric demand.

The benefits associated with the Smart Grid include:

1. More efficient transmission of electricity
2. Quicker restoration of electricity after power disturbances
3. Reduced operations and management costs for utilities, and ultimately lower power costs for consumers
4. Reduced peak demand, which will also help lower electricity rates
5. Increased integration of large-scale renewable energy systems
6. Better integration of customer-owner power generation systems, including renewable energy systems

Smart Grids are a way to address energy efficiency, to bring increased awareness to consumers about the connection between electricity use and the environment.


About this Project
------------------

This project was developed by the Department of Electrical Engineering of the University of Brasilia, where I worked to develop and implement the architecture of SmartGrid. Furthermore, the architecture was presented to the Brazilian Ministry of Science and Technology. This code is compatible with Sibratec's DEM 421 energy meters.

The current repository has two main directories, _Docs_ and _smart_. The _Docs_ directory contains the documentation files (mostly written in Portuguese). The _smart_ directory has four subdirectories and two _SQL_ files. 

The four directories -- _smartgrid_daemon_, _smartgrid_dataserver_, _smartgrid_node_ and _smartgrid-ui_ -- present in the _smart_ directory contains __Smartgrid__ modules. These modules correspond to parts of the system according to the architecture shown in the image below: 

![Architecture](Docs/architecture.jpg)

- ___Node___ (smartgrid_node) is the program/​​module responsible for reading the Sibratec's DEM 421 AC meter (medidor) data and send this data to the _Crawler Daemon_.
- ___Crawler Daemon___ (smartgrid_daemon) is the program/module that communicates with all Nodes through the network, collects the read data of all meters, and save this data into a database.
- ___Dataserver___ (smartgrid_dataserver) is the program/module that reads the data saved in the database by the __Crawler Daemon__ and sends to the user system interface.
- ___User Interface___ (smartgrid-ui) is the Web User Interface (Servidor). The original project was designed to allow different levels of access to the meters data through the Internet.

License
-------

__Smartgrid 421AC__ is released under [GNU GPL version 2][10].

Requirements
------------

The project requires several libraries, technologies and frameworks. However, in most cases, all you need is to have the [Apache Maven][12] (version 3 or above) installed in your machine. The libraries that each modules requires are listed as follow:

- ___Node___ (smartgrid_node)

    * [Restlet][13] Framework,
    * [RxTx][14] serial programming library,
    * Toedter's [JCalendar][15],
    * [Groovy][16] Language,
    * A relational DBMS ([Postgres][17] is preferred) with its respective JDBC driver installed.

- ___Crawler Daemon___ (smartgrid_daemon)

    * [Groovy][16] Language,
    * A relational DBMS ([Postgres][17] is preferred) with its respective JDBC driver installed,
    * [HTTP Builder][18] API.

- ___Dataserver___ (smartgrid_dataserver)

    * [Restlet][13] Framework,
    * [Groovy][16] Language,
    * A relational DBMS ([Postgres][17] is preferred) with its respective JDBC driver installed,
    * [HTTP Builder][18] API.

- ___User Interface___ (smartgrid-ui)

    * To run the web interface you just need [PHP][19] version 5.2.4 or greater
    * [MySQL][20] version 5.0 or greater
    * We recommend [Apache][21] or [Nginx][22] as the most robust and featureful server for running UI, but any server that supports PHP and MySQL will do.


Build and Installation
----------------------

To build the ___Node___, ___Crawler Daemon___ and ___Dataserver___ modules, all you need is go to the module's directory and run Maven. 

> mvn package

When the build process will finished with success, you only have to go the _target_ directory and run the correspondent jar with dependencies. No installation is required.

To install the Web User Interface module, all you have to do is copy the content of _smartgrid-ui_ directory to the server's public directory (_/var/www_ in Apache).

Database Schemes and Recommendations
------------------------------------

The ___Crawler Daemon___  and ___Dataserver___ share the same database. These modules may run on the same computer where the database is installed. If your smart grid network is large, you can install each part in different machines to improve the performance of your system. The scheme of database used by these modules is save in the _smartgrid_central.sql_ file.

The ___Node___ module is able to communicate with 
up to 128 Sibratec's DEM 421 energy meters. However, for reasons of safety, is better to connect only a single computer by node and run this module on these computers. So, for each computer connected on a meter, you will have to install a DBMS. The scheme of database for each node is described in _smartgrid_local.sql_ file.

This code was developed using [IntelliJ Idea 12][23] Community Edition in Windows 7. At developing stage, all modules were running in the same machine with Node's (local) and global databases running in a virtual machine. 

After generating the JAR files, we moved the  ___Crawler Daemon___, ___Dataserver___ and the global database to other computers with Linux (Ubuntu 12.04 LTS) installed. No code change was required and the same _jars_ worked on both Linux and Windows. 

Another change we made was to move the Node's _jar_ to Raspberry Pi with Linux. No change in the _jar_ was necessary, but we had to replace the driver communication for the serial port. 

The serial port driver for Windows, used by RxTx, is distributed with this code (see _smartgrid_node/rxtxSerial.dll_ and _smartgrid_node/rxtxSerial_64bits.dll_). If you want to connect the meter to a machine running Linux, please follow [these instructions][24]. 

The configuration files are simples JSONs and the module's configurations are very intuitive. All you have to do is change the mapped values on these files. For example, on the nodes running Raspberry Pi we used a RS232-to-USB cable and we installed a communication driver on Linux. Then, we just replace the value 'COM5' by '/dev/ttyS0' for the property 'Port' in _smartgrid_node/properties.json_ file.

Contact
-------

Please send all comments, questions, reports and suggestions (especially if you would like to contribute) to **sawp@sawp.com.br**.


Contributing
------------

If you would like to contribute with new algorithms, increment of code 
performance, documentation or another kind of modifications, please contact us.


[10]: http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
[11]: https://www.smartgrid.gov/the_smart_grid
[12]: http://maven.apache.org/
[13]: http://restlet.com/
[14]: http://en.wikibooks.org/wiki/Serial_Programming/Serial_Java
[15]: http://toedter.com/jcalendar/
[16]: http://groovy.codehaus.org/
[17]: http://www.postgresql.org/
[18]: http://groovy.codehaus.org/HTTP+Builder
[19]: http://www.php.net/
[20]: http://www.mysql.com/
[21]: http://httpd.apache.org/
[22]: http://nginx.org/
[23]: http://www.jetbrains.com/idea/
[24]: http://jlog.org/rxtx-lin.html

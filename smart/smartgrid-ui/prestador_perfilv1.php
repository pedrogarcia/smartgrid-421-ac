<?php 
include 'util/util_prestador_escolha_perfil.php';
show_header("Selecionar Perfil - Prestador");
?>
<style type="text/css">
    .selectDisabled{color:#C8C8C8;}
    #map_canvas {width:100%; height:354px; }
    body{margin: 0;}
    .link_button:link, .link_button:visited {
        color: #000000;
        padding:5px;
        background-color: #eeeeee;
        border-top: 1px solid #CCC;
        border-right: 1px solid #666;
        border-bottom: 1px solid #666;
        border-left: 1px solid #CCC;
    }

    .link_button:hover {
        padding:5px;
        border-top: 1px solid #666;
        border-right: 1px solid #CCC;
        border-bottom: 1px solid #CCC;
        border-left: 1px solid #666;
    }
</style>


            <!-- main content -->
            <div id="contentwrapper">
                <div class="main_content">
                    <nav>
                        <div id="jCrumbs" class="breadCrumb module">
                            <ul>
                                <li>
                                    <a href="#"><i class="icon-home"></i></a>
                                </li>
                                <li>
                                    Perfil
                                </li>                               
                            </ul>
                        </div>
                    </nav>                    

                    <div class="row-fluid">                   
                        <div class="span12">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Selecione a área para monitoração </h3>
                            </div>   


                            <div class="row-fluid">
                                <div class="span12 well">
                                    <div class="heading clearfix">
                                        <h3 class="pull-left">Selecione a área desejada </h3>
                                        <div id="display_area" class="pull-right">Área selecionada: 0 km<sup>2</sup></div>
                                    </div>    
                                    <div class="row-fluid">   
                                        <div class="span12">
                                            <a href="javascript:void(0);" onclick="removeLastPoint();" class="btn btn-small  pull-right" style="margin-right:2px;" >Desfazer</a>
                                            <a href="javascript:void(0);" onclick="zoomToPoly();" class="btn btn-small  pull-right" style="margin-right:2px;">Ver área selecionada</a>
                                            <a href="javascript:void(0);" onclick="clearMap();" class="btn btn-small  pull-right" style="margin-right:2px;">Começar novamente</a>    
                                            <a id="selecionar_area" data-toggle="modal"  href="#myTasks" class="btn btn-small btn-gebo pull-right" style="margin-right:2px;">Gerar relatório <i class="splashy-calendar_week"></i></a>


                                            <div>
                                                <form action="#" onsubmit="showAddress(this.address.value, 15); return false">
                                                       &nbsp; Endereço: <input type="text" size="15" id="addressInput" name="address" />
                                                       <input type="submit" class="btn " value="Ir" style="margin-top:-10px" />
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div id="map_canvas" class="span12"></div>
                                    </div>
                            </div>                            
                            <div class="row-fluid">  
                                <div class="span9 well">
                                    <div class="heading clearfix">
                                        <h3 class="pull-left">Selecione a região </h3>
                                    </div>      
                                    <div class="row-fluid">
                                        <div class="row-fluid">
                                            <div class="span4">
                                                <span class="help-block">Selecione a Região</span>
                                                <select id="regiaobrasil" class="span12">
                                                    <option value="" >--</option>                                                
                                                    <option value="Norte" class="selectDisabled">Norte</option>
                                                    <option value="Nordeste" class="selectDisabled">Nordeste</option>
                                                    <option value="Centro-Oeste" selected>Centro-Oeste</option>
                                                    <option value="Sudeste" class="selectDisabled">Sudeste</option>
                                                    <option value="Sul" class="selectDisabled">Sul</option>                                                                                                                                                
                                                </select>
                                            </div>

                                            <div class="span4">
                                                <span class="help-block">Selecione o Estado</span>
                                                <select id="estado" class="span12">
                                                    <option value="0" >--</option>
                                                    <option value="Distrito Federal" selected>Distrito Federal</option>
                                                    <option value="Goiás" class="selectDisabled">Goiás</option>
                                                    <option value="Mato Grosso" class="selectDisabled">Mato Grosso</option>
                                                    <option value="Mato Grosso do Sul" class="selectDisabled">Mato Grosso do Sul</option>                                                                                                                                              
                                                </select>
                                            </div>

                                            <div class="span4">
                                                <span class="help-block">Selecione a cidade</span>
                                                <select id="cidade" class="span12">
                                                    <option value="">--</option>
                                                    <option class="selectDisabled" value="Águas Claras">Águas Claras</option>
                                                    <option value="Brasília" selected>Brasília</option>
                                                    <option class="selectDisabled" value="Brazlândia">Brazlândia</option>
                                                    <option class="selectDisabled" value="Candangolândia">Candangolândia</option>
                                                    <option class="selectDisabled" value="Ceilândia">Ceilândia</option>
                                                    <option class="selectDisabled" value="Cruzeiro">Cruzeiro</option>
                                                    <option class="selectDisabled" value="Gama">Gama</option>
                                                    <option class="selectDisabled" value="Guará">Guará</option>
                                                    <option class="selectDisabled" value="Lago Norte">Lago Norte</option>
                                                    <option class="selectDisabled" value="Lago Sul">Lago Sul</option>
                                                    <option class="selectDisabled" value="Núcleo Bandeirante">Núcleo Bandeirante</option>
                                                    <option class="selectDisabled" value="Octogonal">Octogonal</option>
                                                    <option class="selectDisabled" value="Park Way">Park Way</option>
                                                    <option class="selectDisabled" value="Planaltina">Planaltina</option>
                                                    <option class="selectDisabled" value="Recando das Emas">Recando das Emas</option>
                                                    <option class="selectDisabled" value="Samambaia">Samambaia</option>
                                                    <option class="selectDisabled" value="Santa Maria">Santa Maria</option>
                                                    <option class="selectDisabled" value="Sobradinho">Sobradinho</option>
                                                    <option class="selectDisabled" value="Sudoeste">Sudoeste</option>
                                                    <option class="selectDisabled" value="Taguatinga">Taguatinga</option>

                                                </select> 
                                            </div>     
            

                                            <a id="selecionar_regiao" data-toggle="modal"  href="#myTasks" class="btn btn-small btn-gebo pull-right">Gerar relatório <i class="splashy-calendar_week"></i></a>                          
                                        </div>                                        
                                    </div>   
                                                                                                     
                                </div>
                                <div class="span3 well">
                                    <div class="heading clearfix">
                                        <h3 class="pull-left">Identificador único </h3>
                                    </div>  
                                    <div class="row-fluid">
                                        <div class="row-fluid">
                                            <span class="help-block">Digite o número de inscrição</span>
                                            <input type="text" class="span12" id="num_inscricao">
                                            <a id="selecionar_identificador" data-toggle="modal"  href="#myTasks" class="btn btn-small btn-gebo pull-right">Gerar relatório <i class="splashy-calendar_week"></i></a>                          
                                        </div>
                                    </div>
                                </div>                                
                            </div>  
                            
                        </div>
                    </div>                  
                </div>
            </div>
            <div class="modal hide fade" id="myTasks">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">×</button>
                    <h3>Confirmar seleção da região</h3>
                </div>
                <form method="get" action="informacoesTecnicasAnaliseEnergia.php">
                <div class="modal-body">
                    <div class="row-fluid">
                      <div class="span12 alert alert-info">
                          <strong>Atenção:</strong> os dados que mostraremos a seguir foram gerados em laboratório e extrapolados para aplicações no mundo real. As medições não condizem com a realidade.
                      </div>
                 <!--        <div class="span6">
                            <span class="help-block">Data início</span>
                            <input type="text" class="span12" id="di" name="di">
                        </div>
                        <div class="span6">
                            <span class="help-block">Data fim</span>
                            <input type="text" class="span12" id="df" name="df">
                        </div>  -->
                    <input type="hidden" id="perfil" name="perfil" value="prestador" />
                    <input type="hidden" id="regiao" name="regiao" value="" />   
                    </div> 

                </div>
                <div class="modal-footer">
                  
                    
                    <button type="submit" href="informacoesTecnicasAnaliseEnergia.php" class="btn btn-small btn-success pull-right">Gerar relatório</button>
                  
                </div>
                </form>
            </div>            
            
<?php show_sidebar(); ?>
<?php show_footer(); ?>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>

<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAGCVh1Kric5V_15lxuWQO4RStxadBL4uELOO--kHvwV1fp_evphS2PYymoirCULW6iM0G-wcgw_lGpg&sensor=false" type="text/javascript">
// AIzaSyD7-9hjb6R7UGoifCJwbAltm_m6JF-SEek
</script>


<script>
var map;
var polyShape;
var polygonMode;
var polyPoints = [];
var marker;

var lastClickTime;
var clckTimeOut;

var fillColor = "#F5F5F5"; // blue fill
var lineColor = "#1C87A7"; // black line
var opacity = 0.6;
var lineWeight = 4;
var options = {
showOnLoad : true
};
var geocoder = null;
var addressMarker;
var edit = false;

function load() {
  if (GBrowserIsCompatible()) {
  map = new GMap2(document.getElementById("map_canvas"), {draggableCursor: 'crosshair', draggingCursor: 'pointer'});
  map.addControl(new GSmallMapControl());
  map.addControl(new GMapTypeControl());
  map.setCenter(new GLatLng(-20.632784250388013, -49.21875), 3);
  map.setMapType(G_NORMAL_MAP);
  GEvent.addListener(map, 'click', mapClick);
  geocoder = new GClientGeocoder();

  var publisherID = 'pub-1002932879165162';

  if(1)//if(!searchForLink(parent.document, "mapdevelopers"))
  {
    var adsManagerOptions = {
    maxAdsOnMap : 1,
    style: 'adunit',
    channel: '6214138502',
    position: new GControlPosition(G_ANCHOR_BOTTOM_RIGHT)
    };
    adsManager = new GAdsManager(map, publisherID, adsManagerOptions);
    adsManager.enable();
  }
    }
}

function showAddress(address, zoom) {
  if (geocoder) {
  geocoder.getLatLng(address,
    function(point) {
    if (!point) {
      alert(address + " not found");
    } else {
      if (addressMarker) {
      map.removeOverlay(addressMarker);
      }
      addressMarker = new GMarker(point);
      map.setCenter(point);
      map.setZoom(zoom);
      map.addOverlay(addressMarker);
    }
    }
  );
  }
}

 // mapClick - Handles the event of a user clicking anywhere on the map
// Adds a new point to the map and draws either a new line from the last point
// or a new polygon section depending on the drawing mode.
function singleClick(marker, clickedPoint) {
  window.clearTimeout(clckTimeOut);
  clckTimeOut = null;
  // Push onto polypoints of existing polygon
  if(edit)
  {
    edit = false;
    copyEdits();
  }
  polyPoints.push(clickedPoint);
  drawCoordinates();
 }

function copyEdits()
{
  count = polyShape.getVertexCount();
  polyPoints = [];
  for(var i = 0; i < count; i++)
  {
    polyPoints.push(polyShape.getVertex(i));
  }
  drawCoordinates();
}

 function removeLastPoint()
 {
  polyPoints.pop();
  drawCoordinates();
 }

  // Clear current Map
  function clearMap(){
  map.clearOverlays();
  polyPoints = [];
  document.getElementById("status").innerHTML = "";
  }


  // Toggle from Polygon PolyLine mode

  function toggleDrawMode(){
  map.clearOverlays();
  polyShape = null;
  polygonMode = document.getElementById("drawMode_polygon").checked;
  drawCoordinates();
  }


// drawCoordinates
function drawCoordinates(){
  //Re-create Polyline
  polyShape = new GPolygon(polyPoints,lineColor,lineWeight,opacity,fillColor,opacity,{clickable:false });
  displayArea();
  map.clearOverlays();

  // Grab last point of polyPoints to add marker
  marker = new GMarker(polyPoints[polyPoints.length -1]);
  map.addOverlay(marker);
  map.addOverlay(polyShape);
  GEvent.addListener(polyShape, 'lineupdated', displayArea);
}

function displayArea()
{
  var meters = polyShape.getArea();
  var kilometer = meters/1000000;
  var miles = meters * .000000386102159;
  var acre = meters * 0.000247105381;
  var feet = meters * 10.7639104;
  document.getElementById("display_area").innerHTML = "Área selecionada: ";
  document.getElementById('display_area').innerHTML += kilometer.toFixed(2) + ' km<sup>2</sup>';
}

function editShape()
{
  edit = true;
  polyShape.enableEditing();
}

function zoomToPoly() {
 if (polyShape && polyPoints.length > 0) {
   var bounds = polyShape.getBounds();
   map.setCenter(bounds.getCenter());
   map.setZoom(map.getBoundsZoomLevel(bounds));
 }
}

function mapClick(marker, clickedPoint)
{
  //catches the second click from the map listener when the shape listener fires a click
  var d = new Date();
  var clickTime = d.getTime();
  var clickInterval = clickTime - lastClickTime;
  if(clickInterval<10)
  {
    return 0;
  }
  else lastClickTime=clickTime;

  //stops a single click if there is a double click
  if (clckTimeOut)
  {
      window.clearTimeout(clckTimeOut);
      clckTimeOut = null;
      //doubleclick
  }
  else
  {
      clckTimeOut = window.setTimeout(function(){singleClick(marker, clickedPoint)},500);
  }
 }

function searchForLink(page, link_search)
{
  var links = page.getElementsByTagName("a");
  for(var i = 0; i < links.length; i++)
  {
    var element = links[i]
    var index = element.href.indexOf(link_search);
    if (element.offsetWidth === 0 || element.offsetHeight === 0) index = -1;
    if(index>-1)
    {
      return true;
    }
  }
  return false
}

</script>

<!-- datepicker -->
<script src="lib/datepicker/bootstrap-datepicker.min.js"></script>

<!-- specific JSs -->           
<script src="js/smart/prestador_perfil.js"></script>


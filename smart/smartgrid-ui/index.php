<!DOCTYPE html>
<html lang="en" class="login_page">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Energia Fácil</title>
    
        <!-- Bootstrap framework -->
            <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
            <link rel="stylesheet" href="bootstrap/css/bootstrap-responsive.min.css" />
        <!-- theme color-->
            <link rel="stylesheet" href="css/blue.css" />
        <!-- tooltip -->    
			<link rel="stylesheet" href="lib/qtip2/jquery.qtip.min.css" />
        <!-- main styles -->
            <link rel="stylesheet" href="css/style.css" />
    
        <!-- Favicon -->
            <link rel="shortcut icon" href="favicon.ico" />
    
        <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
    
        <!--[if lte IE 8]>
            <script src="js/ie/html5.js"></script>
			<script src="js/ie/respond.min.js"></script>
        <![endif]-->
		
    </head>
    <body>

            <header>
                <div class="navbar navbar-fixed-top">
                    <div class="navbar-inner">
                        <div class="container-fluid">
                            <a class="brand" href="meuConsumoEnergiaUtilizada.php"><i class="icon-home icon-white"></i> Energia Fácil</a>
                        </div>
                    </div>
            </header>    	

		<div class="login_box">
			
			<form action="meuConsumoEnergiaUtilizada.php" method="post" id="login_form">
				<div class="top_b">Entrar</div>    
				<div class="alert alert-info alert-login">
					Insira seu nome de usuário, senha e escolha o perfil.
				</div>
				<div class="cnt_b">
					<div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-user"></i></span><input type="text" id="username" name="username" placeholder="Usuário" value="" />
						</div>
					</div>
					<div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-lock"></i></span><input type="password" id="password" name="password" placeholder="Senha" value="" />
						</div>
					</div>				
					<div class="formRow clearfix">
						<label class="checkbox"><input type="checkbox" /> Lembre-me</label>

						<a href="meuConsumoEnergiaUtilizada.php?perfil=casa&regiao=" class="btn btn-gebo" type="submit">Casa</a>
						<a href="regulador_perfil.php?perfil=regulador&regiao=" class="btn btn-danger" type="submit">Regulador</a>
						<a href="prestador_perfil.php?perfil=prestador&regiao=" class="btn btn-inverse" type="submit">Prestador</a>
						<a href="informacoesTecnicasAnaliseEnergia_integrado_local.php?perfil=sensor&regiao=" class="btn btn-success" style="margin-top:10px;" type="submit">DEM421AC | Setor 6 | Addrs 90832</a>
					</div>
				</div>

				<div class="btm_b clearfix">
					<!-- <button class="btn btn-inverse pull-right" type="submit">Entrar</button> -->

					<span class="link_reg"><a href="#reg_form">Ainda não está registrado? Registre-se aqui</a></span>
				</div>  
			</form>
			
			<form action="dashboard.php" method="post" id="pass_form" style="display:none">
				<div class="top_b">Não consegue entrar?</div>    
					<div class="alert alert-info alert-login">
					Digite seu e-mail. Você receberá um link para criar uma nova senha através dele.
				</div>
				<div class="cnt_b">
					<div class="formRow clearfix">
						<div class="input-prepend">
							<span class="add-on">@</span><input type="text" placeholder="E-mail" />
						</div>
					</div>
				</div>
				<div class="btm_b tac">
					<button class="btn btn-inverse" type="submit">Solicitar nova senha</button>
				</div>  
			</form>
			
			<form action="dashboard.php" method="post" id="reg_form" style="display:none">
				<div class="top_b">Cadastre-se no Energia Fácil</div>
				<div class="alert alert-login">
					Ao se cadastrar no Energia Fácil, você concordará com nossos <a data-toggle="modal" href="#terms">Termos de Serviço</a>.
				</div>
				<div id="terms" class="modal hide fade" style="display:none">
					<div class="modal-header">
						<a class="close" data-dismiss="modal">×</a>
						<h3>Termos e Condições.</h3>
					</div>
					<div class="modal-body">
						<p>
							&lt;Incluir termos de uso&gt;
						</p>
					</div>
					<div class="modal-footer">
						<a data-dismiss="modal" class="btn" href="#">Fechar</a>
					</div>
				</div>
				<div class="cnt_b">
					
					<div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-user"></i></span><input type="text" placeholder="Usuário" />
						</div>
					</div>
					<div class="formRow">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-lock"></i></span><input type="text" placeholder="Senha" />
						</div>
					</div>
					<div class="formRow">
						<div class="input-prepend">
							<span class="add-on">@</span><input type="text" placeholder="E-mail" />
						</div>
						<small>Seu e-mail está seguro conosco.</small>
					</div>
					 
				</div>
				<div class="btm_b tac">
					<button class="btn btn-inverse" type="submit">Cadastre-me</button>
				</div>  
			</form>
			
			<div class="links_b links_btm clearfix">
				<span class="linkform"><a href="#pass_form">Esqueceu sua senha?</a></span>
				<span class="linkform" style="display:none">Melhor não, <a href="#login_form">voltar para o login</a></span>
			</div>
		</div>
		
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery-migrate.min.js"></script>
        <script src="js/jquery.actual.min.js"></script>
        <script src="lib/validation/jquery.validate.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function(){
                
				//* boxes animation
				form_wrapper = $('.login_box');
				function boxHeight() {
					form_wrapper.animate({ marginTop : ( - ( form_wrapper.height() / 2) - 24) },400);	
				};
				form_wrapper.css({ marginTop : ( - ( form_wrapper.height() / 2) - 24) });
                $('.linkform a,.link_reg a').on('click',function(e){
					var target	= $(this).attr('href'),
						target_height = $(target).actual('height');
					$(form_wrapper).css({
						'height'		: form_wrapper.height()
					});	
					$(form_wrapper.find('form:visible')).fadeOut(400,function(){
						form_wrapper.stop().animate({
                            height	 : target_height,
							marginTop: ( - (target_height/2) - 24)
                        },500,function(){
                            $(target).fadeIn(400);
                            $('.links_btm .linkform').toggle();
							$(form_wrapper).css({
								'height'		: ''
							});	
                        });
					});
					e.preventDefault();
				});
				
				//* validation
				$('#login_form').validate({
					onkeyup: false,
					errorClass: 'error',
					validClass: 'valid',
					rules: {
						username: { required: true, minlength: 3 },
						password: { required: true, minlength: 3 }
					},
					highlight: function(element) {
						$(element).closest('div').addClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					unhighlight: function(element) {
						$(element).closest('div').removeClass("f_error");
						setTimeout(function() {
							boxHeight()
						}, 200)
					},
					errorPlacement: function(error, element) {
						$(element).closest('div').append(error);
					}
				});
            });
        </script>
    </body>
</html>

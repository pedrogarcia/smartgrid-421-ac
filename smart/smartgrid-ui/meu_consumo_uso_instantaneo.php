<?php 
include 'util/util.php';
show_header("Meu consumo");
?>

            <!-- main content -->
            <div id="contentwrapper">
                <div class="main_content">
                    <nav>
                        <div id="jCrumbs" class="breadCrumb module">
                            <ul>
                                <li>
                                    <a href="#"><i class="icon-home"></i></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Meu consumo</a>
                                </li>
                                <li>
                                    Uso instantâneo
                                </li>
                            </ul>
                        </div>
                    </nav>                    

                    <div class="row-fluid">                   
                        <div class="span12">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Indicadores de consumo</h3>
                            </div>                            
                            <ul class="ov_boxes">
                                <li>
                                    <div class="p_bar_up p_canvas">2,4,9,7,12,8,16</div>
                                    <div class="ov_text">
                                        <strong>$3 458,00</strong>
                                        Consumo em reais
                                    </div>
                                </li>
                                <li>
                                    <div class="p_bar_down p_canvas">20,15,18,14,10,13,9,7</div>
                                    <div class="ov_text">
                                        <strong>$43 567,43</strong>
                                        Consumo em kWh
                                    </div>
                                </li>
                                <li>
                                    <div class="p_line_up p_canvas">3,5,9,7,12,8,16</div>
                                    <div class="ov_text">
                                        <strong>2304</strong>
                                        Pegadas de carbono
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Potência consumida</h3>
                            </div>
                            <div id="fl_1" style="height:300px;width:100%;margin:15px auto 0"></div>   
                        </div>     
                    </div>                    
                </div>
            </div>
            
<?php show_sidebar(); ?>
<?php show_footer(true, false); ?>

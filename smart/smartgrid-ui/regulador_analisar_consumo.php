<?php 
include 'util/util_regulador.php';
show_header("Energia Utilizada - Meu consumo");
?>

            <!-- main content -->
            <div id="contentwrapper">
                <div class="main_content">
                    <nav>
                        <div id="jCrumbs" class="breadCrumb module">
                            <ul>
                                <li>
                                    <a href="#"><i class="icon-home"></i></a>
                                </li>
                                <li>
                                    <a href="meuConsumoEnergiaUtilizada.php">Meu consumo</a>
                                </li>
                                <li>
                                    Energia Utilizada
                                </li>
                                <?php
                                 if ($tipo_regiao != "") {echo "<li><strong>Região: </strong>" . $tipo_regiao . "</li>";}
                                ?>                                
                            </ul>
                        </div>
                    </nav>                    

                    <div class="row-fluid">                   
                        <div class="span12">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Consumo em tempo real</h3>
                            </div>                            
                            <div class="row-fluid">  
                                <div class="span7">
                                    <div id="leitura_tempo_real" style="min-width: 100%; height: 200px; margin: 0 auto;"></div>
                                </div>
                                <div class="span5">
                                    <ul class="ov_boxes" style="float:right;">
                                        <li>
                                            <div class="p_line_up p_canvas ttip_t" title="Energia consumida nos últimos 7 dias">2,4,9,7,12,8,16</div>
                                            <div class="ov_text">
                                                <strong><span id="potencia_consumida"></span> W</strong>
                                                Consumo total
                                            </div>
                                        </li>
                                        <li>
                                            <div class="p_bar_down p_canvas ttip_t" title="Energia comprada nos últimos 7 dias">20,15,18,14,10,13,9,7</div>
                                            <div class="ov_text">
                                                <strong><span id="potencia_comprada"></span> W</strong>
                                                Potência comprada
                                            </div>
                                        </li>
                                        <li>
                                            <div class="p_bar_up p_canvas ttip_t" title="Energia gerada nos últimos 7 dias">3,5,9,7,12,8,16</div>
                                            <div class="ov_text">
                                                <strong><span id="potencia_gerada"></span> W</strong>
                                                Potência gerada
                                            </div>
                                        </li>
                                    </ul>
                                </div>                                
                            </div> 
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Histórico comparado de energia <strong><span class="ttip_t" title="A energia 'utilizada' é a soma da energia 'comprada' da rede elétrica, mais a energia 'gerada' por fontes particulares.">utilizada</span></strong></h3>
                            </div>
                            <div class="row-fluid">  
                                <div class="span6">
                                    <ul class="ov_boxes">
                                        <li>
                                            <div class="p_bar_anterior_azul p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">20,25,34</div>
                                            <div class="ov_text ov_text_120">
                                                <strong>R$ 16,06</strong>
                                                Consumo Jan-2013
                                            </div>
                                        </li>
                                        <li>
                                            <div class="p_bar_atual_azul p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">25,34,12</div>
                                            <div class="ov_text ov_text_120">
                                                <strong>R$ <span class="mes_atual_reais"></span></strong>
                                                Parcial Fev-2013
                                            </div>
                                        </li>
                                    </ul>                                    
                                </div>   
                                <div class="span6">
                                    <ul class="ov_boxes">
                                        <li>
                                            <div class="p_bar_anterior_azul p_canvas ttip_t" title="Energia consumida nos últimos 3 meses">210,200,230</div>
                                            <div class="ov_text ov_text_120">
                                                <strong>106.43 kWh</strong>
                                                Consumo Jan-2013
                                            </div>
                                        </li>
                                        <li>
                                            <div class="p_bar_atual_azul p_canvas ttip_t" title="Energia consumida nos últimos 3 meses">200,230,130</div>
                                            <div class="ov_text ov_text_120">
                                                <strong><span class="mes_atual_kwh"></span> kWh</strong>
                                                Parcial Fev-2013
                                            </div>
                                        </li>
                                    </ul> 
                                </div>                                                            
                            </div> 
                            <div class="row-fluid">  
                                <div class="span12">
                                    <div id="mes_anterior_mes_atual" style="min-width: 100%; height:200px; margin: 0 auto;"></div>
                                </div>                              
                            </div>                             
                        </div>     
                    </div>                    
                </div>
            </div>
            
<?php show_sidebar(); ?>
<?php show_footer(); ?>

<!-- specific JSs -->   
<script src="lib/highcharts/highcharts.js"></script>
<script src="lib/highcharts/modules/exporting.js"></script>          
<script src="js/smart/meuConsumoEnergiaUtilizada.js"></script>


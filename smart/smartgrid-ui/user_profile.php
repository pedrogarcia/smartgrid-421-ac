<?php 
include 'util/util.php';
show_header("Meu consumo");
?>

            
            <!-- main content -->
            <div id="contentwrapper">
                <div class="main_content">
                
                    <nav>
                        <div id="jCrumbs" class="breadCrumb module">
                            <ul>
                                <li>
                                    <a href="#"><i class="icon-home"></i> </a>
                                </li>                                            
                                <li>
                                     Editar Perfil
                                </li>
                                <?php
                                 if ($tipo_regiao != "") {echo "<li><strong>Região: </strong>" . $tipo_regiao . "</li>";}
                                ?>                                
                            </ul>
                        </div>
                    </nav>
                
                    
                    <div class="row-fluid">
						<div class="span12">
							<h3 class="heading">Perfil de <a data-original-title="Insira o nome" data-pk="1" data-type="text" id="username" href="#">Leandro Rezende</a></h3>
							<div class="row-fluid">
								<div class="span8">
									<form class="form-horizontal">
										<fieldset>
											<div class="control-group formSep">
												<label class="control-label">Usuário</label>
												<div class="controls text_line">
													<strong><a data-original-title="Enter your firstname" data-placeholder="Required" data-pk="1" data-type="text" id="firstname" href="#">leandro.rezende</a></strong>
												</div>
											</div>
                                            <div class="control-group formSep">
                                                <label class="control-label">Código</label>
                                                <div class="controls text_line">
                                                    <strong><a data-original-title="Código da conta de Energia Elétrica" data-placeholder="Required"  data-pk="1" data-type="text" id="codigo" href="#">942348-4</a></strong>
                                                </div>
                                            </div>    
                                            <div class="control-group formSep">
                                                <label class="control-label">Concessionária</label>
                                                <div class="controls text_line">
                                                    <strong><a data-original-title="Selecione sua Concessionária" data-value="" data-pk="1" data-type="select" id="sex" href="#"></a></strong>
                                                </div>
                                            </div>                                                                                      
											<div class="control-group formSep">
												<label class="control-label">Endereço</label>
												<div class="controls text_line">
													<strong><a data-original-title="Preencha seu endereço" data-pk="1" data-type="address" id="address" href="#"></a></strong>
												</div>
											</div>                                            
											<div class="control-group formSep">
												<label for="fileinput" class="control-label">Imagem do perfil</label>
												<div class="controls">
													<div data-provides="fileupload" class="fileupload fileupload-new">
														<input type="hidden" />
														<div style="width: 80px; height: 80px;" class="fileupload-new thumbnail"><img src="img/users/leandroIntranet.jpg" alt="" /></div>
														<div style="width: 80px; height: 80px; line-height: 80px;" class="fileupload-preview fileupload-exists thumbnail"></div>
														<span class="btn btn-file"><span class="fileupload-new">Selecionar imagem</span><span class="fileupload-exists">Change</span><input type="file" id="fileinput" name="fileinput" /></span>
														<a data-dismiss="fileupload" class="btn fileupload-exists" href="#">Remover</a>
													</div>	
												</div>
											</div>
                                           
											<div class="control-group formSep">
												<label for="u_password" class="control-label">Senha</label>
												<div class="controls">
													<div class="sepH_b">
														<input type="password" id="u_password" class="input-xlarge" value="my_password" />
														<span class="help-block">Insira sua senha</span>
													</div>
													<input type="password" id="s_password_re" class="input-xlarge" />
													<span class="help-block">Repita a senha</span>
												</div>
											</div>
										
											<div class="control-group">
												<div class="controls">
													<button class="btn btn-gebo" type="submit">Salvar alterações</button>
												<button class="btn">Cancelar</button>
												</div>
											</div>
										</fieldset>
									</form>
								</div>
							</div>
						</div>
					</div>
                        
                </div>
            </div>
           
            
<?php show_sidebar(); ?>
<?php show_footer(); ?>


            <!-- specific JSs -->

            <!-- Mockjax -->
            <script src="js/jquery.mockjax.js"></script>
            <!-- moment.js date library -->
            <script src="lib/moment_js/moment.min.js"></script>
            <!-- x-editable -->
            <script src="lib/x-editable/bootstrap-editable/js/bootstrap-editable.min.js"></script>
            <script src="lib/x-editable/inputs-ext/address/address.js"></script>
            
            <!-- editable elements functions -->
            <script src="js/gebo_editable_elements.js"></script>	
<?php 
include 'util/util.php';
show_header("Relatório Comparativo - Regulador");
?>
<!-- nice form elements -->
<link rel="stylesheet" href="lib/uniform/Aristo/uniform.aristo.css" />

            <!-- main content -->
            <div id="contentwrapper">
                <div class="main_content">
                    <nav>
                        <div id="jCrumbs" class="breadCrumb module">
                            <ul>
                                <li>
                                    <a href="#"><i class="icon-home"></i></a>
                                </li>
                                <li>
                                    <a href="meuConsumoEnergiaUtilizada.php">Regulador</a>
                                </li>
                                <li>
                                    Relatório Comparativo
                                </li>
                                <?php
                                 if ($tipo_regiao != "") {echo "<li><strong>Região: </strong>" . $tipo_regiao . "</li>";}
                                ?>                                
                            </ul>
                        </div>
                    </nav>                    

                    <div class="row-fluid" id="caixaPesquisa">                   
                        <div class="span12">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Relatório comparativo de consumo de energia por região</h3>

                            </div>     
                            <form >                       
                                <div class="row-fluid">  
                                    <div class="span12 well">
                                        <div>
                                            <p class="f_legend">Selecione o período do relatório</p>
                                            <div class="row-fluid">
                                                <div class="span6">
                                                    <span class="help-block">Data início</span>
                                                    <input type="text" class="span12" id="prdi">
                                                </div>
                                                <div class="span6">
                                                    <span class="help-block">Data fim</span>
                                                    <input type="text" class="span12" id="prdf">
                                                </div>       
                                            </div>                                         
                                             <div class="row-fluid"> 
                                                <div class="span10 form-inline">
                                                    <span class="help-block">Selecione as regiões que deseja comparar</span>
                                                        <label class="uni-radio">
                                                            <input type="checkbox" value="Centro-Oeste" id="uni_co" name="uni_co" class="uni_style" checked="" />
                                                            Centro-Oeste
                                                        </label>
                                                        <label class="uni-radio">
                                                            <input type="checkbox" value="Nordeste"  id="uni_ne"  name="uni_ne" class="uni_style" checked="" />
                                                            Nordeste
                                                        </label>
                                                        <label class="uni-radio">
                                                            <input type="checkbox" value="Norte"  id="uni_no"  name="uni_no" class="uni_style" checked="" />
                                                            Norte
                                                        </label>
                                                        <label class="uni-radio">
                                                            <input type="checkbox" value="Sudeste"  id="uni_se"  name="uni_se" class="uni_style" checked="" />
                                                            Sudeste
                                                        </label>                 
                                                        <label class="uni-radio">
                                                            <input type="checkbox" value="Sul"  id="uni_su"  name="uni_su" class="uni_style" checked="" />
                                                            Sul
                                                        </label>                                                                                                 
                                                </div>                                                                                                 
                                            <!-- </div> -->                                                
                                                <div class="span2">
                                                    <span class="help-block"></span>
                                                        <button  id="processarImpactoConsumoComparado" class="btn btn-gebo"  style="float:right; margin-top:18px;" type="submit">Comparar regiões</button>
                                                </div>                                                                                                                                                        
                                            </div>
                                        </div>                                        
                                    </div>                           
                                </div>
                            </form>
                        </div>                       
                    </div>
                    <div class="row-fluid" id="exibirImpactoConsumoComparado" style="visibility:hidden; overflow:hidden; height:10px;">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="heading clearfix">
                                    <h3 class="pull-left">Energia consumida </h3>
                                    <button  class="btn btn-gebo btn-small processarFatura pull-right exibirCaixaPesquisa"  style="float:right;" type="submit">Verificar novo período</button>
                                </div>                            
                                <div id="comparativo_regioes"></div>
                            </div>
                        </div>
                             
                        <div class="row-fluid">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Relatório geral do período 01/03/2013 a 15/03/2013</h3>
                                
                            </div>                          
                            <table class="table table-striped table-bordered table-condensed" >
                                <thead>
                                    <tr>
                                        <th>Região</th>                                            
                                        <th>Tensão Méd. (Hz)</th>                                               
                                        <th>Frequência  Méd. (Hz)</th>  
                                        <th>Energia Gerada (kWh)</th>         
                                        <th>Energia Consumida (kWh)</th>  
                                        <th>Potência Ativa Méd (kWh)</th>       
                                        <th>Potência Reativa Méd (kWhr)</th>
                                        <th>Fator de potência</th>                                                                
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td>Centro-oeste</td><td>225</td><td>59</td><td>34,5</td> <td>3463,5</td> <td>3463,5</td> <td>34,5</td> <td>0,98</td></tr>
                                    <tr><td>Nordeste    </td><td>220</td><td>56</td><td>33,8</td> <td>3385,8</td> <td>3385,8</td> <td>23,8</td> <td>0,78</td></tr>
                                    <tr><td>Norte       </td><td>216</td><td>59</td><td>33,64</td><td>3322,64</td><td>3322,64</td><td>24,64</td><td>0,68</td></tr>
                                    <tr><td>Sudeste     </td><td>218</td><td>65</td><td>33,72</td><td>3353,72</td><td>3353,72</td><td>53,72</td><td>0,88</td></tr>
                                    <tr><td>Sul         </td><td>218</td><td>57</td><td>35,72</td><td>3355,72</td><td>3355,72</td><td>64,72</td><td>0,98</td></tr>
                                    
                                </tbody>
                            </table>

                        </div>  
                    </div>  
                </div>
            </div>
            
<?php show_sidebar(); ?>
<?php show_footer(); ?>
<!-- specific JSs -->   
<script src="lib/highcharts/highcharts.js"></script>
<script src="lib/highcharts/modules/exporting.js"></script>        
<!-- styled form elements -->
<script src="lib/uniform/jquery.uniform.min.js"></script>
<!-- datepicker -->
<script src="lib/datepicker/bootstrap-datepicker.min.js"></script>
<!-- specific JSs -->             
<script src="js/smart/regulador_relatorio_comparativo.js"></script>

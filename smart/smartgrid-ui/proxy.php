<?php
  $host = $_GET["host"];
  $meteraddress = $_GET["meteraddress"];
  $limit = $_GET["limit"];

  $url = "http://164.41.10.22:8081/meterinfo?host=" . $host . "&meteraddress=" . $meteraddress . "&limit=" . $limit;
  $opts = array(
    'http'=>array(
      'timeout' => 1
    )
  );
  $context = stream_context_create($opts);
  $json = file_get_contents($url, false, $context);
  $data = json_decode($json, TRUE);
  print json_encode($data);
?>

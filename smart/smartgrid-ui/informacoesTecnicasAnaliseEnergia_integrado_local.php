<?php 
include 'util/util.php';
show_header(" Análise de energia - Informações Técnicas");
?>

            <!-- main content -->
            <div id="contentwrapper">
                <div class="main_content">
                    <nav>
                        <div id="jCrumbs" class="breadCrumb module">
                            <ul>
                                <li>
                                    <a href="#"><i class="icon-home"></i></a>
                                </li>
                                <li>
                                    <a href="informacoesTecnicasAnaliseEnergia..php">Informações Técnicas</a>
                                </li>
                                <li>
                                    Análise de Energia
                                </li>
                                <?php
                                 if ($tipo_regiao != "") {echo "<li><strong>Região: </strong>" . $tipo_regiao . "</li>";}
                                ?>
                            </ul>

                        </div>

                    </nav>                    

                    <div class="row-fluid" id="caixaPesquisa">                   
                        <div class="span12">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Informações técnicas</h3>
                            </div>     


                            <form >                       
                                <div class="row-fluid">  
                                    <div class="span12 well">
                                        <div>
                                            <p class="f_legend">Período extraído do medidor Sibratec DEM421AC Setor 6 Addrs 90832</p>
                                            <div class="row-fluid">
                                                <div class="span2">
                                                    <span class="help-block">Data início</span>
                                                    <input type="text" class="span12" id="di" value="10/04/2013 13:46:27" disabled>
                                                </div>
                                                <div class="span2">
                                                    <span class="help-block">Data fim</span>
                                                    <input type="text" class="span12" id="df" value='10/04/2013 14:14:41' disabled>
                                                </div>                                                
                                                <div class="span2 form-inline">
                                                    <span class="help-block">Selecione a fase</span>
                                                        <select id="tipoGranularidade" class="span12" disabled>
                                                            <option value="0" selected>Todas</option>
                                                            <option value="1"selected>Fase R</option>
                                                            <option value="2">Fase S</option>
                                                            <option value="3">Fase T</option>
                                                        </select>
                                                </div> 
                                            <!-- <div class="row-fluid"> -->
                                                <div class="span4 form-inline">
                                                   <!--  <span class="help-block">Tipo de energia avaliada</span>
                                                        <label class="uni-radio">
                                                            <input type="checkbox" value="Consumido" id="uni_consumida" name="uni_consumida" class="uni_style" checked="" />
                                                            Consumo
                                                        </label>
                                                        <label class="uni-radio">
                                                            <input type="checkbox" value="Comprado"  id="uni_comprada"  name="uni_comprada" class="uni_style" checked="" />
                                                            Compra
                                                        </label>
                                                        <label class="uni-radio">
                                                            <input type="checkbox" value="Gerado"  id="uni_gerada"  name="uni_gerada" class="uni_style" checked="" />
                                                            Geração
                                                        </label> -->
                                                </div>                                                                                                  
                                            <!-- </div> -->                                                
                                                <div class="span2">
                                                    <span class="help-block"></span>
                                                        <button  class="btn btn-gebo processarRelatorio"  style="float:right; margin-top:18px;">Gerar relatório</button>
                                                </div>                                                                                                                                                        
                                            </div>
                                        </div>                                        
                                    </div>                           
                                </div>
                            </form>
                        </div>                       
                    </div>

                    <div class="area_graficos" style="height:10px; visibility:hidden">

                        <div class="row-fluid">
                            <div class="span12">
                                <div class="heading clearfix">
                                    <h3 class="pull-left">Histórico de Tensão e Frequência na Fase R <strong></strong></h3>
                                    <button  class="btn btn-gebo btn-small processarTabela pull-right"  style="float:right;" >Visualizar tabela detalhada</button>
                                    <span class="pull-right label label-inverse">Período <span class="prdi">10/04/13</span><span class="pcdi"></span> - <span class="pcdf"><span class="prdf">10/04/13</span>
                                </span></span>
                                </div>
                                <div class="row-fluid"> 
                                    <div class="span6">
                                        <ul class="ov_boxes">
                                            <li>
                                                <div class="p_bar_atual_azul p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">25,34,12</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong> <span class="freq_media_sensor"></span> Hz</strong>
                                                    Frequência média
                                                </div>
                                            </li>                                           
                                            <li>
                                                <div class="p_bar_atual_vermelho p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">20,25,34</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong><span class="freq_maior_sensor"></span> Hz</strong>
                                                    Frequência máxima
                                                </div>
                                            </li>
                                            <li>
                                                <div class="p_bar_anterior_vermelho p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">25,34,12</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong><span class="freq_menor_sensor"></span> Hz</strong>
                                                    Frequência mínima
                                                </div>
                                            </li>                                                                                                                     
                                        </ul>
                                    </div> 
                                    <div class="span6">
                                        <ul class="ov_boxes">
    <!--                                         <li>
                                                <div class="p_bar_anterior_azul p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">20,25,34</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong>5</strong>
                                                    Nª interrupções
                                                </div>
                                            </li>
                                            <li>
                                                <div class="p_bar_atual_azul p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">25,34,12</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong> 26 minutos</strong>
                                                    Tempo interrompido
                                                </div>
                                            </li> -->
                                            <li>
                                                <div class="p_bar_anterior_azul p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">20,25,34</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong><span class="tensao_media_sensor_pu"></span>pu (<span class="tensao_media_sensor"></span> V)</strong>
                                                    Tensão média
                                                </div>
                                            </li>
                                            <li>
                                                <div class="p_bar_atual_vermelho p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">20,25,34</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong><span class="tensao_media_sensor_pu"></span>pu (<span class="tensao_maior_sensor"></span> V)</strong>
                                                    Tensão Máxima
                                                </div>
                                            </li>
                                            <li>
                                                <div class="p_bar_anterior_vermelho p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">25,34,12</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong> <span class="tensao_media_sensor_pu"></span>pu (<span class="tensao_menor_sensor"></span> V)</strong>
                                                    Tensão Mínima
                                                </div>
                                            </li>   
                                        </ul>
                                                                          
                                    </div>   
                                                                                         
                                </div> 

                               <div class="row-fluid">  
                                    <div class="span12">
                                        <div id="tensao_frequencia_dual" style="min-width: 100%; height:300px; margin: 0 auto; background:gray"></div>
                                    </div> 
                                                
                                </div>                                                      
                            </div>     
                        </div>




                        <div class="row-fluid">                   
                            <div class="span12">
                                <div class="heading clearfix">
                                    <h3 class="pull-left">Fator de potêcia e histórico de potência ativa e reativa FASE A</h3>
                                    <button  class="btn btn-gebo btn-small processarTabela pull-right"  style="float:right;" >Visualizar tabela detalhada</button>
                                    <span class="pull-right label label-inverse">Período <span class="prdi">01/03/13</span><span class="pcdi"></span> - <span class="pcdf"><span class="prdf">14/03/13</span>
                                </span></span>                            
                                </div>            
                                <div class="row-fluid">
                                    <div class="span7">                                  
                                        <div id="fator_potencia" style="min-width: 100%; height:300px; margin: 0 auto; background:gray"></div>
                                    </div>                                  
                                    <div class="span5">
                                        <ul class="ov_boxes" >
                                            <li>
                                                <div class="p_line_up p_canvas ttip_t" title="Energia consumida nos últimos 7 dias">2,4,9,7,12,8,16</div>
                                                <div class="ov_text">
                                                    <strong><span class="pot_ativa_consumida_sensor"></span> W</strong>
                                                    Potência ativa média
                                                </div>
                                            </li>
                                            <li>
                                                <div class="p_line_down p_canvas ttip_t" title="Energia gerada nos últimos 7 dias">3,5,9,7,12,8,16</div>
                                                <div class="ov_text">
                                                    <strong><span class="pot_reativa_consumida_sensor"></span> W</strong>
                                                    Potência reativa média
                                                </div>
                                            </li>      
                                            <li>
                                                <div class="p_bar_down p_canvas ttip_t" title="Energia gerada nos últimos 7 dias">3,5,9,7,12,8,16</div>
                                                <div class="ov_text">
                                                    <strong><span class="fator_potencia_medio_sensor"></span></strong>
                                                    Fator de potência médio
                                                </div>
                                            </li>                                                                              
                                        </ul>
                                        
                                    </div>                              
                                </div>

                                <div class="row-fluid">                                    
                                    <div class="span12">
                                        <div class="heading clearfix">
                                            <h3 class="pull-left">Potências Ativas e Reativas 10/04/2013</h3>
                                        </div>                                     
                                        <div id="potencia_ativa" style="min-width: 100%; height:350px; margin: 0 auto; background:gray"></div>
                                    </div>                                                                
                                </div> 

                                               
                                
                            </div>
                        </div>
                    </div>

                    <div class="area_tabela" style="visibility:hidden; height:10px;">


                        <div class="row-fluid">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Relatório geral do período</h3>
                                <button  class="btn btn-gebo btn-small processarFatura pull-right processarConsulta"  style="float:right;" type="submit">Verificar novo período</button>
                            </div>                          
                            <table  class="table table-striped table-bordered table-condensed" >
                                <thead>
                                    <tr>
                                        <th>Data</th>                                            
                                        <th>Hora</th>
                                        <th>Tensão</th>                                              
                                        <th>Frequência</th>                                            
                                        <th>Potência Ativa</th>         
                                        <th>Potência Reativa</th>
                                        <th>Fator de potência</th>                                                                
                                    </tr>
                                </thead>
                                <tbody id="tabelaGeral">                                  

                                </tbody>
                            </table>

                        </div>                 
                    </div>
                           
                </div>
            </div>
            
<?php show_sidebar(); ?>
<?php show_footer(); ?>

<!-- datepicker -->
<script src="lib/datepicker/bootstrap-datepicker.min.js"></script>

<!-- specific JSs -->   
<script src="lib/highcharts/highcharts.js"></script>
<script src="lib/highcharts/highcharts-more.js"></script>
<script src="lib/highcharts/modules/exporting.js"></script>          
<script src="js/smart/informacoesTecnicasAnaliseEnergia_integrado_local.js"></script>
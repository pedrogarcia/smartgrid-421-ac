<?php 
include 'util/util.php';
show_header(" Situações de Risco - Informações Técnicas");
?>

<style>

#map {
  width:100%;
  height: 400px;
}
#year {
}

</style>





            <!-- main content -->
            <div id="contentwrapper">
                <div class="main_content">
                    <nav>
                        <div id="jCrumbs" class="breadCrumb module">
                            <ul>
                                <li>
                                    <a href="#"><i class="icon-home"></i></a>
                                </li>
                                <li>
                                    <a href="informacoesTecnicasAnaliseEnergia..php">Informações Técnicas</a>
                                </li>
                                <li>
                                    Análise de Energia
                                </li>
                                <?php
                                 if ($tipo_regiao != "") {echo "<li><strong>Região: </strong>" . $tipo_regiao . "</li>";}
                                ?>                                
                            </ul>
                       
                        </div>
                    </nav>                    

                    <div class="row-fluid" id="caixaPesquisa">                   
                        <div class="span12">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Informações técnicas</h3>
                            </div>     
                            <form >                       
                                <div class="row-fluid">  
                                    <div class="span12 well">
                                        <div>
                                            <p class="f_legend">Selecione o período para análise</p>
                                            <div class="row-fluid">
                                                <div class="span2">
                                                    <span class="help-block">Data início</span>
                                                    <input type="text" class="span12" id="di">
                                                </div>
                                                <div class="span2">
                                                    <span class="help-block">Data fim</span>
                                                    <input type="text" class="span12" id="df">
                                                </div>                                                
                                            <!-- <div class="row-fluid"> -->
                                                <div class="span6 form-inline">
                                                   <!--  <span class="help-block">Tipo de energia avaliada</span>
                                                        <label class="uni-radio">
                                                            <input type="checkbox" value="Consumido" id="uni_consumida" name="uni_consumida" class="uni_style" checked="" />
                                                            Consumo
                                                        </label>
                                                        <label class="uni-radio">
                                                            <input type="checkbox" value="Comprado"  id="uni_comprada"  name="uni_comprada" class="uni_style" checked="" />
                                                            Compra
                                                        </label>
                                                        <label class="uni-radio">
                                                            <input type="checkbox" value="Gerado"  id="uni_gerada"  name="uni_gerada" class="uni_style" checked="" />
                                                            Geração
                                                        </label> -->
                                                </div>                                                                                                  
                                            <!-- </div> -->                                                
                                                <div class="span2">
                                                    <span class="help-block"></span>
                                                        <a href="javascript:void(0);"  class="btn btn-gebo processarRelatorio"  style="float:right; margin-top:18px;">Gerar relatório</a>
                                                </div>                                                                                                                                                        
                                            </div>
                                        </div>                                        
                                    </div>                           
                                </div>
                            </form>
                        </div>                       
                    </div>

                    <?php
                     if ($tipo_cabecalho == "casa") {
                    ?>   

                    <div class="area_tabela">

                        <div class="row-fluid">
                                   <div class="span12">
                                        <ul class="ov_boxes">
                                             <li>
                                                <div class="p_bar_anterior_azul p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">20,25,34</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong>5</strong>
                                                    Nª interrupções
                                                </div>
                                            </li>
                                            <li>
                                                <div class="p_bar_atual_azul p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">25,34,12</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong> 26 minutos</strong>
                                                    Tempo interrompido
                                                </div>
                                            </li> 
                                            <li>
                                                <div class="p_bar_atual_vermelho p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">25,34,12</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong> 201,2 a 223,9</strong>
                                                    Var. de Tensão (V)
                                                </div>
                                            </li> 
                                            <li>
                                                <div class="p_bar_atual_vermelho p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">25,34,12</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong> 56 a 64</strong>
                                                    Var. de Freq. (Hz)
                                                </div>
                                            </li>                                                                                         
 
                                        </ul>
                                                                          
                                    </div>   
                        </div>
                        <?php
                         if ($tipo_cabecalho != "casa") {
                        ?>   
                            <div class="row-fluid">
                                <div class="heading clearfix">
                                    <h3 class="pull-left">Áreas afetadas por instabilidades de tensão, frequência ou fator de potência</h3>
                                    <span class="pull-right label label-inverse">Período <span class="prdi">01/03/13</span><span class="pcdi"></span> - <span class="pcdf"><span class="prdf">14/03/13</span>
                                </div>  
                                <div class="span11">
                                  <div id="msg" class="alert alert">Passe o mouse sobre o mapa para ver o progresso das instabilidades.</div>
                                  <div id="year" class="alert alert-info">1962</div>
                                  <div id="map" onmouseover="start()"></div>
                                  
                                  <div id="lightbox" ></div>
                                  
                              </div>
                            </div>  
                        <?php }?>
                        <div class="row-fluid">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Interrupções de fornecimento de energia elétrica</h3>
                                <span class="pull-right label label-inverse">Período <span class="prdi">01/03/13</span><span class="pcdi"></span> - <span class="pcdf"><span class="prdf">14/03/13</span>
                            </div>                          
                            <table class="table table-striped table-bordered table-condensed" >
                                <thead>
                                    <tr>
                                                                                
                                        <th>Data início da interrupção</th>                                            
                                        <th>Data fim da interrupção</th>
                                        <th>Tempo total interrompido</th>                                                                                                          
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td>01/01/2013 00:00</td><td>01/01/2013 00:10</td><td>10 minutos</td></tr>
                                    <tr><td>02/01/2013 00:01</td><td>02/01/2013 00:20</td><td>19 minutos</td></tr>
                                    <tr><td>03/01/2013 00:02</td><td>03/01/2013 00:30</td><td>28 minutos</td></tr>
                                    <tr><td>04/01/2013 00:03</td><td>04/01/2013 00:40</td><td>37 minutos</td></tr>
                                    <tr><td>05/01/2013 00:04</td><td>05/01/2013 00:50</td><td>46 minutos</td></tr>
                                    <tr><td colspan="2">Total</td><td>2 horas e 20 minutos</td></tr>
                                </tbody>
                            </table>

                        </div>    
                        <div class="row-fluid">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Tensão de risco</h3>
                                <span class="pull-right label label-inverse">Período <span class="prdi">01/03/13</span><span class="pcdi"></span> - <span class="pcdf"><span class="prdf">14/03/13</span>
                            </div>                          
                            <table class="table table-striped table-bordered table-condensed" >
                                <thead>
                                    <tr>
                                         
                                        <th>Data início da tensão de risco</th>                                            
                                        <th>Data fim da tensão de risco</th>
                                        <th>Tempo total da tensão de risco</th>   
                                        <th>Oscilação de risco</th>                                                                                                         
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td>01/01/2013 00:00</td><td>01/01/2013 00:10</td><td>10 minutos</td><td>230 a 260 V</td></tr>
                                    <tr><td>02/01/2013 00:01</td><td>02/01/2013 00:20</td><td>19 minutos</td><td>230 a 280 V</td></tr>
                                    <tr><td>03/01/2013 00:02</td><td>03/01/2013 00:30</td><td>28 minutos</td><td>250 a 260 V</td></tr>
                                    <tr><td>04/01/2013 00:03</td><td>04/01/2013 00:40</td><td>37 minutos</td><td>270 a 280 V</td></tr>
                                    <tr><td>05/01/2013 00:04</td><td>05/01/2013 00:50</td><td>46 minutos</td><td>110 a 140 V</td></tr>
                                    <tr><td colspan="2">Tempo total de risco</td><td colspan="2">2 horas e 20 minutos</td></tr>
                                </tbody>
                            </table>

                        </div> 
                        <div class="row-fluid">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Frequência de risco</h3>
                                <span class="pull-right label label-inverse">Período <span class="prdi">01/03/13</span><span class="pcdi"></span> - <span class="pcdf"><span class="prdf">14/03/13</span>
                            </div>                          
                            <table class="table table-striped table-bordered table-condensed" >
                                <thead>
                                    <tr>
                                        
                                        <th>Data início da frequência de risco</th>                                            
                                        <th>Data fim da frequência de risco</th>
                                        <th>Tempo total da frequência de risco</th>   
                                        <th>Oscilação de risco</th>                                                                                                         
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td>01/01/2013 00:00</td><td>01/01/2013 00:10</td><td>10 minutos</td><td>65 a 75 Hz</td></tr>
                                    <tr><td>02/01/2013 00:01</td><td>02/01/2013 00:20</td><td>19 minutos</td><td>75 a 85 Hz</td></tr>
                                    <tr><td>03/01/2013 00:02</td><td>03/01/2013 00:30</td><td>28 minutos</td><td>50 a 55 Hz</td></tr>
                                    <tr><td>04/01/2013 00:03</td><td>04/01/2013 00:40</td><td>37 minutos</td><td>52 a 53 Hz</td></tr>
                                    <tr><td>05/01/2013 00:04</td><td>05/01/2013 00:50</td><td>46 minutos</td><td>68 a 75 Hz</td></tr>
                                    <tr><td colspan="2">Tempo total de risco</td><td colspan="2">2 horas e 20 minutos</td></tr>
                                </tbody>
                            </table>

                        </div>  

                        <div class="row-fluid">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Fator de potência não recomendado</h3>
                                <span class="pull-right label label-inverse">Período <span class="prdi">01/03/13</span><span class="pcdi"></span> - <span class="pcdf"><span class="prdf">14/03/13</span>
                            </div>                          
                            <table class="table table-striped table-bordered table-condensed" >
                                <thead>
                                    <tr>
                                           
                                        <th>Data início PF não recomendado</th>                                            
                                        <th>Data fim PF não recomendado</th>
                                        <th>Tempo total PF não recomendado</th>   
                                        <th>Oscilação PF não recomendado</th>                                                                                                         
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td>01/01/2013 00:00</td><td>01/01/2013 00:10</td><td>10 minutos</td><td>0,8 a 0,86</td></tr>
                                    <tr><td>02/01/2013 00:01</td><td>02/01/2013 00:20</td><td>19 minutos</td><td>0,82 a 0,88</td></tr>
                                    <tr><td>03/01/2013 00:02</td><td>03/01/2013 00:30</td><td>28 minutos</td><td>0,83 a 0,87</td></tr>
                                    <tr><td>04/01/2013 00:03</td><td>04/01/2013 00:40</td><td>37 minutos</td><td>0,81 a 0,86</td></tr>
                                    <tr><td>05/01/2013 00:04</td><td>05/01/2013 00:50</td><td>46 minutos</td><td>0,84 a 0,85</td></tr>
                                    <tr><td colspan="2">Tempo total de risco</td><td colspan="2">2 horas e 20 minutos</td></tr>
                                </tbody>
                            </table>

                        </div>                                                                                      
                    </div>

                    <?php }?>

                    <?php
                     if ($tipo_cabecalho == "regulador") {
                    ?>   

                    <div class="area_tabela">

                        <div class="row-fluid">
                                   <div class="span12">
                                        <ul class="ov_boxes">
                                             <li>
                                                <div class="p_bar_anterior_azul p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">20,25,34</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong>35</strong>
                                                    Nª interrupções
                                                </div>
                                            </li>
                                            <li>
                                                <div class="p_bar_atual_azul p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">25,34,12</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong> 5h 39min</strong>
                                                    Tempo interrompido
                                                </div>
                                            </li> 
                                            <li>
                                                <div class="p_bar_atual_vermelho p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">25,34,12</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong> 190,2 a 223,9</strong>
                                                    Var. de Tensão (V)
                                                </div>
                                            </li> 
                                            <li>
                                                <div class="p_bar_atual_vermelho p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">25,34,12</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong> 45 a 64</strong>
                                                    Var. de Freq. (Hz)
                                                </div>
                                            </li>                                                                                         
 
                                        </ul>
                                                                          
                                    </div>   
                        </div>
                        <?php
                         if ($tipo_cabecalho != "casa") {
                        ?>   
                            <div class="row-fluid">
                                <div class="heading clearfix">
                                    <h3 class="pull-left">Áreas afetadas por instabilidades de tensão, frequência ou fator de potência</h3>
                                    <span class="pull-right label label-inverse">Período <span class="prdi">01/03/13</span><span class="pcdi"></span> - <span class="pcdf"><span class="prdf">14/03/13</span>
                                </div>  
                                <div class="span11">
                                  <div id="msg" class="alert alert">Passe o mouse sobre o mapa para ver o progresso das instabilidades.</div>
                                  <div id="year" class="alert alert-info">1962</div>
                                  <div id="map" onmouseover="start()"></div>
                                  
                                  <div id="lightbox" ></div>
                                  
                              </div>
                            </div>  
                        <?php }?>
                        <div class="row-fluid">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Interrupções de fornecimento de energia elétrica</h3>
                                <span class="pull-right label label-inverse">Período <span class="prdi">01/03/13</span><span class="pcdi"></span> - <span class="pcdf"><span class="prdf">14/03/13</span>
                            </div>                          
                            <table class="table table-striped table-bordered table-condensed" >
                                <thead>
                                    <tr>
                                        <th>Concessionária</th>                                         
                                        <th>Data início da interrupção</th>                                            
                                        <th>Data fim da interrupção</th>
                                        <th>Tempo total interrompido</th>                                                                                                          
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td>CEB</td>         <td>01/01/2013 00:00</td><td>01/01/2013 00:10</td><td>10 minutos</td></tr>
                                    <tr><td>Eletronorte</td> <td>02/01/2013 00:01</td><td>02/01/2013 00:20</td><td>19 minutos</td></tr>
                                    <tr><td>Itaipu</td>      <td>03/01/2013 00:02</td><td>03/01/2013 00:30</td><td>28 minutos</td></tr>
                                    <tr><td>Furnas</td>      <td>04/01/2013 00:03</td><td>04/01/2013 00:40</td><td>37 minutos</td></tr>
                                    <tr><td>CELG</td>        <td>05/01/2013 00:04</td><td>05/01/2013 00:50</td><td>46 minutos</td></tr>
                                    <tr><td colspan="3">Total</td><td>2 horas e 20 minutos</td></tr>
                                </tbody>
                            </table>

                        </div>    
                        <div class="row-fluid">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Tensão de risco</h3>
                                <span class="pull-right label label-inverse">Período <span class="prdi">01/03/13</span><span class="pcdi"></span> - <span class="pcdf"><span class="prdf">14/03/13</span>
                            </div>                          
                            <table class="table table-striped table-bordered table-condensed" >
                                <thead>
                                    <tr>
                                        <th>Concessionária</th>   
                                        <th>Data início da tensão de risco</th>                                            
                                        <th>Data fim da tensão de risco</th>
                                        <th>Tempo total da tensão de risco</th>   
                                        <th>Oscilação de risco</th>                                                                                                         
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td>CEB</td>         <td>01/01/2013 00:00</td><td>01/01/2013 00:10</td><td>10 minutos</td><td>230 a 260 V</td></tr>
                                    <tr><td>Eletronorte</td> <td>02/01/2013 00:01</td><td>02/01/2013 00:20</td><td>19 minutos</td><td>230 a 280 V</td></tr>
                                    <tr><td>Itaipu</td>      <td>03/01/2013 00:02</td><td>03/01/2013 00:30</td><td>28 minutos</td><td>250 a 260 V</td></tr>
                                    <tr><td>Furnas</td>      <td>04/01/2013 00:03</td><td>04/01/2013 00:40</td><td>37 minutos</td><td>270 a 280 V</td></tr>
                                    <tr><td>CELG</td>        <td>05/01/2013 00:04</td><td>05/01/2013 00:50</td><td>46 minutos</td><td>110 a 140 V</td></tr>
                                    <tr><td colspan="3">Tempo total de risco</td><td colspan="2">2 horas e 20 minutos</td></tr>
                                </tbody>
                            </table>

                        </div> 
                        <div class="row-fluid">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Frequência de risco</h3>
                                <span class="pull-right label label-inverse">Período <span class="prdi">01/03/13</span><span class="pcdi"></span> - <span class="pcdf"><span class="prdf">14/03/13</span>
                            </div>                          
                            <table class="table table-striped table-bordered table-condensed" >
                                <thead>
                                    <tr>
                                        <th>Concessionária</th>   
                                        <th>Data início da frequência de risco</th>                                            
                                        <th>Data fim da frequência de risco</th>
                                        <th>Tempo total da frequência de risco</th>   
                                        <th>Oscilação de risco</th>                                                                                                         
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td>CEB</td>         <td>01/01/2013 00:00</td><td>01/01/2013 00:10</td><td>10 minutos</td><td>65 a 75 Hz</td></tr>
                                    <tr><td>Eletronorte</td> <td>02/01/2013 00:01</td><td>02/01/2013 00:20</td><td>19 minutos</td><td>75 a 85 Hz</td></tr>
                                    <tr><td>Itaipu</td>      <td>03/01/2013 00:02</td><td>03/01/2013 00:30</td><td>28 minutos</td><td>50 a 55 Hz</td></tr>
                                    <tr><td>Furnas</td>      <td>04/01/2013 00:03</td><td>04/01/2013 00:40</td><td>37 minutos</td><td>52 a 53 Hz</td></tr>
                                    <tr><td>CELG</td>        <td>05/01/2013 00:04</td><td>05/01/2013 00:50</td><td>46 minutos</td><td>68 a 75 Hz</td></tr>
                                    <tr><td colspan="3">Tempo total de risco</td><td colspan="2">2 horas e 20 minutos</td></tr>
                                </tbody>
                            </table>

                        </div>  

                        <div class="row-fluid">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Fator de potência não recomendado</h3>
                                <span class="pull-right label label-inverse">Período <span class="prdi">01/03/13</span><span class="pcdi"></span> - <span class="pcdf"><span class="prdf">14/03/13</span>
                            </div>                          
                            <table class="table table-striped table-bordered table-condensed" >
                                <thead>
                                    <tr>
                                        <th>Concessionária</th>   
                                        <th>Data início PF não recomendado</th>                                            
                                        <th>Data fim PF não recomendado</th>
                                        <th>Tempo total PF não recomendado</th>   
                                        <th>Oscilação PF não recomendado</th>                                                                                                         
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td>CEB</td>         <td>01/01/2013 00:00</td><td>01/01/2013 00:10</td><td>10 minutos</td><td>0,8 a 0,86</td></tr>
                                    <tr><td>Eletronorte</td> <td>02/01/2013 00:01</td><td>02/01/2013 00:20</td><td>19 minutos</td><td>0,82 a 0,88</td></tr>
                                    <tr><td>Itaipu</td>      <td>03/01/2013 00:02</td><td>03/01/2013 00:30</td><td>28 minutos</td><td>0,83 a 0,87</td></tr>
                                    <tr><td>Furnas</td>      <td>04/01/2013 00:03</td><td>04/01/2013 00:40</td><td>37 minutos</td><td>0,81 a 0,86</td></tr>
                                    <tr><td>CELG</td>        <td>05/01/2013 00:04</td><td>05/01/2013 00:50</td><td>46 minutos</td><td>0,84 a 0,85</td></tr>
                                    <tr><td colspan="3">Tempo total de risco</td><td colspan="2">2 horas e 20 minutos</td></tr>
                                </tbody>
                            </table>

                        </div>                                                                                      
                    </div>

                    <?php }?>

                    <?php
                     if ($tipo_cabecalho == "prestador") {
                    ?>   

                    <div class="area_tabela">

                        <div class="row-fluid">
                                   <div class="span12">
                                        <ul class="ov_boxes">
                                             <li>
                                                <div class="p_bar_anterior_azul p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">20,25,34</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong>12</strong>
                                                    Nª interrupções
                                                </div>
                                            </li>
                                            <li>
                                                <div class="p_bar_atual_azul p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">25,34,12</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong> 56 minutos</strong>
                                                    Tempo interrompido
                                                </div>
                                            </li> 
                                            <li>
                                                <div class="p_bar_atual_vermelho p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">25,34,12</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong> 201,2 a 289,9</strong>
                                                    Var. de Tensão (V)
                                                </div>
                                            </li> 
                                            <li>
                                                <div class="p_bar_atual_vermelho p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">25,34,12</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong> 56 a 99</strong>
                                                    Var. de Freq. (Hz)
                                                </div>
                                            </li>                                                                                         
 
                                        </ul>
                                                                          
                                    </div>   
                        </div>
                        <?php
                         if ($tipo_cabecalho != "casa") {
                        ?>   
                            <div class="row-fluid">
                                <div class="heading clearfix">
                                    <h3 class="pull-left">Áreas afetadas por instabilidades de tensão, frequência ou fator de potência</h3>
                                    <span class="pull-right label label-inverse">Período <span class="prdi">01/03/13</span><span class="pcdi"></span> - <span class="pcdf"><span class="prdf">14/03/13</span>
                                </div>  
                                <div class="span11">
                                  <div id="msg" class="alert alert">Passe o mouse sobre o mapa para ver o progresso das instabilidades.</div>
                                  <div id="year" class="alert alert-info">1962</div>
                                  <div id="map" onmouseover="start()"></div>
                                  
                                  <div id="lightbox" ></div>
                                  
                              </div>
                            </div>  
                        <?php }?>
                        <div class="row-fluid">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Interrupções de fornecimento de energia elétrica</h3>
                                <span class="pull-right label label-inverse">Período <span class="prdi">01/03/13</span><span class="pcdi"></span> - <span class="pcdf"><span class="prdf">14/03/13</span>
                            </div>                          
                            <table class="table table-striped table-bordered table-condensed" >
                                <thead>
                                    <tr>
                                        <th>Concessionária</th>                                         
                                        <th>Data início da interrupção</th>                                            
                                        <th>Data fim da interrupção</th>
                                        <th>Tempo total interrompido</th>                                                                                                          
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td>CEB</td>         <td>01/01/2013 00:00</td><td>01/01/2013 00:10</td><td>10 minutos</td></tr>
                                    <tr><td>CEB</td> <td>02/01/2013 00:01</td><td>02/01/2013 00:20</td><td>19 minutos</td></tr>
                                    <tr><td>CEB</td>      <td>03/01/2013 00:02</td><td>03/01/2013 00:30</td><td>28 minutos</td></tr>
                                    <tr><td>CEB</td>      <td>04/01/2013 00:03</td><td>04/01/2013 00:40</td><td>37 minutos</td></tr>
                                    <tr><td>CEB</td>        <td>05/01/2013 00:04</td><td>05/01/2013 00:50</td><td>46 minutos</td></tr>
                                    <tr><td colspan="3">Total</td><td>2 horas e 20 minutos</td></tr>
                                </tbody>
                            </table>

                        </div>    
                        <div class="row-fluid">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Tensão de risco</h3>
                                <span class="pull-right label label-inverse">Período <span class="prdi">01/03/13</span><span class="pcdi"></span> - <span class="pcdf"><span class="prdf">14/03/13</span>
                            </div>                          
                            <table class="table table-striped table-bordered table-condensed" >
                                <thead>
                                    <tr>
                                        <th>Concessionária</th>   
                                        <th>Data início da tensão de risco</th>                                            
                                        <th>Data fim da tensão de risco</th>
                                        <th>Tempo total da tensão de risco</th>   
                                        <th>Oscilação de risco</th>                                                                                                         
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td>CEB</td>         <td>01/01/2013 00:00</td><td>01/01/2013 00:10</td><td>10 minutos</td><td>230 a 260 V</td></tr>
                                    <tr><td>CEB</td> <td>02/01/2013 00:01</td><td>02/01/2013 00:20</td><td>19 minutos</td><td>230 a 280 V</td></tr>
                                    <tr><td>CEB</td>      <td>03/01/2013 00:02</td><td>03/01/2013 00:30</td><td>28 minutos</td><td>250 a 260 V</td></tr>
                                    <tr><td>CEB</td>      <td>04/01/2013 00:03</td><td>04/01/2013 00:40</td><td>37 minutos</td><td>270 a 280 V</td></tr>
                                    <tr><td>CEB</td>        <td>05/01/2013 00:04</td><td>05/01/2013 00:50</td><td>46 minutos</td><td>110 a 140 V</td></tr>
                                    <tr><td colspan="3">Tempo total de risco</td><td colspan="2">2 horas e 20 minutos</td></tr>
                                </tbody>
                            </table>

                        </div> 
                        <div class="row-fluid">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Frequência de risco</h3>
                                <span class="pull-right label label-inverse">Período <span class="prdi">01/03/13</span><span class="pcdi"></span> - <span class="pcdf"><span class="prdf">14/03/13</span>
                            </div>                          
                            <table class="table table-striped table-bordered table-condensed" >
                                <thead>
                                    <tr>
                                        <th>Concessionária</th>   
                                        <th>Data início da frequência de risco</th>                                            
                                        <th>Data fim da frequência de risco</th>
                                        <th>Tempo total da frequência de risco</th>   
                                        <th>Oscilação de risco</th>                                                                                                         
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td>CEB</td>         <td>01/01/2013 00:00</td><td>01/01/2013 00:10</td><td>10 minutos</td><td>65 a 75 Hz</td></tr>
                                    <tr><td>CEB</td> <td>02/01/2013 00:01</td><td>02/01/2013 00:20</td><td>19 minutos</td><td>75 a 85 Hz</td></tr>
                                    <tr><td>CEB</td>      <td>03/01/2013 00:02</td><td>03/01/2013 00:30</td><td>28 minutos</td><td>50 a 55 Hz</td></tr>
                                    <tr><td>CEB</td>      <td>04/01/2013 00:03</td><td>04/01/2013 00:40</td><td>37 minutos</td><td>52 a 53 Hz</td></tr>
                                    <tr><td>CEB</td>        <td>05/01/2013 00:04</td><td>05/01/2013 00:50</td><td>46 minutos</td><td>68 a 75 Hz</td></tr>
                                    <tr><td colspan="3">Tempo total de risco</td><td colspan="2">2 horas e 20 minutos</td></tr>
                                </tbody>
                            </table>

                        </div>  

                        <div class="row-fluid">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Fator de potência não recomendado</h3>
                                <span class="pull-right label label-inverse">Período <span class="prdi">01/03/13</span><span class="pcdi"></span> - <span class="pcdf"><span class="prdf">14/03/13</span>
                            </div>                          
                            <table class="table table-striped table-bordered table-condensed" >
                                <thead>
                                    <tr>
                                        <th>Concessionária</th>   
                                        <th>Data início PF não recomendado</th>                                            
                                        <th>Data fim PF não recomendado</th>
                                        <th>Tempo total PF não recomendado</th>   
                                        <th>Oscilação PF não recomendado</th>                                                                                                         
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td>CEB</td>         <td>01/01/2013 00:00</td><td>01/01/2013 00:10</td><td>10 minutos</td><td>0,8 a 0,86</td></tr>
                                    <tr><td>CEB</td> <td>02/01/2013 00:01</td><td>02/01/2013 00:20</td><td>19 minutos</td><td>0,82 a 0,88</td></tr>
                                    <tr><td>CEB</td>      <td>03/01/2013 00:02</td><td>03/01/2013 00:30</td><td>28 minutos</td><td>0,83 a 0,87</td></tr>
                                    <tr><td>CEB</td>      <td>04/01/2013 00:03</td><td>04/01/2013 00:40</td><td>37 minutos</td><td>0,81 a 0,86</td></tr>
                                    <tr><td>CEB</td>        <td>05/01/2013 00:04</td><td>05/01/2013 00:50</td><td>46 minutos</td><td>0,84 a 0,85</td></tr>
                                    <tr><td colspan="3">Tempo total de risco</td><td colspan="2">2 horas e 20 minutos</td></tr>
                                </tbody>
                            </table>

                        </div>                                                                                      
                    </div>

                    <?php }?>
                           
                </div>
            </div>
            
<?php show_sidebar(); ?>
<?php show_footer(); ?>

<!-- datepicker -->
<script src="lib/datepicker/bootstrap-datepicker.min.js"></script>


<!-- specific JSs -->   
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=visualization"></script>
    <script type="text/javascript" src="js/smart/walmarts.json"></script>
    <script type="text/javascript">
    var map, heatmap, data;
    var nextStore = 0;
    var year = 1962;
    var month = 1;
    var running = false;
    var unlock = false;

    function initialize() {
      data = new google.maps.MVCArray();
      
      map = new google.maps.Map(document.getElementById('map'), {
        zoom: 4,
        center: new google.maps.LatLng(37,-96),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: [{
          stylers: [{saturation: -100}]
        }, {
          featureType: 'poi.park',
          stylers: [{visibility: 'off'}]
        }],
        disableDefaultUI: true
      });

      heatmap = new google.maps.visualization.HeatmapLayer({
        map: map,
        data: data,
        radius: 16,
        dissipate: false,
        maxIntensity: 8,
        gradient: [
          'rgba(0, 0, 0, 0)',
          'rgba(0, 0, 255, 1.0)',
          'rgba(255, 0, 0, 0.50)'
          
        ]
      });

      google.maps.event.addListener(map, 'mouseout', stop);
      google.maps.event.addListener(map, 'tilesloaded', function() {
        unlock = true;
      });
    }

    function start() {
      if (unlock && ! running) {
        document.getElementById("lightbox").style.display = "none";
        document.getElementById("msg").style.display = "none";
        running = true;
        nextMonth();
      }
    }

    function stop() {
      document.getElementById("lightbox").style.display = "block";
      document.getElementById("msg").style.display = "block";
      running = false;
    }

    function nextMonth() {
      if (! running) {
        return;
      }
      while (stores[nextStore].date[0] <= year && stores[nextStore].date[1] <= month) {
        data.push(new google.maps.LatLng(stores[nextStore].coords[0], stores[nextStore].coords[1]));
        nextStore++;
      }
      if (nextStore < stores.length) {
        if (month == 12) {
          month = 1;
          year++;
          document.getElementById('year').innerHTML = year;
        } else {
          month++;
        }
        setTimeout(nextMonth, 30);
      }
    }
    </script>       
<script src="js/smart/informacoesTecnicasSituacoesRisco.js"></script>
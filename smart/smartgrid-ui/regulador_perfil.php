<?php 
include 'util/util_regulador_escolha_perfil.php';
show_header("Selecionar Perfil - Regulador");
?>
<style type="text/css">
    .selectDisabled{color:#C8C8C8;}
    #map_canvas {width:100%; height:354px; }
    body{margin: 0;}
    .link_button:link, .link_button:visited {
        color: #000000;
        padding:5px;
        background-color: #eeeeee;
        border-top: 1px solid #CCC;
        border-right: 1px solid #666;
        border-bottom: 1px solid #666;
        border-left: 1px solid #CCC;
    }

    .link_button:hover {
        padding:5px;
        border-top: 1px solid #666;
        border-right: 1px solid #CCC;
        border-bottom: 1px solid #CCC;
        border-left: 1px solid #666;
    }
    div#main-map{
      height:300px;
      background:red;
    }    
</style>


            <!-- main content -->
            <div id="contentwrapper">
                <div class="main_content">
                    <nav>
                        <div id="jCrumbs" class="breadCrumb module">
                            <ul>
                                <li>
                                    <a href="#"><i class="icon-home"></i></a>
                                </li>
                                <li>
                                    Perfil
                                </li>                               
                            </ul>
                        </div>
                    </nav>                    

                    <div class="row-fluid">                   
                        <div class="span12">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Selecione a área para monitoração </h3>
                            </div>   


                            <div class="row-fluid">
                                <div class="span12 well">
                                    <div class="heading clearfix">
                                        <h3 class="pull-left">Selecione a área desejada </h3>
                                        <!-- <div id="display_area" class="pull-right">Área selecionada: 0 km<sup>2</sup></div> -->
                                    </div>    
                                    <div class="row-fluid">   
                                        <div class="span12">
                                            <input id="reset" value="Recomeçar" type="button" class="navi btn btn-small  pull-right"/>
                                            <input id="showData"  value="Exibir pontos selecionados" type="button" class="navi btn btn-small  pull-right"/>
                                            <a id="selecionar_area" data-toggle="modal"  href="#myTasks" class="btn btn-small btn-gebo pull-right" style="margin-right:2px;">Gerar relatório <i class="splashy-calendar_week"></i></a>                        
                                                                                        

                                            <div>
                                                <!-- <form action="#" onsubmit="showAddress(this.address.value, 15); return false">
                                                       &nbsp; Endereço: <input type="text" size="15" id="addressInput" name="address" />
                                                       <input type="submit" class="btn " value="Ir" style="margin-top:-10px" />
                                                </form> -->
                                            </div>

                                        </div>
                                    </div>
                              
                                    <div class="row-fluid">
                                        <div id="main-map" class="span12" ></div>
                                    </div>
                                    <div class="row-fluid" style="margin-top:0px;">
                                        <div id="dataPanel" class="span12 well" style="background:white"></div>
                                    </div>     
                            </div>                            
                            <div class="row-fluid">  
                                <div class="span9 well">
                                    <div class="heading clearfix">
                                        <h3 class="pull-left">Selecione a região </h3>
                                    </div>      
                                    <div class="row-fluid">
                                        <div class="row-fluid">
                                            <div class="span4">
                                                <span class="help-block">Selecione a Região</span>
                                                <select id="regiaobrasil" class="span12">
                                                    <option value="" >--</option>                                                
                                                    <option value="Norte" class="selectDisabled">Norte</option>
                                                    <option value="Nordeste" class="selectDisabled">Nordeste</option>
                                                    <option value="Centro-Oeste" selected>Centro-Oeste</option>
                                                    <option value="Sudeste" class="selectDisabled">Sudeste</option>
                                                    <option value="Sul" class="selectDisabled">Sul</option>                                                                                                                                                
                                                </select>
                                            </div>

                                            <div class="span4">
                                                <span class="help-block">Selecione o Estado</span>
                                                <select id="estado" class="span12">
                                                    <option value="0" >--</option>
                                                    <option value="Distrito Federal" selected>Distrito Federal</option>
                                                    <option value="Goiás" class="selectDisabled">Goiás</option>
                                                    <option value="Mato Grosso" class="selectDisabled">Mato Grosso</option>
                                                    <option value="Mato Grosso do Sul" class="selectDisabled">Mato Grosso do Sul</option>                                                                                                                                              
                                                </select>
                                            </div>

                                            <div class="span4">
                                                <span class="help-block">Selecione a cidade</span>
                                                <select id="cidade" class="span12">
                                                    <option value="">--</option>
                                                    <option class="selectDisabled" value="Águas Claras">Águas Claras</option>
                                                    <option value="Brasília" selected>Brasília</option>
                                                    <option class="selectDisabled" value="Brazlândia">Brazlândia</option>
                                                    <option class="selectDisabled" value="Candangolândia">Candangolândia</option>
                                                    <option class="selectDisabled" value="Ceilândia">Ceilândia</option>
                                                    <option class="selectDisabled" value="Cruzeiro">Cruzeiro</option>
                                                    <option class="selectDisabled" value="Gama">Gama</option>
                                                    <option class="selectDisabled" value="Guará">Guará</option>
                                                    <option class="selectDisabled" value="Lago Norte">Lago Norte</option>
                                                    <option class="selectDisabled" value="Lago Sul">Lago Sul</option>
                                                    <option class="selectDisabled" value="Núcleo Bandeirante">Núcleo Bandeirante</option>
                                                    <option class="selectDisabled" value="Octogonal">Octogonal</option>
                                                    <option class="selectDisabled" value="Park Way">Park Way</option>
                                                    <option class="selectDisabled" value="Planaltina">Planaltina</option>
                                                    <option class="selectDisabled" value="Recando das Emas">Recando das Emas</option>
                                                    <option class="selectDisabled" value="Samambaia">Samambaia</option>
                                                    <option class="selectDisabled" value="Santa Maria">Santa Maria</option>
                                                    <option class="selectDisabled" value="Sobradinho">Sobradinho</option>
                                                    <option class="selectDisabled" value="Sudoeste">Sudoeste</option>
                                                    <option class="selectDisabled" value="Taguatinga">Taguatinga</option>

                                                </select> 
                                            </div>     
            

                                            <a id="selecionar_regiao" data-toggle="modal"  href="#myTasks" class="btn btn-small btn-gebo pull-right">Gerar relatório <i class="splashy-calendar_week"></i></a>                          
                                        </div>                                        
                                    </div>   
                                                                                                     
                                </div>
                                <div class="span3 well">
                                    <div class="heading clearfix">
                                        <h3 class="pull-left">Selecione a prestadora </h3>
                                    </div>  
                                    <div class="row-fluid">
                                        <div class="row-fluid">
                                            <span class="help-block">Selecione a concessionária</span>
                                                <select id="prestador" name="prestador" class="span12">
                                                    <option value="0" >--</option>
                                                    <option value="CEB" selected>CEB</option>
                                                    <option value="Eletronorte" class="selectDisabled">Eletronorte</option>
                                                    <option value="Itaipu" class="selectDisabled">Itaipu</option>
                                                    <option value="Furnas" class="selectDisabled">Furnas</option>                                                                                                                                              
                                                </select>
                                            <a id="selecionar_prestador" data-toggle="modal"  href="#myTasks" class="btn btn-small btn-gebo pull-right">Gerar relatório <i class="splashy-calendar_week"></i></a>                          
                                        </div>
                                    </div>
                                </div>                                
                            </div>  
                            
                        </div>
                    </div>                  
                </div>
            </div>
            <div class="modal hide fade" id="myTasks">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">×</button>
                    <h3>Confirmar seleção da região</h3>
                </div>
                <form method="get" action="informacoesTecnicasSituacoesRisco.php">
                <div class="modal-body">
                    <div class="row-fluid">
                      <div class="span12 alert alert-info">
                          <strong>Atenção:</strong> os dados que mostraremos a seguir foram gerados em laboratório e extrapolados para aplicações no mundo real. As medições não condizem com a realidade.
                      </div>
                 <!--        <div class="span6">
                            <span class="help-block">Data início</span>
                            <input type="text" class="span12" id="di" name="di">
                        </div>
                        <div class="span6">
                            <span class="help-block">Data fim</span>
                            <input type="text" class="span12" id="df" name="df">
                        </div>  -->
                    <input type="hidden" id="perfil" name="perfil" value="regulador" />
                    <input type="hidden" id="regiao" name="regiao" value="" />   
                    </div> 

                </div>
                <div class="modal-footer">
                  
                    
                    <button type="submit" href="informacoesTecnicasAnaliseEnergia.php" class="btn btn-small btn-success pull-right">Gerar relatório</button>
                  
                </div>
                </form>
            </div>            
            
<?php show_sidebar(); ?>
<?php show_footer(); ?>

<!-- var fillColor = "#F5F5F5"; // blue fill
var lineColor = "#1C87A7"; // black line -->
  <script type="text/javascript">
  $(function(){
      //create map
     var brasilia=new google.maps.LatLng(-15.779699, -47.900391);
     var myOptions = {
        zoom: 11,
        center: brasilia,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
     map = new google.maps.Map(document.getElementById('main-map'), myOptions);
     
     var creator = new PolygonCreator(map);
     
     //reset
     $('#reset').click(function(){ 
        creator.destroy();
        creator=null;
        
        creator=new PolygonCreator(map);
     });     
     
     
     //show paths
     $('#showData').click(function(){ 
        $('#dataPanel').empty();
        if(null==creator.showData()){
          $('#dataPanel').append('Por favor, primeiro crie uma área');
        }else{
          $('#dataPanel').append(creator.showData());
        }
     });
     
     //show color
     $('#showColor').click(function(){ 
        $('#dataPanel').empty();
        if(null==creator.showData()){
          $('#dataPanel').append('Por favor, selecione uma área primeiro.');
        }else{
            $('#dataPanel').append(creator.showColor());
        }
     });
  }); 
  </script>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="js/smart/polygon.min.js"></script>

<!-- datepicker -->
<script src="lib/datepicker/bootstrap-datepicker.min.js"></script>

<!-- specific JSs -->           
<script src="js/smart/regulador_perfil.js"></script>


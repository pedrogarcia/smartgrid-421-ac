<?php 
function show_header($page_name){ 
?>
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <title><?php echo $page_name ?> :: Energia Fácil</title>
        
            <!-- Bootstrap framework -->
                <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
                <link rel="stylesheet" href="bootstrap/css/bootstrap-responsive.min.css" />
            <!-- gebo blue theme-->
                <link rel="stylesheet" href="css/dark.css" id="link_theme" />
            <!-- breadcrumbs-->
                <link rel="stylesheet" href="lib/jBreadcrumbs/css/BreadCrumb.css" />
            <!-- tooltips-->
                <link rel="stylesheet" href="lib/qtip2/jquery.qtip.min.css" />
            <!-- colorbox -->
                <link rel="stylesheet" href="lib/colorbox/colorbox.css" />    
            <!-- code prettify -->
                <link rel="stylesheet" href="lib/google-code-prettify/prettify.css" />    
            <!-- notifications -->
                <link rel="stylesheet" href="lib/sticky/sticky.css" />    
            <!-- splashy icons -->
                <link rel="stylesheet" href="img/splashy/splashy.css" />
    		<!-- flags -->
                <link rel="stylesheet" href="img/flags/flags.css" />	
    		<!-- calendar -->
                <link rel="stylesheet" href="lib/fullcalendar/fullcalendar_gebo.css" />
                
            <!-- main styles -->
                <link rel="stylesheet" href="css/style.css" />
    			
                <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans" />

            <!-- datatables -->
                <link rel="stylesheet" href="lib/datatables/extras/TableTools/media/css/TableTools.css">                

            <!-- datepicker -->
                <link rel="stylesheet" href="lib/datepicker/datepicker.css" />
    	
            <!-- Favicon -->
                <link rel="shortcut icon" href="favicon.ico" />

            <!-- x-editable -->
                <link rel="stylesheet" href="lib/x-editable/bootstrap-editable/css/bootstrap-editable.css">
                <link rel="stylesheet" href="lib/x-editable/inputs-ext/address/address.css">

    		
            <!--[if lte IE 8]>
                <link rel="stylesheet" href="css/ie.css" />
                <script src="js/ie/html5.js"></script>
    			<script src="js/ie/respond.min.js"></script>
    			<script src="lib/flot/excanvas.min.js"></script>
            <![endif]-->
    		
    		<script>
    			//* hide all elements & show preloader
    			document.documentElement.className += 'js';
    		</script>
        </head>
        <body>
    		<div id="loading_layer" style="display:none"><img src="img/ajax_loader.gif" alt="" /></div>
    	
    		<div id="maincontainer" class="clearfix">
    			<!-- header -->
                <header>
                    <div class="navbar navbar-fixed-top">
                        <div class="navbar-inner">
                            <div class="container-fluid">
                                <a class="brand" href="prestador_perfil.php?perfil=prestador&regiao="><i class="icon-home icon-white"></i> Energia Fácil</a>
                                <ul class="nav user_menu pull-right">
    								<li class="divider-vertical hidden-phone hidden-tablet"></li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle nav_condensed" data-toggle="dropdown"><i class="flag-br"></i> <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
    										<li><a href="javascript:void(0)"><i class="flag-br"></i> Português</a></li>
    										<li><a href="javascript:void(0)"><i class="flag-gb"></i> English</a></li>
    										<li><a href="javascript:void(0)"><i class="flag-es"></i> Español</a></li>
                                        </ul>
                                    </li>
                                    <li class="divider-vertical hidden-phone hidden-tablet"></li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="img/avatar_ceb.png" alt="" class="user_avatar" /> CEB - DF <b class="caret"></b></a>
                                        <ul class="dropdown-menu">                                     
                                            <li><a href="javascript:void(0)" onclick="javascrip:alert('Não disponível na versão Trial. É possível determinar preços de energia diferentes para períodos diferentes');"><i class="icon-refresh"></i> Alterar preço do kwh</a></li>
    										<li class="divider"></li>
                                            <li><a href="javascrip:void(0)" onclick="javascrip:alert('Não disponível na versão Trial.');"><i class="icon-question-sign"></i> Ajuda</a></li>               
    										<li><a href="javascrip:void(0)" onclick="javascrip:alert('Não disponível na versão Trial.');"><i class="icon-home"></i> Sobre Energia Fácil 1.0</a></li>
                                            <li class="divider"></li>
                                            <li><a href="index.php"><i class="icon-off"></i> Sair</a></li>                                        
                                        </ul>
                                    </li>
                                </ul>
    							<ul class="nav" id="mobile-nav">
                                    <li>
                                        <a href="prestador_perfil.php"><i class="icon-user icon-white"></i> Alterar Perfil </a>
                                    </li>                                      
    								<li class="dropdown">
    									<a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon-signal icon-white"></i> Consumo <b class="caret"></b></a>
    									<ul class="dropdown-menu">
    										<li><a href="meuConsumoEnergiaUtilizada.php"><i class="icon-asterisk"></i> Energia utilizada</a></li>
                                            <li><a href="meuConsumoImpactoAmbiental.php"><i class="icon-leaf"></i> Impacto ambiental</a></li>     
                                            <li class="divider"></li>                                       
                                            <li><a href="meuConsumoEnergiaComprada.php"><i class="icon-download"></i> Energia comprada</a></li>
    										<li><a href="meuConsumoEnergiaGerada.php"><i class="icon-upload"></i> Energia gerada</a></li>								
                                        </ul>
    								</li>
                                    <li class="dropdown">
                                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon-list-alt icon-white"></i> Faturamento <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="minhaContaFatura.php"><i class="icon-book"></i> Relatório resumido</a></li>
                                            <li class="divider"></li>
                                            <li><a href="minhaContaRelatorioGeral.php"><i class="icon-calendar"></i> Relatório detalhado por período</a></li>
                                        </ul>
                                    </li> 
                                    <li class="dropdown">
                                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon-certificate icon-white"></i> Informações técnicas <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="informacoesTecnicasAnaliseEnergia.php"><i class="icon-random"></i> Análise da Energia</a></li>
                                            <li><a href="informacoesTecnicasSituacoesRisco.php"><i class="icon-warning-sign"></i> Situações de risco</a></li>
                                        </ul>
                                    </li>                                                                     
                                    <li>
                                        
                                    </li>                                                                                          
    							</ul>
                            </div>
                        </div>
                    </div>
                </header>
<?php } ?>        

<?php 
function show_sidebar(){ 
?>    

                <!-- sidebar -->
                <a href="javascript:void(0)" class="sidebar_switch on_switch ttip_r" title="Ocultar Glossário">Ocultar glossário</a>
                <div class="sidebar">
                    
                    <div class="antiScroll">
                        <div class="antiscroll-inner">
                            <div class="antiscroll-content">
                                <div class="sidebar_inner">   
                                    <!-- <form action="search_page.html" class="input-append" method="post" > -->
                                        <form  >
                                        <input autocomplete="off" name="query" class="search_query input-medium" size="16" type="text" placeholder="Pesquisar no glossário..." /><button onclick="javascript:alert('Funcionalidade não disponível na versão Trial');" class="btn"><i class="icon-search"></i></button>
                                    </form>                                                                                 
                                    <div id="side_accordion" class="accordion">
                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a href="#collapseOne" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                                    <i class="icon-cog"></i> Sobre o Energia Fácil
                                                </a>
                                            </div>
                                            <div class="accordion-body collapse" id="collapseOne">
                                                <div class="accordion-inner" style="text-align:justify">
                                                    O Energia Fácil foi desenvolvido por pesquisadores da Universidade de Brasília com a finalidade de explorar a tecnologia Smart Grid. <br /><br />A ideia geral é entregar para os usuários finais de energia elétrica informações importantes para que utilizem sua energia racionalmente, reduzindo o valor da conta e preservando o meio ambiente.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="push"></div>
                                </div>
                                   
                                <div class="sidebar_info">
                                    <ul class="unstyled">
                                        <li>
                                            <strong>Energia comprada </strong>
                                        </li> 
                                        <li>
                                            <span class="act"><strong>Fevereiro - 2013</strong></span>
                                            Mês base
                                        </li>                                                                       
                                        <li>
                                            <span class="act act-danger"><span class="mes_atual_kwh_compra"></span> kwh</span>
                                            Energia comprada
                                        </li>
                                        <li>
                                            <span class="act act-danger">R$ <span class="mes_atual_reais_compra"></span></span>
                                            Gasto parcial
                                        </li>
                                    </ul>
                                </div> 
                            
                            </div>
                        </div>
                    </div>
                </div>

<?php } ?>        

<?php 
function show_footer(){ 
?>    
                <script src="js/jquery.min.js"></script>
                <script src="js/jquery-migrate.min.js"></script>
                <!-- smart resize event -->
                <script src="js/jquery.debouncedresize.min.js"></script>
                <!-- hidden elements width/height -->
                <script src="js/jquery.actual.min.js"></script>
                <!-- js cookie plugin -->
                <script src="js/jquery_cookie.min.js"></script>
                <!-- main bootstrap js -->
                <script src="bootstrap/js/bootstrap.min.js"></script>
                <!-- bootstrap plugins -->
                <script src="js/bootstrap.plugins.min.js"></script>
                <!-- tooltips -->
                <script src="lib/qtip2/jquery.qtip.min.js"></script>
                <!-- jBreadcrumbs -->
                <script src="lib/jBreadcrumbs/js/jquery.jBreadCrumb.1.1.min.js"></script>
                <!-- lightbox -->
                <script src="lib/colorbox/jquery.colorbox.min.js"></script>
                <!-- fix for ios orientation change -->
                <script src="js/ios-orientationchange-fix.js"></script>
                <!-- scrollbar -->
                <script src="lib/antiscroll/antiscroll.js"></script>
                <script src="lib/antiscroll/jquery-mousewheel.js"></script>
                <!-- to top -->
                <script src="lib/UItoTop/jquery.ui.totop.min.js"></script>
                <!-- mobile nav -->
                <script src="js/selectNav.js"></script>
                <!-- common functions -->
                <script src="js/gebo_common.js"></script>
                
                <script src="lib/jquery-ui/jquery-ui-1.10.0.custom.min.js"></script>
                <!-- touch events for jquery ui-->
                <script src="js/forms/jquery.ui.touch-punch.min.js"></script>
                <!-- multi-column layout -->
                <script src="js/jquery.imagesloaded.min.js"></script>
                <script src="js/jquery.wookmark.js"></script>
                <!-- responsive table -->
                <script src="js/jquery.mediaTable.min.js"></script>
                <!-- small charts -->
                <script src="js/jquery.peity.min.js"></script>
                
                <script src="lib/flot/jquery.flot.min.js"></script>
                <!-- não compatível com o highcharts stock 
                <!-- <script src="lib/flot/jquery.flot.resize.min.js"></script>-->
                <script src="lib/flot/jquery.flot.pie.min.js"></script>

                <!-- funcoes genericas -->
                <script src="js/smart/smartCommon.js"></script>                
        
                <script>
                    $(document).ready(function() {
                        //* show all elements & remove preloader
                        setTimeout('$("html").removeClass("js")',1000);
                    });
                </script>
            
            </div>
        </body>
    </html>
<?php } ?>   
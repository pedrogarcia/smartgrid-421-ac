<?php 
include 'util/util.php';
show_header("Impacto Ambiental - Meu consumo");
?>

            <!-- main content -->
            <div id="contentwrapper">
                <div class="main_content">
                    <nav>
                        <div id="jCrumbs" class="breadCrumb module">
                            <ul>
                                <li>
                                    <a href="#"><i class="icon-home"></i></a>
                                </li>
                                <li>
                                    <a href="meuConsumoEnergiaUtilizada.php">Meu consumo</a>
                                </li>
                                <li>
                                    Impacto Ambiental
                                </li>
                                <?php
                                 if ($tipo_regiao != "") {echo "<li><strong>Região: </strong>" . $tipo_regiao . "</li>";}
                                ?>                                
                            </ul>
                        </div>
                    </nav>                    

                    <div class="row-fluid" id="caixaPesquisa">                   
                        <div class="span12">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Sumário de economia e impacto ambiental</h3>
                            </div>     
                            <form >                       
                                <div class="row-fluid">  
                                    <div class="span6 well">
                                        <div>
                                            <p class="f_legend">Selecione o período de referência</p>
                                            <div class="row-fluid">
                                                <div class="span6">
                                                    <span class="help-block">Data início</span>
                                                    <input type="text" class="span12" id="prdi">
                                                </div>
                                                <div class="span6">
                                                    <span class="help-block">Data fim</span>
                                                    <input type="text" class="span12" id="prdf">
                                                </div>       
                                            </div>
                                        </div>
                                        <button  class="btn btn-gebo ttip_t processarImpactoConsumoA" title="Gera o impacto de todo o período selecionado" style="float:right;" type="submit">Gerar impacto histórico</button>
                                    </div>
                                    <div class="span6 well">
                                        <div>
                                            <p class="f_legend">Selecione o período que deseja comparar</p>
                                            <div class="row-fluid">
                                                <div class="span6">
                                                    <span class="help-block">Data início</span>
                                                    <input type="text" class="span12" id="pcdi">
                                                </div>
                                                <div class="span6">
                                                    <span class="help-block">Data fim</span>
                                                    <input type="text" class="span12" id="pcdf">
                                                </div>                                           
                                            </div>
                                        </div>
                                        <button  class="btn btn-gebo ttip_t processarImpactoConsumoB" title="Gera o impacto de todo o período selecionado" style="float:right;" type="submit">Gerar impacto histórico</button>
                                    </div>                               
                                </div>
                                <div class="row-fluid">                   
                                    <div class="span12">
                                        
                                        <button id="processarImpactoConsumoComparado" class="btn btn-gebo ttip_t" title="Exibe a diferença de energia utilizada entre dois períodos" style="float:right; margin-right:10px;" type="submit">Gerar impacto comparado</button>
                                    </div>
                                </div> 
                            </form>
                        </div>                       
                    </div>
                    <div class="row-fluid" id="exibirImpactoConsumoComparado" style="display:none;">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="heading clearfix">
                                    <h3 class="pull-left">Quadro comparativo </h3>
                                </div>                            
                                <table class="table table-striped table-bordered table-condensed">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th><span class="prdi"></span> - <span class="prdf"></span></th>
                                            <th><span class="pcdi"></span> - <span class="pcdf"></span></th>
                                            <th>Diferença de consumo</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><strong>Energia Consumida</strong></td>
                                            <td>123 kWh</td>
                                            <td>233 kWh</td>
                                            <td>110 kWh</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Valor Gasto</strong></td>
                                            <td>R$ 123,00 </td>
                                            <td>R$ 321,00 </td>
                                            <td>R$ 198,00 </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="heading clearfix">
                                    <h3 class="pull-left">O meio ambiente agradece sua economia equivalente a:</h3>
                                </div>
                                <div class="row-fluid">  
                                    <div class="span12">
                                        <ul class="ov_boxes">
                                            <li>
                                                <div class="p_canvas"><img src="img/ambiente/carbono.png" alt="" style="height:40px;" /></div>
                                                <div class="ov_text ov_text_120">
                                                    <strong>0,3 toneladas</strong>
                                                    De carbono
                                                </div>
                                            </li>
                                            <li>
                                                <div class="p_canvas"><img src="img/ambiente/gasolina.png" alt="" style="height:40px;" /></div>
                                                <div class="ov_text ov_text_120">
                                                    <strong>7 litros<span class="mes_atual_reais"></span></strong>
                                                    De gasolina
                                                </div>
                                            </li>
                                            <li>
                                                <div class="p_canvas"><img src="img/ambiente/arvores.png" alt="" style="height:40px;" /></div>
                                                <div class="ov_text ov_text_120">
                                                    <strong>6 Árvores</strong>
                                                    Plantadas
                                                </div>
                                            </li>
                                            <li>
                                                <div class="p_canvas"><img src="img/ambiente/sacola.png" alt="" style="height:40px;" /></div>
                                                <div class="ov_text ov_text_120">
                                                    <strong>12 Sacolas</strong>
                                                    Plásticas
                                                </div>
                                            </li>     
                                            <li>
                                                <div class="p_canvas"><img src="img/ambiente/pegada.png" alt="" style="height:40px;" /></div>
                                                <div class="ov_text ov_text_120">
                                                    <strong>25 Pegadas</strong>
                                                    De carbono
                                                </div>
                                            </li>
                                            <li>
                                                <div class="p_canvas"><img src="img/ambiente/tv.png" alt="" style="height:40px;" /></div>
                                                <div class="ov_text ov_text_120">
                                                    <strong>35 Horas</strong>
                                                    De TV ligada 
                                                </div>
                                            </li>                                                                             
                                        </ul>                                    
                                    </div>  
                                    <button class="btn btn-gebo exibirCaixaPesquisa" style="float:right; margin-right:10px;" type="submit">Nova geração de impacto</button>                                                           
                                </div> 
                                <div class="row-fluid">  
                                    <div class="span12">
                                        <div id="mes_anterior_mes_atual" style="min-width: 100%; height:200px; margin: 0 auto;"></div>
                                    </div>                              
                                </div>                             
                            </div>    
                        </div> 
                    </div>  
                    <div class="row-fluid" id="exibirImpactoConsumo" style="display:none;">
                        <div class="span12">
                            <div class="heading clearfix">
                                <h3 class="pull-left">No período indicado você impactou o ambiente com o equivalente a:</h3>
                                <span class="pull-right label label-inverse">Período <span class="prdi"></span><span class="pcdi"></span> - <span class="pcdf"><span class="prdf"> </span>
                            </div>
                            <div class="row-fluid">  
                                <div class="span12">
                                    <ul class="ov_boxes">
                                        <li>
                                            <div class="p_canvas"><img src="img/ambiente/carbono.png" alt="" style="height:40px;" /></div>
                                            <div class="ov_text ov_text_120">
                                                <strong>184 toneladas</strong>
                                                De carbono
                                            </div>
                                        </li>
                                        <li>
                                            <div class="p_canvas"><img src="img/ambiente/gasolina.png" alt="" style="height:40px;" /></div>
                                            <div class="ov_text ov_text_120">
                                                <strong>345 litros<span class="mes_atual_reais"></span></strong>
                                                De gasolina
                                            </div>
                                        </li>
                                        <li>
                                            <div class="p_canvas"><img src="img/ambiente/arvores.png" alt="" style="height:40px;" /></div>
                                            <div class="ov_text ov_text_120">
                                                <strong>453454 Árvores</strong>
                                                Desmatadas
                                            </div>
                                        </li>
                                        <li>
                                            <div class="p_canvas"><img src="img/ambiente/sacola.png" alt="" style="height:40px;" /></div>
                                            <div class="ov_text ov_text_120">
                                                <strong>124234 Sacolas</strong>
                                                Plásticas
                                            </div>
                                        </li>     
                                        <li>
                                            <div class="p_canvas"><img src="img/ambiente/pegada.png" alt="" style="height:40px;" /></div>
                                            <div class="ov_text ov_text_120">
                                                <strong>2523 Pegadas</strong>
                                                De carbono
                                            </div>
                                        </li>
                                        <li>
                                            <div class="p_canvas"><img src="img/ambiente/tv.png" alt="" style="height:40px;" /></div>
                                            <div class="ov_text ov_text_120">
                                                <strong>33425 Horas</strong>
                                                De TV ligada 
                                            </div>
                                        </li>                                                                             
                                    </ul>                                    
                                </div>   
                                <button class="btn btn-gebo exibirCaixaPesquisa" style="float:right; margin-right:10px;" type="submit">Nova geração de impacto</button>                                                                                                                     
                            </div> 
                            <div class="row-fluid">  
                                <div class="span12">
                                    <div id="mes_anterior_mes_atual" style="min-width: 100%; height:200px; margin: 0 auto;"></div>
                                </div>                              
                            </div>                             
                        </div>     
                    </div>                                      
                </div>
            </div>
            
<?php show_sidebar(); ?>
<?php show_footer(); ?>
<!-- datepicker -->
<script src="lib/datepicker/bootstrap-datepicker.min.js"></script>
<!-- specific JSs -->             
<script src="js/smart/meuConsumoImpactoAmbiental.js"></script>

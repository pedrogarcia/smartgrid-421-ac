/* [ ---- Gebo Admin Panel - editable_elements ---- ] */

    $(function(){
        
        //ajax mocks
        $.mockjaxSettings.responseTime = 500; 
        
        $.mockjax({
            url: '/post',
            response: function(settings) {
                log(settings, this);
            }
        });
    
        $.mockjax({
            url: '/error',
            status: 400,
            statusText: 'Bad Request',
            response: function(settings) {
                this.responseText = 'Please input correct value'; 
                log(settings, this);
            }        
        });
        
        $.mockjax({
            url: '/status',
            status: 500,
            response: function(settings) {
                this.responseText = 'Internal Server Error';
                log(settings, this);
            }        
        });
      
        $.mockjax({
            url: '/groups',
            response: function(settings) {
                this.responseText = [ 
                 {value: 0, text: 'Guest'},
                 {value: 1, text: 'Service'},
                 {value: 2, text: 'Customer'},
                 {value: 3, text: 'Operator'},
                 {value: 4, text: 'Support'},
                 {value: 5, text: 'Admin'}
               ];
               log(settings, this);
            }        
        });
        
        function log(settings, response) {
                var s = [], str;
                s.push(settings.type.toUpperCase() + ' url = "' + settings.url + '"');
                for(var a in settings.data) {
                    if(settings.data[a] && typeof settings.data[a] === 'object') {
                        str = [];
                        for(var j in settings.data[a]) {str.push(j+': "'+settings.data[a][j]+'"');}
                        str = '{ '+str.join(', ')+' }';
                    } else {
                        str = '"'+settings.data[a]+'"';
                    }
                    s.push(a + ' = ' + str);
                }
                s.push('RESPONSE: status = ' + response.status);
    
                if(response.responseText) {
                    if($.isArray(response.responseText)) {
                        s.push('[');
                        $.each(response.responseText, function(i, v){
                           s.push('{value: ' + v.value+', text: "'+v.text+'"}');
                        }); 
                        s.push(']');
                    } else {
                       s.push($.trim(response.responseText));
                    }
                }
                s.push('--------------------------------------\n');
                $('#console').val(s.join('\n') + $('#console').val());
        }
        
        //defaults
        $.fn.editable.defaults.url = '/post'; 
        
         //enable / disable
        $('#enable').click(function() {
            console.log('fd');
            $('#user .editable').editable('toggleDisabled');
        });    
         
         //editables 
        $('#username').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'username',
            title: 'Enter username'
        });
         //editables 
        $('#codigo').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'codigo',
            title: 'Insira seu código de Energia Elétrica'
        });        
         
        $('#firstname').editable({
            validate: function(value) {
               if($.trim(value) == '') return 'This field is required';
            }
        });
         
         $('#sex').editable({
            prepend: "CEB - Companhia Energética de Brasília",
            source: [
                {value: 1, text: 'Celtins'},
                {value: 2, text: 'Ceron'},
                {value: 3, text: 'Cemat'},
                {value: 4, text: 'Celpa'},
                {value: 5, text: 'Cea'},
                {value: 6, text: 'Cepisa'},
                {value: 7, text: 'Chesf'},
                {value: 8, text: 'Cemig'},
                {value: 9, text: 'Furnas'},                                                              
                {value: 10, text: 'Bragantina'},
                {value: 11, text: 'Caiuá'},
                {value: 12, text: 'Sulgipe'},
                {value: 13, text: 'Energisa'},
                {value: 14, text: 'ELFSM'},
                {value: 15, text: 'Light'},
                {value: 16, text: 'CESP'},
                {value: 17, text: 'CNEE'},
                {value: 18, text: 'Duke Energy'},
                {value: 19, text: 'EDEVP'},
                {value: 10, text: 'Elektro'},
                {value: 21, text: 'Emae'},                                                              
                {value: 22, text: 'Erte'},
                {value: 23, text: 'Eletronorte'},
            ],
            display: function(value, sourceData) {
                var colors = {"": "gray", 1: "green", 2: "blue"},
                    elem = $.grep(sourceData, function(o){return o.value == value;});
                if(elem.length) {    
                    $(this).text(elem[0].text).css("color", colors[value]); 
                } else {
                    $(this).empty(); 
                }
            }   
         });    
         
         $('#status').editable();   
         
         $('#group').editable({
            showbuttons: false 
         });   
        
         $('#vacation').editable();  
             
         $('#dob').editable();
               
         $('#event').editable({
            placement: 'right',
            combodate: {
                firstItem: 'name'
            }
         });      
         
         $('#comments').editable({
            showbuttons: true
         }); 

        $('#endereco').editable({
            showbuttons: true
         }); 
         
         $('#note').editable(); 
         
         $('#pencil').click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            $('#note').editable('toggle');
        });   
        
        $('#fruits').editable({
            pk: 1,
            limit: 3,
            source: [
                {value: 1, text: 'banana'},
                {value: 2, text: 'peach'},
                {value: 3, text: 'apple'},
                {value: 4, text: 'watermelon'},
                {value: 5, text: 'orange'}
            ]
         }); 
         
         $('#address').editable({
            url: '/post',
            value: {
               city: "Lago Norte", 
               street: "SHIN CA 05, Bloco I", 
               building: "612"
            },
            validate: function(value) {
               if(value.city == '') return 'Cidade é obrigatório!'; 
            },
            display: function(value) {
                if(!value) {
                    $(this).empty();
                    return; 
                }
                var html = '<b>' + $('<div>').text(value.city).html() + '</b>, ' + $('<div>').text(value.street).html() + ' ' + $('<div>').text(value.building).html();
                $(this).html(html); 
            }         
        });              
              
        $('#user .editable').on('hidden', function(e, reason){
            if(reason === 'save' || reason === 'nochange') {
                var $next = $(this).closest('tr').next().find('.editable');
                if($('#autoopen').is(':checked')) {
                    setTimeout(function() {
                        $next.editable('show');
                    }, 300); 
                } else {
                    $next.focus();
                } 
            }
        });
        
    });
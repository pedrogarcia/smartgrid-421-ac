/* [ ---- Biblioteca Meu Consumo Energia Utilizada ---- ] */

	$(document).ready(function() {


        //* datepicker
        gebo_datepicker.init();        

        //* Start da recuperação dos dados
        smart_informacoes_tecnicas_analise_energia.obter_dados();      

        //* Habilitar uso dos highcharts
        smart_informacoes_tecnicas_analise_energia.init_high_charts();

        //* Ações relatório
        smart_informacoes_tecnicas_analise_energia.acoes_relatorio();

        //* Gráfico Vu de tensão
        //smart_informacoes_tecnicas_analise_energia.vu_meter_tensao(); 

        //* Gráfico Vu de frequencia
        smart_informacoes_tecnicas_analise_energia.tensao_frequencia_dual();      

        //* Gráfico Vu de frequencia
        smart_informacoes_tecnicas_analise_energia.fator_potencia();    

        //* Gráfico Vu de frequencia
        smart_informacoes_tecnicas_analise_energia.potencia_ativa();     

        //* Gráfico Vu de frequencia
        smart_informacoes_tecnicas_analise_energia.potencia_consumida();    

        //* Gráfico Vu de frequencia
        smart_informacoes_tecnicas_analise_energia.potencia_gerada();                                                            

		//* to top
		$().UItoTop({inDelay:200,outDelay:200,scrollSpeed: 500});
	});


    //* bootstrap datepicker
    gebo_datepicker = {
        init: function() {
            var date_picker_options = {
                format: 'dd/mm/yy'
            };

            $('#di').datepicker(date_picker_options);
            $('#df').datepicker(date_picker_options);      
        }
    };



    smart_informacoes_tecnicas_analise_energia = {

        obter_dados: function(){

            /*Função para recuperar json e escrever em arrays*/

            function getJSON(host, meteraddress, limit) {
                var url = "http://164.41.10.22/~pedro/proxy.php?host=" + host + "&meteraddress=" + meteraddress + "&limit=" + limit;
                //var url = "json.html";
                var json;
                $.ajax({
                  async: false,
                  type: "GET",
                  url: url,
                  dataType: "json",
                  success: function(data){
                      json = data; 
                  }
                });
                return json;
            }

            totalRegistros = 20;
            json = getJSON("164.41.10.172", "090832", totalRegistros);
            arraydata = json.data;
            arrayhora = json.hora;
            arrayfatorDePotencia = json.fatorDePotencia;
            arrayfrequencia = json.frequencia;        
            arraypotenciaAtivaConsumida = json.potenciaAtivaConsumida;
            arraypotenciaReativaConsumida = json.potenciaReativaConsumida;
            arrayrTensaoDeFase = json.rTensaoDeFase;  

            

            /*Valor referência*/
            
            tensaoReferencia=220;
            dataInicio = json.hora[0] + " " + json.data[0];
            dataFim = json.hora[totalRegistros-1] + " " + json.data[totalRegistros-1];

            /*Calculos para exibição*/

            totalFrequencia = 0;
            totalTensao = 0;
            maiorFrequencia = 0;
            menorFrequencia = 1000;
            maiorTensao = 0;
            menorTensao = 1000;
            totalPotenciaAtiva = 0;
            totalPotenciaReativa = 0;
            totalFatorPotencia = 0;




            $.each(arraypotenciaAtivaConsumida, function(i, potenciaativa) {
                totalPotenciaAtiva = totalPotenciaAtiva + potenciaativa;              
            });


            $.each(arraypotenciaReativaConsumida, function(i, potenciareativa) {
                totalPotenciaReativa = totalPotenciaReativa + potenciareativa;            
            });


            $.each(arrayfatorDePotencia, function(i, fatorpotencia) {
                totalFatorPotencia = totalFatorPotencia + fatorpotencia;               
            });                                    

            $.each(arrayfrequencia, function(i, frequencia) {
                totalFrequencia = totalFrequencia + frequencia;

                if (frequencia > maiorFrequencia) {
                    maiorFrequencia = frequencia;
                }
                if (frequencia < menorFrequencia) {
                    menorFrequencia = frequencia;
                }                
            });
            $.each(arrayrTensaoDeFase, function(i, tensao) {
                totalTensao = totalTensao + tensao;

                if (tensao > maiorTensao) {
                    maiorTensao = tensao;
                }
                if (tensao < menorTensao) {
                    menorTensao = tensao;
                }                                     
            });

            mediaFrequencia = totalFrequencia/totalRegistros;
            mediaTensao = totalTensao/totalRegistros;
            mediaPotenciaativa = totalPotenciaAtiva/totalRegistros;
            mediaPotenciareativa = totalPotenciaReativa/totalRegistros;
            mediaFatorPotencia = totalFatorPotencia/totalRegistros; 

            /*Transformação em PU*/
            maiorTensao_pu = tensaoReferencia / maiorTensao;
            mediaTensao_pu = tensaoReferencia / mediaTensao;
            menorTensao_pu = tensaoReferencia / menorTensao;

            /*Duas casas decimais*/
            mediaFrequencia = mediaFrequencia.toFixed(2);
            maiorFrequencia = maiorFrequencia.toFixed(2);
            menorFrequencia = menorFrequencia.toFixed(2);
            maiorTensao = maiorTensao.toFixed(2);
            mediaTensao = mediaTensao.toFixed(2);
            menorTensao = menorTensao.toFixed(2);
            maiorTensao_pu = maiorTensao_pu.toFixed(2);
            mediaTensao_pu = mediaTensao_pu.toFixed(2);
            menorTensao_pu = menorTensao_pu.toFixed(2);
            mediaPotenciaativa = mediaPotenciaativa.toFixed(2);
            mediaPotenciareativa = mediaPotenciareativa.toFixed(2);
            mediaFatorPotencia = mediaFatorPotencia.toFixed(2);

            $(".freq_maior_sensor").html(maiorFrequencia);
            $(".freq_media_sensor").html(mediaFrequencia);
            $(".freq_menor_sensor").html(menorFrequencia);
            $(".tensao_maior_sensor").html(maiorTensao);
            $(".tensao_media_sensor").html(mediaTensao);
            $(".tensao_menor_sensor").html(menorTensao);   
            $(".tensao_maior_sensor_pu").html(maiorTensao_pu);
            $(".tensao_media_sensor_pu").html(mediaTensao_pu);
            $(".tensao_menor_sensor_pu").html(menorTensao_pu); 
            $(".fator_potencia_medio_sensor").html(mediaFatorPotencia);
            $(".pot_ativa_consumida_sensor").html(mediaPotenciaativa);
            $(".pot_reativa_consumida_sensor").html(mediaPotenciareativa); 

            $(".prdi").html(dataInicio);
            $(".prdf").html(dataFim);
            $("#di").val(dataInicio);
            $("#df").val(dataFim);


            /*Escrita da tabela de dados*/
            for (var i = 0; i < totalRegistros; i++) {
                data_tabela = json.data[i];
                hora_tabela = json.hora[i];
                tensao_tabela = json.rTensaoDeFase[i];
                frequencia_tabela = json.frequencia[i];
                pot_ativa_tabela = json.potenciaAtivaConsumida[i];
                pot_reativa_tabela = json.potenciaReativaConsumida[i];
                fat_potencia_tabela = json.fatorDePotencia[i];

                conteudoTabela = "<tr>";         
                conteudoTabela = conteudoTabela +"<td>"+data_tabela+"</td><td>"+hora_tabela+"</td><td>"+tensao_tabela+"</td><td>"+frequencia_tabela+"</td><td>"+pot_ativa_tabela+"</td><td>"+pot_reativa_tabela+"</td><td>"+fat_potencia_tabela+"</td>";
                conteudoTabela = conteudoTabela + "</tr>";
                $("#tabelaGeral").append(conteudoTabela);
            };

            
             

        }, 


        acoes_relatorio: function(){

            $(".processarRelatorio").click(function(){
                $(".area_graficos").attr("style","visibility:visible; overflow:visible;");
                $(".area_tabela").attr("style","visibility:hidden; overflow:hidden; height:10px;");
                return false;
            })

            $(".processarTabela").click(function(){
                $(".area_tabela").attr("style","visibility:visible; overflow:visible;");
                $(".area_graficos").attr("style","visibility:hidden; overflow:hidden; height:10px;");
            })

            $(".processarConsulta").click(function(){
                $(".area_tabela").attr("style","visibility:hidden; overflow:hidden; height:10px;");
                $(".area_graficos").attr("style","visibility:hidden; overflow:hidden; height:10px;");
            })


        },

        vu_meter_tensao: function(){

   
                    var chart_vu_meter_tensao = new Highcharts.Chart({
                    
                        chart: {
                            renderTo: 'vu_meter_tensao',
                            type: 'gauge',
                            plotBorderWidth: 1,
                            plotBackgroundColor: {
                                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                                stops: [
                                    [0, '#f5f5f5'],
                                    [0.3, '#FFFFFF'],
                                    [1, '#f5f5f5']
                                ]
                            },
                            plotBackgroundImage: null,
                            height: 200
                        },
                    
                        title: {
                            text: 'Analisador de tensao e frequencia (histórico medido)'
                        },
                        
                        pane: [{
                            startAngle: -45,
                            endAngle: 45,
                            background: null,
                            center: ['25%', '145%'],
                            size: 300
                        }, {
                            startAngle: -45,
                            endAngle: 45,
                            background: null,
                            center: ['75%', '145%'],
                            size: 300
                        }],                     
                    
                        yAxis: [{
                            min: 200,
                            max: 240,
                            minorTickPosition: 'outside',
                            tickPosition: 'outside',
                            labels: {
                                rotation: 'auto',
                                distance: 20
                            },
                            plotBands: [{
                                from: 200,
                                to: 210,
                                color: '#C02316',
                                innerRadius: '100%',
                                outerRadius: '105%'
                            },
                            {
                                from: 210,
                                to: 215,
                                color: '#FFEFBF',
                                innerRadius: '100%',
                                outerRadius: '105%'
                            },  
                            {
                                from: 215,
                                to: 225,
                                color: '#BBFFBB',
                                innerRadius: '100%',
                                outerRadius: '105%'
                            },                                                       
                            {
                                from: 225,
                                to: 230,
                                color: '#FFEFBF',
                                innerRadius: '100%',
                                outerRadius: '105%'
                            }, 
                            {
                                from: 230,
                                to: 240,
                                color: '#C02316',
                                innerRadius: '100%',
                                outerRadius: '105%'
                            }                                                       
                            ],
                            pane: 0,
                            title: {
                                text: 'Volts',
                                y: -40
                            }
                        }, {
                            min: 40,
                            max: 80,
                            minorTickPosition: 'outside',
                            tickPosition: 'outside',
                            labels: {
                                rotation: 'auto',
                                distance: 20
                            },
                            plotBands: [{
                                from: 40,
                                to: 50,
                                color: '#C02316',
                                innerRadius: '100%',
                                outerRadius: '105%'
                            },
                            {
                                from: 50,
                                to: 55,
                                color: '#FFEFBF',
                                innerRadius: '100%',
                                outerRadius: '105%'
                            },  
                            {
                                from: 55,
                                to: 65,
                                color: '#BBFFBB',
                                innerRadius: '100%',
                                outerRadius: '105%'
                            },                                                       
                            {
                                from: 65,
                                to: 70,
                                color: '#FFEFBF',
                                innerRadius: '100%',
                                outerRadius: '105%'
                            }, 
                            {
                                from: 70,
                                to: 80,
                                color: '#C02316',
                                innerRadius: '100%',
                                outerRadius: '105%'
                            } ],
                            pane: 1,
                            title: {
                                text: 'Hertz',
                                y: -40
                            }
                        }],
                        
                        plotOptions: {
                            gauge: {
                                dataLabels: {
                                    enabled: false
                                },
                                dial: {
                                    radius: '100%'
                                }
                            }
                        },
                            
                    
                        series: [{
                            data: [220],
                            yAxis: 0
                        }, {
                            data: [60],
                            yAxis: 1
                        }]
                    
                    },
                    
                    // Let the music play
                    function(chart) {
                        setInterval(function() {
                            var left = chart.series[0].points[0],
                                right = chart.series[1].points[0],
                                leftVal, 
                                inc = (Math.random() - 0.5) * 3;
                                inc2 = (Math.random() - 0.5) * 2;
                    
                            leftVal =  left.y + inc;
                            rightVal = right.y + inc2;

                            left.update(leftVal, false);
                            right.update(rightVal, false);
                            chart.redraw();
                    
                        }, 500);
                    
                    });

        },

        tensao_frequencia_dual: function(){


                    chart_tensao_frequencia_dual = new Highcharts.Chart({
                        chart: {
                            renderTo: 'tensao_frequencia_dual',
                            zoomType: 'xy'
                        },
                        title: {
                            text: 'Historico do periodo'
                        },
                        subtitle: {
                            text: null,
                        },
                        xAxis: [{
                            categories: arrayhora
                        }],
                        yAxis: [{ // Primary yAxis
                            labels: {
                                formatter: function() {
                                    return this.value +'Volts';
                                },
                                style: {
                                    color: '#89A54E'
                                }
                            },
                            title: {
                                text: 'Tensão',
                                style: {
                                    color: '#89A54E'
                                }
                            }
                        }, { // Secondary yAxis
                            title: {
                                text: 'Frequência',
                                style: {
                                    color: '#4572A7'
                                }
                            },
                            labels: {
                                formatter: function() {
                                    return this.value +' Hz';
                                },
                                style: {
                                    color: '#4572A7'
                                }
                            },
                            opposite: true
                        }],
                        tooltip: {
                            formatter: function() {
                                return ''+
                                    this.x +': '+ this.y +
                                    (this.series.name == 'Frequência' ? ' Hz' : 'V');
                            }
                        },
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            
                            verticalAlign: 'bottom',
     
                            backgroundColor: '#FFFFFF'
                        },
                        series: [{
                            name: 'Frequência',
                            color: '#4572A7',
                            type: 'spline',
                            yAxis: 1,
                            data: arrayfrequencia
                
                        }, {
                            name: 'Tensão',
                            color: '#89A54E',
                            type: 'spline',
                            data: arrayrTensaoDeFase
                        }]
                    });


        },   

        fator_potencia: function(){

        chart_fator_potencia = new Highcharts.Chart({
            chart: {
                renderTo: 'fator_potencia',
                type: 'spline'
            },
            title: {
                text: null,
            },
            subtitle: {
                text: null,
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: 'Fator de potência'
                },
                min: 0,
                minorGridLineWidth: 0,
                gridLineWidth: 0,
                alternateGridColor: null,
                plotBands: [{ // Light air
                    from: 0.95,
                    to: 1.05,
                    color: 'rgba(68, 170, 213, 0.1)',
                    label: {
                        text: 'Ideal',
                        style: {
                            color: '#606060'
                        }
                    }
                }]
            },
            tooltip: {
                formatter: function() {
                        return ''+
                        'FP = '+ this.y +' ';
                }
            },
            xAxis: [{
                            categories: arrayhora
                        }],
            series: [{
                name: 'Fator de potência',
                data: arrayfatorDePotencia
    
            }]
            ,
            navigation: {
                menuItemStyle: {
                    fontSize: '10px'
                }
            }
        });
                



        },

        potencia_ativa: function(){

            chart_potencia_ativa = new Highcharts.Chart({
                chart: {
                    renderTo: 'potencia_ativa',
                    type: 'area'
                },
                title: {
                    text: null,
                },
                subtitle: {
                    text: null,
                },
                xAxis: [{
                            categories: arrayhora
                        }],
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Potência kWh'
                    }
                },
                legend: {
                    layout: 'horizontal',
                    backgroundColor: '#FFFFFF',
                    shadow: true
                },
                tooltip: {
                    formatter: function() {
                        return ''+
                            this.x +': '+ this.y +' kWh';
                    }
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                    series: [{
                    name: 'Pot. Reativa',
                    data: arraypotenciaReativaConsumida
                    
        
                }, {
                    name: 'Pot. Ativa',
                    data: arraypotenciaAtivaConsumida
                }]
            });

        },


        init_high_charts: function() {
            Highcharts.setOptions({
                global: {
                    useUTC: false
                }
            });
        },
    }

// JavaScript Document


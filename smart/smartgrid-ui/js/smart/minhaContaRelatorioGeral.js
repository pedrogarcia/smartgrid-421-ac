/* [ ---- Biblioteca Meu Consumo Energia Utilizada ---- ] */

	$(document).ready(function() {


        $(".processarFatura").click(function(){        
            $("#exibirFatura").attr("style","visibility:visible; overflow:visible;");
            $("#caixaPesquisa").hide();
            $(".exibirCaixaPesquisa").show(); 
            $('.prdi').html($("#prdi").val());
            $('.prdf').html($("#prdf").val());               
            return false;
        });    

        $(".exibirCaixaPesquisa").click(function(){        
            $("#exibirFatura").attr("style","visibility:hidden; overflow:hidden; height:10px;");
            $("#caixaPesquisa").show();
            $(".exibirCaixaPesquisa").hide(); 
            $("#prdi").val("");
            $("#prdf").val("");              
            return false;
        });         
        
        //* datepicker
        gebo_datepicker.init();

        //* nice form elements
        gebo_uniform.init();   

        //* Gerar gráfico
        smart_minha_conta_detalhe_compra.grafico_detalhamento();                     


		//* to top
		$().UItoTop({inDelay:200,outDelay:200,scrollSpeed: 500});

	});


    //* bootstrap datepicker
    gebo_datepicker = {
        init: function() {
            var date_picker_options = {
                format: 'dd/mm/yy'
            };

            $('#prdi').datepicker(date_picker_options);
            $('#prdf').datepicker(date_picker_options);
            $('#pcdi').datepicker(date_picker_options);
            $('#pcdf').datepicker(date_picker_options);            
        }
    };

    gebo_uniform = {
        init: function() {
            $(".uni_style").uniform();
        }
    };    
    //data/minhaContaRelatorioCompra.php
    smart_minha_conta_detalhe_compra = {

        grafico_detalhamento: function() {

                var names = [];

                if ($('#uni_consumida').attr('checked')){
                 names.push($('#uni_consumida').val());
                }
                if ($('#uni_gerada').attr('checked')){
                 names.push($('#uni_gerada').val());
                }                
                if ($('#uni_comprada').attr('checked')){
                 names.push($('#uni_comprada').val());
                }                

                var seriesOptions = [],
                        yAxisOptions = [],
                        seriesCounter = 0,
                        colors = Highcharts.getOptions().colors;

                    $.each(names, function(i, name) {

                        $.getJSON('data/minhaContaRelatorioGeral'+ name +'.php',   function(data) {

                            seriesOptions[i] = {
                                name: name,
                                data: data
                            };

                            // As we're loading the data asynchronously, we don't know what order it will arrive. So
                            // we keep a counter and create the chart when all the data is loaded.
                            seriesCounter++;

                            if (seriesCounter == names.length) {
                                createChart();
                            }
                        });
                    });



                    // create the chart when all data is loaded
                    function createChart() {

                        chart = new Highcharts.StockChart({
                            chart: {
                                renderTo: 'grafico_detalhamento'
                            },

                            rangeSelector: {
                                selected: 4
                            },

                            yAxis: {
                                labels: {
                                    formatter: function() {
                                        return (this.value > 0 ? '+' : '') + this.value + '%';
                                    }
                                },
                                plotLines: [{
                                    value: 0,
                                    width: 2,
                                    color: 'silver'
                                }]
                            },
                            
                            plotOptions: {
                                series: {
                                    compare: 'percent'
                                }
                            },
                            
                            tooltip: {
                                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
                                valueDecimals: 2
                            },
                            
                            series: seriesOptions
                        });
                    }


        }
    }

   
/* [ ---- Biblioteca Meu Consumo Energia Utilizada ---- ] */

	$(document).ready(function() {

        //* Habilitar uso dos highcharts
        smart_meu_consumo_energia_comprada.init_high_charts();

        //* Criação de gráfico para leitura da potência em Watts
        smart_meu_consumo_energia_comprada.leitura_tempo_real();

        //* Criação de gráfico mês anterior/mês atual
        smart_meu_consumo_energia_comprada.mes_anterior_mes_atual();

        //* Criação de gráfico simulação geração
        //smart_meu_consumo_energia_comprada.simulador_leitura_compra();        
   

		//* to top
		$().UItoTop({inDelay:200,outDelay:200,scrollSpeed: 500});
	});

    smart_meu_consumo_energia_comprada = {

        simulador_leitura_compra: function(){
            var mes_atual_reais_compra = 4+Math.random(),
                mes_atual_kwh_compra = 36+Math.random();
                $('.mes_atual_reais_compra').html(mes_atual_reais_compra.toFixed(3));
                $('.mes_atual_kwh_compra').html(mes_atual_kwh_compra.toFixed(3));

            setInterval(function() {
                // Simulação de valores de leitura
            mes_atual_reais_compra = mes_atual_reais_compra+(Math.random()/1000),
            mes_atual_kwh_compra = mes_atual_kwh_compra+(Math.random()/1000);

                $('.mes_atual_reais_compra').html(mes_atual_reais_compra.toFixed(3));
                $('.mes_atual_kwh_compra').html(mes_atual_kwh_compra.toFixed(3));
            }, 1000);
        },

        init_high_charts: function() {
            Highcharts.setOptions({
                global: {
                    useUTC: false
                }
            });
        },


        leitura_tempo_real: function() {
            var chart_leitura_tempo_real;
            chart_leitura_tempo_real = new Highcharts.Chart({
                chart: {
                    renderTo: 'leitura_tempo_real',
                    type: 'spline',
                    marginRight: 0,
                    events: {
                        load: function() {
                            // set up the updating of the chart each second
                            var series = this.series[0];
                            setInterval(function() {
                                var x = (new Date()).getTime(), // current time
                                    y = Math.random();
                                    

                                var potencia_consumida = y,
                                    potencia_gerada = Math.random(),
                                    potencia_comprada = potencia_consumida-potencia_gerada;

                                    series.addPoint([x, potencia_comprada], true, true);
                                
                                $('#potencia_consumida').html(potencia_consumida.toFixed(2));
                                $('#potencia_gerada').html(potencia_gerada.toFixed(2));
                                $('#potencia_comprada').html(potencia_comprada.toFixed(2));
                            }, 1000);
                        }
                    }
                },
                title: {
                    text: null
                },
                xAxis: {
                    type: 'datetime',
                    tickPixelInterval: 150
                },
                yAxis: {
                    title: {
                        text: 'Consumo (Watts)'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    formatter: function() {
                            return '<b>'+ this.series.name +'</b><br/>'+
                            '<b>Hora</b>'+Highcharts.dateFormat('%H:%M:%S', this.x) +'<br/>'+
                            '<b>Consumo</b>'+Highcharts.numberFormat(this.y, 2)+' W';
                    }
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                series: [{
                    color: '#E11B28',
                    name: 'Potência comprada (watts)',
                    data: (function() {
                        // generate an array of random data
                        var data = [],
                            time = (new Date()).getTime(),
                            i;
        
                        for (i = -19; i <= 0; i++) {
                            data.push({
                                x: time + i * 1000,
                                y: (Math.random()^2)
                            });
                        }
                        
                        return data;
                    })()
                }]
            });
        }, 
        mes_anterior_mes_atual: function() {
            var chart_leitura_tempo_real;            
            chart_mes_anterior_mes_atual = new Highcharts.Chart({
                chart: {
                    renderTo: 'mes_anterior_mes_atual',
                    type: 'line',
                    marginRight: 130,
                    marginBottom: 25
                },
                title: {
                    text: null,
                },
                xAxis: {
                    categories: ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"]
                },
                yAxis: {
                    title: {
                        text: 'Energia comprada (kWh)'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    formatter: function() {
                            return '<b>'+ this.x + '/' +this.series.name +'</b><br/>'+
                            '<b>Consumo:</b> '+ this.y +'kWh';
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -10,
                    y: 100,
                    borderWidth: 0
                },
                series: [{
                    name: 'Jan/2013',
                    color: '#F29198',
                    data: [5.58, 10.48, 13.83, 17.34, 18.98, 22.3, 23.34, 25.92, 27.48, 30.43, 31.33, 32.23, 33.99, 36.52, 41.13, 43.98, 48.36, 53.6, 55.51, 59.16, 65.15, 66.2, 68.77, 73.98, 77.26, 77.59, 82.12, 84.97, 87.49, 91.49]
                }, {
                    name: 'Fev/2013',
                    color: '#E11B28',
                    data: [1.43, 5.19, 10.89, 14.33, 16.4, 16.95, 19.93, 21.69, 24.38, 29.28, 32.18, 35.38, 35.63]
                }]
            });
        }, 
    }


/* [ ---- Biblioteca Meu Consumo Energia Utilizada ---- ] */

    $(document).ready(function() {

  
        $("#processarImpactoConsumoComparado").click(function(){
            $("#exibirImpactoConsumoComparado").attr("style","visibility:visible; overflow:visible;");
            $("#caixaPesquisa").hide();
            $(".exibirCaixaPesquisa").show();  
            $('.prdi').html($("#prdi").val());
            $('.prdf').html($("#prdf").val());                 
            return false;
        })    
        $(".exibirCaixaPesquisa").click(function(){
            $("#exibirImpactoConsumoComparado").attr("style","visibility:hidden; overflow:hidden; height:10px;");
            $("#caixaPesquisa").show();
            $(".exibirCaixaPesquisa").hide();
            $('#prdi').val("");
            $('#prdf').val("");           
            return false;
        })              

        //* nice form elements
        gebo_uniform.init();   

        //* datepicker
        gebo_datepicker.init();

         //* Habilitar uso dos highcharts
        regulador_relatorio_comparativo.init_high_charts();        
        
        //gerador gráfico comparativo de pizza
        regulador_relatorio_comparativo.comparativo_regioes();            

        //* to top
        $().UItoTop({inDelay:200,outDelay:200,scrollSpeed: 500});
    });

        //* bootstrap datepicker
    gebo_datepicker = {
        init: function() {
            $('#prdi').datepicker(date_picker_options);
            $('#prdf').datepicker(date_picker_options);
            $('#pcdi').datepicker(date_picker_options);
            $('#pcdf').datepicker(date_picker_options);            
        }
    };

    gebo_uniform = {
        init: function() {
            $(".uni_style").uniform();
        }
    };    

    var date_picker_options = {
    format: 'dd/mm/yy'
    };




    regulador_relatorio_comparativo = {

        init_high_charts: function() {
            Highcharts.setOptions({
                global: {
                    useUTC: false
                }
            });
        },

        //comparativo_regioes
        comparativo_regioes: function(){

 var colors = Highcharts.getOptions().colors,
            categories = ['Sudeste', 'Centro-Oeste', 'Sul', 'Nordeste', 'Norte'],
            name = 'Regiões do Brasil',
            data = [{
                    y: 55.11,
                    color: colors[0],
                    drilldown: {
                        name: 'Cidades Sudeste',
                        categories: ['Santos', 'Campinas', 'São Paulo', 'ABC Paulista'],
                         data: [10.85, 7.35, 33.06, 2.81],
                        color: colors[0]
                    }
                }, {
                    y: 21.63,
                    color: colors[1],
                    drilldown: {
                        name: 'Cidades Centro-Oeste',
                        categories: ['Brasília', 'Goiânia', 'Cuiabá', 'Campo Grande', 'Anápolis'],
                        data: [0.20, 0.83, 1.58, 13.12, 5.43],
                        color: colors[1]
                    }
                }, {
                    y: 11.94,
                    color: colors[2],
                    drilldown: {
                        name: 'Cidades Sul',
                        categories: ['Santa Maria', 'Curitiba', 'Santa Catarina', 'Porto Alegre', 'Gramado',
                            'Camburiú', 'Maringá', 'Criciúma'],
                        data: [0.12, 0.19, 0.12, 0.36, 0.32, 9.91, 0.50, 0.22],
                        color: colors[2]
                    }
                }, {
                    y: 7.15,
                    color: colors[3],
                    drilldown: {
                        name: 'Cidades Nordeste',
                        categories: ['Recife', 'Natal', 'Piauí', 'Fortaleza', 'Maceió',
                            'Sergipe', 'João Pessoa'],
                        data: [4.55, 1.42, 0.23, 0.21, 0.20, 0.19, 0.14],
                        color: colors[3]
                    }
                }, {
                    y: 2.14,
                    color: colors[4],
                    drilldown: {
                        name: 'Cidades Norte',
                        categories: ['Manaus', 'Belém', 'Palmas'],
                        data: [ 0.12, 0.37, 1.65],
                        color: colors[4]
                    }
                }];
    
    
        // Build the data arrays
        var browserData = [];
        var versionsData = [];
        for (var i = 0; i < data.length; i++) {
    
            // add browser data
            browserData.push({
                name: categories[i],
                y: data[i].y,
                color: data[i].color
            });
    
            // add version data
            for (var j = 0; j < data[i].drilldown.data.length; j++) {
                var brightness = 0.2 - (j / data[i].drilldown.data.length) / 5 ;
                versionsData.push({
                    name: data[i].drilldown.categories[j],
                    y: data[i].drilldown.data[j],
                    color: Highcharts.Color(data[i].color).brighten(brightness).get()
                });
            }
        }
    
        // Create the chart
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'comparativo_regioes',
                type: 'pie'
            },
            title: {
                text: 'Comparativo de consumo de energia nas regiões do Brasil'
            },
            yAxis: {
                title: {
                    text: 'Total de consumo em kWh no período selecionado'
                }
            },
            plotOptions: {
                pie: {
                    shadow: false
                }
            },
            tooltip: {
                valueSuffix: '%'
            },
            series: [{
                name: 'Regiões do Brasil',
                data: browserData,
                size: '60%',
                dataLabels: {
                    formatter: function() {
                        return this.y > 5 ? this.point.name : null;
                    },
                    color: 'white',
                    distance: -30
                }
            }, {
                name: 'Cidades',
                data: versionsData,
                innerSize: '60%',
                dataLabels: {
                    formatter: function() {
                        // display only if larger than 1
                        return this.y > 1 ? '<b>'+ this.point.name +':</b> '+ this.y +'%'  : null;
                    }
                }
            }]
        });

        }

    };
/* [ ---- Biblioteca Meu Consumo Energia Utilizada ---- ] */

	$(document).ready(function() {

        // Simulação de valores de leitura
        smart_meu_consumo.simulador_leitura_compra();

        //* small charts
        gebo_peity.init();        

		//* to top
		$().UItoTop({inDelay:200,outDelay:200,scrollSpeed: 500});
	});


    smart_meu_consumo = {
        simulador_leitura_compra: function(){
            var mes_atual_reais_compra = 4+Math.random(),
                mes_atual_kwh_compra = 36+Math.random();
                $('.mes_atual_reais_compra').html(mes_atual_reais_compra.toFixed(3));
                $('.mes_atual_kwh_compra').html(mes_atual_kwh_compra.toFixed(3));

            setInterval(function() {
                // Simulação de valores de leitura
            mes_atual_reais_compra = mes_atual_reais_compra+(Math.random()/1000),
            mes_atual_kwh_compra = mes_atual_kwh_compra+(Math.random()/1000);

                $('.mes_atual_reais_compra').html(mes_atual_reais_compra.toFixed(3));
                $('.mes_atual_kwh_compra').html(mes_atual_kwh_compra.toFixed(3));
            }, 1000);
        },
    }

    //* small charts
    gebo_peity = {
        init: function() {
            $.fn.peity.defaults.line = {
                strokeWidth: 1,
                delimeter: ",",
                height: 32,
                max: null,
                min: 0,
                width: 50
            };
            $.fn.peity.defaults.bar = {
                delimeter: ",",
                height: 32,
                max: null,
                min: 0,
                width: 50
            };
            $(".p_bar_up").peity("bar",{
                colour: "#6cc334"
            });
            $(".p_bar_down").peity("bar",{
                colour: "#e11b28"
            });
            $(".p_line_up").peity("line",{
                colour: "#b4dbeb",
                strokeColour: "#3ca0ca"
            });
            $(".p_line_down").peity("line",{
                colour: "#f7bfc3",
                strokeColour: "#e11b28"
            });            
            $(".p_bar_anterior_azul").peity("bar",{
                colour: "#A4BCD9"
            });
            $(".p_bar_atual_azul").peity("bar",{
                colour: "#2F4D71"
            });
            $(".p_bar_anterior_vermelho").peity("bar",{
                colour: "#F29198"
            });
            $(".p_bar_atual_vermelho").peity("bar",{
                colour: "#E11B28"
            });                        
            $(".p_bar_anterior_verde").peity("bar",{
                colour: "#96D76A"
            });
            $(".p_bar_atual_verde").peity("bar",{
                colour: "#4E8B25"
            });
        }
    };    

// JavaScript Document


//Setando variáveis de configuração
var traducao_datatable_ptbr = {
		"sLengthMenu": "_MENU_ registros por página",
		"sEmptyTable": "Não foram encontrados registros",
		"sInfo": "_TOTAL_ registros encontrados (_START_ a _END_)",
		"sInfoEmpty": "Não foi possível encontrar resultados",
		"sInfoFiltered": " - de _MAX_ registros",
		"sLoadingRecords": "Aguardo - carregando...",
		"sProcessing": "Processando...",
		"sSearch": "Pesquisar:",
		"sZeroRecords": "Não foram encontrados registros",
		"oPaginate": {
						"sFirst": "Primeira ",
						"sLast": "Última",
						"sNext": "Próxima",
						"sPrevious": "Anterior",
						
					  },
		"oAria": {
					"sSortAscending": " - ordem crescente",
					"sSortDescending": " - ordem decrescente"
				  }	
	}



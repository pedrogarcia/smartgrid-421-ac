/* [ ---- Biblioteca Meu Consumo Energia Utilizada ---- ] */

	$(document).ready(function() {

        //* datepicker
        gebo_datepicker.init();        

        //* startar mapa
        smart_regulador_selecionar_perfil.startar_mapa();

        //* Habilitar uso dos highcharts
        smart_regulador_selecionar_perfil.dummy();   

                                                      

		//* to top
		$().UItoTop({inDelay:200,outDelay:200,scrollSpeed: 500});
	});


    //* bootstrap datepicker
    gebo_datepicker = {
        init: function() {
            var date_picker_options = {
                format: 'dd/mm/yy'
            };

            $('#di').datepicker(date_picker_options);
            $('#df').datepicker(date_picker_options);      
        }
    };



    smart_regulador_selecionar_perfil = {


        startar_mapa: function(){
        },

        dummy: function(){
            $("#selecionar_area").click(function(){
               $("#regiao").val("Customizada pelo mapa");
            });
            $("#selecionar_regiao").click(function(){
                var regiaobrasil = ($("#regiaobrasil").val())
                var cidade = ($("#cidade").val())
                var estado = ($("#estado").val())
                $("#regiao").val(regiaobrasil+" - "+cidade+" - "+estado);
            });
            $("#selecionar_prestador").click(function(){
                var prestador = ($("#prestador").val())
                $("#regiao").val(prestador);
            });                       
        },

    }

// JavaScript Document


/* [ ---- Biblioteca Meu Consumo Energia Utilizada ---- ] */

	$(document).ready(function() {

        //* Habilitar uso dos highcharts
        smart_meu_consumo_energia_gerada.init_high_charts();

        //* Criação de gráfico para leitura da potência em Watts
        smart_meu_consumo_energia_gerada.leitura_tempo_real();

        //* Criação de gráfico mês anterior/mês atual
        smart_meu_consumo_energia_gerada.mes_anterior_mes_atual();

        //* Criação de gráfico simulação geração
        smart_meu_consumo_energia_gerada.simulador_leitura_geracao();

		//* to top
		$().UItoTop({inDelay:200,outDelay:200,scrollSpeed: 500});
	});

    smart_meu_consumo_energia_gerada = {

        simulador_leitura_geracao: function(){
            var mes_atual_reais_geracao = 2+Math.random(),
                mes_atual_kwh_geracao = 8+Math.random();
                $('.mes_atual_reais_geracao').html(mes_atual_reais_geracao.toFixed(3));
                $('.mes_atual_kwh_geracao').html(mes_atual_kwh_geracao.toFixed(3));

            setInterval(function() {
                // Simulação de valores de leitura
            mes_atual_reais_geracao = mes_atual_reais_geracao+(Math.random()/1000),
            mes_atual_kwh_geracao = mes_atual_kwh_geracao+(Math.random()/1000);

                $('.mes_atual_reais_geracao').html(mes_atual_reais_geracao.toFixed(3));
                $('.mes_atual_kwh_geracao').html(mes_atual_kwh_geracao.toFixed(3));
            }, 1000);
        },

        init_high_charts: function() {
            Highcharts.setOptions({
                global: {
                    useUTC: false
                }
            });
        },


        leitura_tempo_real: function() {
            var chart_leitura_tempo_real;
            chart_leitura_tempo_real = new Highcharts.Chart({
                chart: {
                    renderTo: 'leitura_tempo_real',
                    type: 'spline',
                    marginRight: 0,
                    events: {
                        load: function() {
                            // set up the updating of the chart each second
                            var series = this.series[0];
                            setInterval(function() {
                                var x = (new Date()).getTime(), // current time
                                    y = Math.random();


                                var potencia_consumida = y,
                                    potencia_gerada = Math.random(),
                                    potencia_comprada = potencia_consumida-potencia_gerada;

                                    series.addPoint([x, potencia_gerada], true, true);

                                
                                $('#potencia_consumida').html(potencia_consumida.toFixed(2));
                                $('#potencia_gerada').html(potencia_gerada.toFixed(2));
                                $('#potencia_comprada').html(potencia_comprada.toFixed(2));
                            }, 1000);
                        }
                    }
                },
                title: {
                    text: null
                },
                xAxis: {
                    type: 'datetime',
                    tickPixelInterval: 150
                },
                yAxis: {
                    title: {
                        text: 'Potência gerada (Watts)'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    formatter: function() {
                            return '<b>'+ this.series.name +'</b><br/>'+
                            '<b>Hora</b>'+Highcharts.dateFormat('%H:%M:%S', this.x) +'<br/>'+
                            '<b>Geração</b>'+Highcharts.numberFormat(this.y, 2)+' W';
                    }
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                series: [{
                    color: '#6CC334',
                    name: 'Potência gerada (watts)',
                    data: (function() {
                        // generate an array of random data
                        var data = [],
                            time = (new Date()).getTime(),
                            i;
        
                        for (i = -19; i <= 0; i++) {
                            data.push({
                                x: time + i * 1000,
                                y: (Math.random()^2)
                            });
                        }
                        
                        return data;
                    })()
                }]
            });
        }, 
        mes_anterior_mes_atual: function() {
            var chart_leitura_tempo_real;            
            chart_mes_anterior_mes_atual = new Highcharts.Chart({
                chart: {
                    renderTo: 'mes_anterior_mes_atual',
                    type: 'line',
                    marginRight: 130,
                    marginBottom: 25
                },
                title: {
                    text: null,
                },
                xAxis: {
                    categories: ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"]
                },
                yAxis: {
                    title: {
                        text: 'Energia gerada (kWh)'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    formatter: function() {
                            return '<b>'+ this.x + '/' +this.series.name +'</b><br/>'+
                            '<b>Geração:</b> '+ this.y +'kWh';
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -10,
                    y: 100,
                    borderWidth: 0
                },
                series: [{
                    name: 'Jan/2013',
                    color: '#96D76A',
                    data: [0.63, 0.7, 1.41, 2.01, 2.35, 2.95, 3.9, 3.92, 4.53, 4.63, 4.65, 5.1, 5.69, 5.83, 6.41, 7.18, 7.67, 7.87, 8.59, 9.59, 9.91, 10.53, 10.79, 11.25, 11.99, 12.6, 13.09, 13.95, 14.46, 15.41]
                }, {
                    name: 'Fev/2013',
                    color: '#4E8B25',
                    data: [2.44, 2.83, 2.92, 3.31, 4.03, 4.68, 5.44, 6.36, 6.63, 6.79, 7.32, 7.55, 8.02]
                }]
            });
        }, 
    }


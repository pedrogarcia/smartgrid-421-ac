/* [ ---- Biblioteca Meu Consumo Energia Utilizada ---- ] */

	$(document).ready(function() {


        $(".processarFatura").click(function(){        
            $("#exibirFatura").attr("style","visibility:visible; overflow:visible;");
            $("#caixaPesquisa").hide();
            $(".exibirCaixaPesquisa").show(); 
            $('.prdi').html($("#prdi").val());
            $('.prdf').html($("#prdf").val());               
            return false;
        });    

        $(".exibirCaixaPesquisa").click(function(){        
            $("#exibirFatura").attr("style","visibility:hidden; overflow:hidden; height:10px;");
            $("#caixaPesquisa").show();
            $(".exibirCaixaPesquisa").hide(); 
            $("#prdi").val("");
            $("#prdf").val("");              
            return false;
        });         
        
        //* datepicker
        gebo_datepicker.init();

        //* nice form elements
        gebo_uniform.init();   

        //* Gerar gráfico
        smart_minha_conta_detalhe_compra.grafico_detalhamento();                     


		//* to top
		$().UItoTop({inDelay:200,outDelay:200,scrollSpeed: 500});

	});


    //* bootstrap datepicker
    gebo_datepicker = {
        init: function() {
            var date_picker_options = {
                format: 'dd/mm/yy'
            };

            $('#prdi').datepicker(date_picker_options);
            $('#prdf').datepicker(date_picker_options);
            $('#pcdi').datepicker(date_picker_options);
            $('#pcdf').datepicker(date_picker_options);            
        }
    };

    gebo_uniform = {
        init: function() {
            $(".uni_style").uniform();
        }
    };    
    //data/minhaContaRelatorioCompra.php
    smart_minha_conta_detalhe_compra = {

        grafico_detalhamento: function() {

                $.getJSON('data/minhaContaRelatorioCompra.php', function(data) {

                    // create the chart
                    chart = new Highcharts.StockChart({
                        chart: {
                            renderTo: 'grafico_detalhamento',
                            alignTicks: false
                        },

                        rangeSelector: {
                            selected: 1
                        },                 

                        title: {
                            text: null
                        },

                        series: [{
                            type: 'column',
                            name: 'Energia consumida (kWh)',
                            data: data,
                            dataGrouping: {
                                units: [[
                                    'week', // unit name
                                    [1] // allowed multiples
                                ], [
                                    'month',
                                    [1, 2, 3, 4, 6]
                                ]]
                            }
                        }]
                    });
                });
            


        }
    }

   
/* [ ---- Biblioteca Meu Consumo Energia Utilizada ---- ] */

	$(document).ready(function() {

        //* datepicker
        gebo_datepicker.init();        

        //* dummy
        smart_informacoes_tecnicas_analise_energia.dummy();

        //* dummy
        smart_informacoes_tecnicas_analise_energia.hit_map();

        //* Habilitar uso dos highcharts
        smart_informacoes_tecnicas_analise_energia.acoes_relatorio();  
                                                      

		//* to top
		$().UItoTop({inDelay:200,outDelay:200,scrollSpeed: 500});
	});


    //* bootstrap datepicker
    gebo_datepicker = {
        init: function() {
            var date_picker_options = {
                format: 'dd/mm/yy'
            };

            $('#di').datepicker(date_picker_options);
            $('#df').datepicker(date_picker_options);      
        }
    };



    smart_informacoes_tecnicas_analise_energia = {


        acoes_relatorio: function(){


            $(".processarRelatorio").click(function(){
                $(".area_tabela").show();
                return false;
            })

            $(".processarConsulta").click(function(){
                $(".area_tabela").hide();
                return false;
            })


        },

        dummy: function(){

        },

        hit_map: function(){
            initialize();
        },        

    }

// JavaScript Document


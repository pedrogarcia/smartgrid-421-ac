/* [ ---- Biblioteca Meu Consumo Energia Utilizada ---- ] */

	$(document).ready(function() {

        //* Habilitar uso dos highcharts
        smart_meu_consumo_energia_utilizada.init_high_charts();

        //* Criação de gráfico para leitura da potência em Watts
        smart_meu_consumo_energia_utilizada.leitura_tempo_real();

        //* Criação de gráfico mês anterior/mês atual
        smart_meu_consumo_energia_utilizada.mes_anterior_mes_atual(); 

        //* Criação de gráfico simulação geração
        smart_meu_consumo_energia_utilizada.simulador_leitura_consumo();             

		//* to top
		$().UItoTop({inDelay:200,outDelay:200,scrollSpeed: 500});
	});

    smart_meu_consumo_energia_utilizada = {


        simulador_leitura_consumo: function(){
            var mes_atual_reais = 7+Math.random(),
                mes_atual_kwh = 42+Math.random();
                $('.mes_atual_reais').html(mes_atual_reais.toFixed(3));
                $('.mes_atual_kwh').html(mes_atual_kwh.toFixed(3));

            setInterval(function() {
                // Simulação de valores de leitura
            mes_atual_reais = mes_atual_reais+(Math.random()/1000),
            mes_atual_kwh = mes_atual_kwh+(Math.random()/1000);

                $('.mes_atual_reais').html(mes_atual_reais.toFixed(3));
                $('.mes_atual_kwh').html(mes_atual_kwh.toFixed(3));
            }, 1000);
        },


        init_high_charts: function() {
            Highcharts.setOptions({
                global: {
                    useUTC: false
                }
            });
        },


        leitura_tempo_real: function() {
            var chart_leitura_tempo_real;
            chart_leitura_tempo_real = new Highcharts.Chart({
                chart: {
                    renderTo: 'leitura_tempo_real',
                    type: 'spline',
                    marginRight: 0,
                    events: {
                        load: function() {
                            // set up the updating of the chart each second
                            var series = this.series[0];
                            setInterval(function() {
                                var x = (new Date()).getTime(), // current time
                                    y = Math.random();
                                    series.addPoint([x, y], true, true);

                                var potencia_consumida = y,
                                    potencia_gerada = Math.random(),
                                    potencia_comprada = potencia_consumida-potencia_gerada;

                                
                                $('#potencia_consumida').html(potencia_consumida.toFixed(2));
                                $('#potencia_gerada').html(potencia_gerada.toFixed(2));
                                $('#potencia_comprada').html(potencia_comprada.toFixed(2));
                            }, 1000);
                        }
                    }
                },
                title: {
                    text: null
                },
                xAxis: {
                    type: 'datetime',
                    tickPixelInterval: 150
                },
                yAxis: {
                    title: {
                        text: 'Consumo (Watts)'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    formatter: function() {
                            return '<b>'+ this.series.name +'</b><br/>'+
                            '<b>Hora</b>'+Highcharts.dateFormat('%H:%M:%S', this.x) +'<br/>'+
                            '<b>Consumo</b>'+Highcharts.numberFormat(this.y, 2)+' W';
                    }
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                series: [{
                    name: 'Potência consumida (watts)',
                    data: (function() {
                        // generate an array of random data
                        var data = [],
                            time = (new Date()).getTime(),
                            i;
        
                        for (i = -19; i <= 0; i++) {
                            data.push({
                                x: time + i * 1000,
                                y: (Math.random()^2)
                            });
                        }
                        
                        return data;
                    })()
                }]
            });
        }, 
        mes_anterior_mes_atual: function() {
            var chart_leitura_tempo_real;            
            chart_mes_anterior_mes_atual = new Highcharts.Chart({
                chart: {
                    renderTo: 'mes_anterior_mes_atual',
                    type: 'line',
                    marginRight: 130,
                    marginBottom: 25
                },
                title: {
                    text: null,
                },
                xAxis: {
                    categories: ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"]
                },
                yAxis: {
                    title: {
                        text: 'Energia utilizada (kWh)'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    formatter: function() {
                            return '<b>'+ this.x + '/' +this.series.name +'</b><br/>'+
                            '<b>Consumo:</b> '+ this.y +'kWh';
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -10,
                    y: 100,
                    borderWidth: 0
                },
                series: [{
                    name: 'Jan/2013',
                    color: '#A4BCD9',
                    data: [5.17, 9.38, 12.89, 17.42, 20.76, 21.93, 26.13, 26.73, 31.78, 37.75, 38.34, 43.54, 49.16, 54.68, 58.17, 63.19, 63.9, 68.01, 70.02, 73.04, 77.96, 80.52, 83.55, 84.67, 85.79, 90.75, 96.14, 100.43, 101.97, 106.43]
                }, {
                    name: 'Fev/2013',
                    color: '#2F4D71',
                    data: [1.46, 3.64, 9.22, 10.91, 13.84, 14.89, 20.39, 25.64, 30.22, 30.61, 33.52, 38.54, 42.32]
                }]
            });
        }, 
    }

// JavaScript Document


/* [ ---- Biblioteca Meu Consumo Energia Utilizada ---- ] */

	$(document).ready(function() {

        //* datepicker
        gebo_datepicker.init();        

        //* Habilitar uso dos highcharts
        smart_informacoes_tecnicas_analise_energia.init_high_charts();

        //* Ações relatório
        smart_informacoes_tecnicas_analise_energia.acoes_relatorio();

        //* Gráfico Vu de tensão
        smart_informacoes_tecnicas_analise_energia.vu_meter_tensao(); 

        //* Gráfico Vu de frequencia
        smart_informacoes_tecnicas_analise_energia.tensao_frequencia_dual();      

        //* Gráfico Vu de frequencia
        smart_informacoes_tecnicas_analise_energia.fator_potencia();    

        //* Gráfico Vu de frequencia
        smart_informacoes_tecnicas_analise_energia.potencia_ativa();     

        //* Gráfico Vu de frequencia
        smart_informacoes_tecnicas_analise_energia.potencia_consumida();    

        //* Gráfico Vu de frequencia
        smart_informacoes_tecnicas_analise_energia.potencia_gerada();                                                            

		//* to top
		$().UItoTop({inDelay:200,outDelay:200,scrollSpeed: 500});
	});


    //* bootstrap datepicker
    gebo_datepicker = {
        init: function() {
            var date_picker_options = {
                format: 'dd/mm/yy'
            };

            $('#di').datepicker(date_picker_options);
            $('#df').datepicker(date_picker_options);      
        }
    };



    smart_informacoes_tecnicas_analise_energia = {


        acoes_relatorio: function(){

            $(".processarRelatorio").click(function(){
                $(".area_graficos").attr("style","visibility:visible; overflow:visible;");
                $(".area_tabela").attr("style","visibility:hidden; overflow:hidden; height:10px;");
                return false;
            })

            $(".processarTabela").click(function(){
                $(".area_tabela").attr("style","visibility:visible; overflow:visible;");
                $(".area_graficos").attr("style","visibility:hidden; overflow:hidden; height:10px;");
            })

            $(".processarConsulta").click(function(){
                $(".area_tabela").attr("style","visibility:hidden; overflow:hidden; height:10px;");
                $(".area_graficos").attr("style","visibility:hidden; overflow:hidden; height:10px;");
            })


        },

        vu_meter_tensao: function(){

   
                    var chart_vu_meter_tensao = new Highcharts.Chart({
                    
                        chart: {
                            renderTo: 'vu_meter_tensao',
                            type: 'gauge',
                            plotBorderWidth: 1,
                            plotBackgroundColor: {
                                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                                stops: [
                                    [0, '#f5f5f5'],
                                    [0.3, '#FFFFFF'],
                                    [1, '#f5f5f5']
                                ]
                            },
                            plotBackgroundImage: null,
                            height: 200
                        },
                    
                        title: {
                            text: 'Analisador de tensao e frequencia em tempo real'
                        },
                        
                        pane: [{
                            startAngle: -45,
                            endAngle: 45,
                            background: null,
                            center: ['25%', '145%'],
                            size: 300
                        }, {
                            startAngle: -45,
                            endAngle: 45,
                            background: null,
                            center: ['75%', '145%'],
                            size: 300
                        }],                     
                    
                        yAxis: [{
                            min: 200,
                            max: 240,
                            minorTickPosition: 'outside',
                            tickPosition: 'outside',
                            labels: {
                                rotation: 'auto',
                                distance: 20
                            },
                            plotBands: [{
                                from: 200,
                                to: 210,
                                color: '#C02316',
                                innerRadius: '100%',
                                outerRadius: '105%'
                            },
                            {
                                from: 210,
                                to: 215,
                                color: '#FFEFBF',
                                innerRadius: '100%',
                                outerRadius: '105%'
                            },  
                            {
                                from: 215,
                                to: 225,
                                color: '#BBFFBB',
                                innerRadius: '100%',
                                outerRadius: '105%'
                            },                                                       
                            {
                                from: 225,
                                to: 230,
                                color: '#FFEFBF',
                                innerRadius: '100%',
                                outerRadius: '105%'
                            }, 
                            {
                                from: 230,
                                to: 240,
                                color: '#C02316',
                                innerRadius: '100%',
                                outerRadius: '105%'
                            }                                                       
                            ],
                            pane: 0,
                            title: {
                                text: 'Volts',
                                y: -40
                            }
                        }, {
                            min: 40,
                            max: 80,
                            minorTickPosition: 'outside',
                            tickPosition: 'outside',
                            labels: {
                                rotation: 'auto',
                                distance: 20
                            },
                            plotBands: [{
                                from: 40,
                                to: 50,
                                color: '#C02316',
                                innerRadius: '100%',
                                outerRadius: '105%'
                            },
                            {
                                from: 50,
                                to: 55,
                                color: '#FFEFBF',
                                innerRadius: '100%',
                                outerRadius: '105%'
                            },  
                            {
                                from: 55,
                                to: 65,
                                color: '#BBFFBB',
                                innerRadius: '100%',
                                outerRadius: '105%'
                            },                                                       
                            {
                                from: 65,
                                to: 70,
                                color: '#FFEFBF',
                                innerRadius: '100%',
                                outerRadius: '105%'
                            }, 
                            {
                                from: 70,
                                to: 80,
                                color: '#C02316',
                                innerRadius: '100%',
                                outerRadius: '105%'
                            } ],
                            pane: 1,
                            title: {
                                text: 'Hertz',
                                y: -40
                            }
                        }],
                        
                        plotOptions: {
                            gauge: {
                                dataLabels: {
                                    enabled: false
                                },
                                dial: {
                                    radius: '100%'
                                }
                            }
                        },
                            
                    
                        series: [{
                            data: [220],
                            yAxis: 0
                        }, {
                            data: [60],
                            yAxis: 1
                        }]
                    
                    },
                    
                    // Let the music play
                    function(chart) {
                        setInterval(function() {
                            var left = chart.series[0].points[0],
                                right = chart.series[1].points[0],
                                leftVal, 
                                inc = (Math.random() - 0.5) * 3;
                                inc2 = (Math.random() - 0.5) * 2;
                    
                            leftVal =  left.y + inc;
                            rightVal = right.y + inc2;

                            left.update(leftVal, false);
                            right.update(rightVal, false);
                            chart.redraw();
                    
                        }, 500);
                    
                    });

        },

        tensao_frequencia_dual: function(){


                    chart_tensao_frequencia_dual = new Highcharts.Chart({
                        chart: {
                            renderTo: 'tensao_frequencia_dual',
                            zoomType: 'xy'
                        },
                        title: {
                            text: 'Historico do periodo'
                        },
                        subtitle: {
                            text: null,
                        },
                        xAxis: [{
                            categories: ['01/03', '02/03', '03/03', '04/03', '05/03', '06/03',
                                '07/03', '08/03', '09/03', '10/03', '11/03', '12/03']
                        }],
                        yAxis: [{ // Primary yAxis
                            labels: {
                                formatter: function() {
                                    return this.value +'Volts';
                                },
                                style: {
                                    color: '#89A54E'
                                }
                            },
                            title: {
                                text: 'Tensão',
                                style: {
                                    color: '#89A54E'
                                }
                            }
                        }, { // Secondary yAxis
                            title: {
                                text: 'Frequência',
                                style: {
                                    color: '#4572A7'
                                }
                            },
                            labels: {
                                formatter: function() {
                                    return this.value +' Hz';
                                },
                                style: {
                                    color: '#4572A7'
                                }
                            },
                            opposite: true
                        }],
                        tooltip: {
                            formatter: function() {
                                return ''+
                                    this.x +': '+ this.y +
                                    (this.series.name == 'Frequência' ? ' Hz' : 'V');
                            }
                        },
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            
                            verticalAlign: 'bottom',
     
                            backgroundColor: '#FFFFFF'
                        },
                        series: [{
                            name: 'Frequência',
                            color: '#4572A7',
                            type: 'spline',
                            yAxis: 1,
                            data: [60, 61, 60, 62, 60, 59, 60, 60, 61, 60.5, 60.7, 60]
                
                        }, {
                            name: 'Tensão',
                            color: '#89A54E',
                            type: 'spline',
                            data: [220, 220.5, 220.4, 222, 220.1, 219.8, 220.6, 220.4, 220.2, 220, 220.8, 220.6 ]
                        }]
                    });


        },   

        fator_potencia: function(){

        chart_fator_potencia = new Highcharts.Chart({
            chart: {
                renderTo: 'fator_potencia',
                type: 'spline'
            },
            title: {
                text: null,
            },
            subtitle: {
                text: null,
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: 'Fator de potência'
                },
                min: 0,
                minorGridLineWidth: 0,
                gridLineWidth: 0,
                alternateGridColor: null,
                plotBands: [{ // Light air
                    from: 0.95,
                    to: 1.05,
                    color: 'rgba(68, 170, 213, 0.1)',
                    label: {
                        text: 'Ideal',
                        style: {
                            color: '#606060'
                        }
                    }
                }]
            },
            tooltip: {
                formatter: function() {
                        return ''+
                        Highcharts.dateFormat('%e. %b %Y, %H:00', this.x) +': FP = '+ this.y +' ';
                }
            },
            plotOptions: {
                spline: {
                    lineWidth: 2,
                    states: {
                        hover: {
                            lineWidth: 4
                        }
                    },
                    marker: {
                        enabled: false
                    },
                    pointInterval: 3600000, // one hour
                    pointStart: Date.UTC(2013, 2, 1, 0, 0, 0)
                }
            },
            series: [{
                name: 'Fator de potência',
                data: [1, 1, 1, 1, 1, 0.9, 0.98, 0.95, 1, 1, 0.25, 1, 1, 0.9, 0.98, 0.95, 1, 1, 1, 1, 1, 0.9, 0.98, 0.95, 1, 1, 1, 1, 1, 0.9, 0.98, 0.95 ]
    
            }]
            ,
            navigation: {
                menuItemStyle: {
                    fontSize: '10px'
                }
            }
        });
                



        },

        potencia_ativa: function(){

            chart_potencia_ativa = new Highcharts.Chart({
                chart: {
                    renderTo: 'potencia_ativa',
                    type: 'area'
                },
                title: {
                    text: null,
                },
                subtitle: {
                    text: null,
                },
                xAxis: {
                    categories: [
                        '01/03',
                        '02/03',
                        '03/03',
                        '04/03',
                        '05/03',
                        '06/03',
                        '07/03',
                        '08/03',
                        '09/03',
                        '10/03',
                        '11/03',
                        '12/03'
                    ]
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Potência kWh'
                    }
                },
                legend: {
                    layout: 'horizontal',
                    backgroundColor: '#FFFFFF',
                    shadow: true
                },
                tooltip: {
                    formatter: function() {
                        return ''+
                            this.x +': '+ this.y +' kWh';
                    }
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                    series: [{
                    name: 'Pot. Ativa',
                    data: [13, 39, 30, 24, 32, 26, 28, 13, 20, 29, 12, 33]
        
                }, {
                    name: 'Pot. Reativa',
                    data: [6, 8, 4, 3, 8, 4, 6, 4, 7, 3, 3, 7]
        
                }]
            });

        },


        init_high_charts: function() {
            Highcharts.setOptions({
                global: {
                    useUTC: false
                }
            });
        },
    }

// JavaScript Document


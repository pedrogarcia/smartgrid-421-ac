/* [ ---- Biblioteca Meu Consumo Energia Utilizada ---- ] */

	$(document).ready(function() {


        $(".processarImpactoConsumoA").click(function(){        
            $("#exibirImpactoConsumo").show();
            $("#exibirImpactoConsumoComparado").hide();
            $("#caixaPesquisa").hide();
            $(".exibirCaixaPesquisa").show(); 
            $('.prdi').html($("#prdi").val());
            $('.prdf').html($("#prdf").val());              
            return false;
        });
        $(".processarImpactoConsumoB").click(function(){            
            $("#exibirImpactoConsumo").show();
            $("#exibirImpactoConsumoComparado").hide();
            $("#caixaPesquisa").hide();
            $(".exibirCaixaPesquisa").show(); 
            $('.pcdi').html($("#pcdi").val());
            $('.pcdf').html($("#pcdf").val());                
            return false;
        });        
        $("#processarImpactoConsumoComparado").click(function(){
            $("#exibirImpactoConsumo").hide();
            $("#exibirImpactoConsumoComparado").show();
            $("#caixaPesquisa").hide();
            $(".exibirCaixaPesquisa").show();  
            $('.prdi').html($("#prdi").val());
            $('.prdf').html($("#prdf").val());
            $('.pcdi').html($("#pcdi").val());
            $('.pcdf').html($("#pcdf").val());                     
            return false;
        })    
        $(".exibirCaixaPesquisa").click(function(){
            $("#exibirImpactoConsumo").hide();
            $("#exibirImpactoConsumoComparado").hide();
            $("#caixaPesquisa").show();
            $(".exibirCaixaPesquisa").hide();
            $('#prdi').val("");
            $('#prdf').val("");
            $('#pcdi').val("");
            $('#pcdf').val("");              
            return false;
        })              


        //* datepicker
        gebo_datepicker.init();

		//* to top
		$().UItoTop({inDelay:200,outDelay:200,scrollSpeed: 500});
	});

        //* bootstrap datepicker
    gebo_datepicker = {
        init: function() {
            $('#prdi').datepicker(date_picker_options);
            $('#prdf').datepicker(date_picker_options);
            $('#pcdi').datepicker(date_picker_options);
            $('#pcdf').datepicker(date_picker_options);            
        }
    };

    var date_picker_options = {
    format: 'dd/mm/yy'
    };



   
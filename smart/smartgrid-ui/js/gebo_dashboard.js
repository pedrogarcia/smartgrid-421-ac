/* [ ---- Gebo Admin Panel - dashboard ---- ] */

	$(document).ready(function() {

		//* small charts
		gebo_peity.init();
		//* charts
		gebo_charts.fl_1();

		//* to top
		$().UItoTop({inDelay:200,outDelay:200,scrollSpeed: 500});
	});
	
	//* small charts
	gebo_peity = {
		init: function() {
			$.fn.peity.defaults.line = {
				strokeWidth: 1,
				delimeter: ",",
				height: 32,
				max: null,
				min: 0,
				width: 50
			};
			$.fn.peity.defaults.bar = {
				delimeter: ",",
				height: 32,
				max: null,
				min: 0,
				width: 50
			};
			$(".p_bar_up").peity("bar",{
				colour: "#6cc334"
			});
			$(".p_bar_down").peity("bar",{
				colour: "#e11b28"
			});
			$(".p_line_up").peity("line",{
				colour: "#b4dbeb",
				strokeColour: "#3ca0ca"
			});
			$(".p_line_down").peity("line",{
				colour: "#f7bfc3",
				strokeColour: "#e11b28"
			});
		}
	};

	//* charts
    gebo_charts = {
        fl_1: function() {
            // Setup the placeholder reference
            elem = $('#fl_1');
            var sin = [], cos = [];
            for (var i = 0; i < 14; i += 0.5) {
                sin.push([i, Math.sin(i)]);
                cos.push([i, Math.cos(i)]);
            }
            // Setup the flot chart using our data
            $.plot(elem, 
                [
                    { label: "sin(x)",  data: sin},
                    { label: "cos(x)",  data: cos}
                ], 
                {
                    lines: { show: true },
                    points: { show: true },
                    yaxis: { min: -1.2, max: 1.2 },
                    grid: {
                        hoverable: true,
                        borderWidth: 1
                    },
					colors: [ "#8cc7e0", "#2d83a6" ]
                }
            );
            // Create a tooltip on our chart
            elem.qtip({
                prerender: true,
                content: 'Loading...', // Use a loading message primarily
                position: {
                    viewport: $(window), // Keep it visible within the window if possible
                    target: 'mouse', // Position it in relation to the mouse
                    adjust: { x: 8, y: -30 } // ...but adjust it a bit so it doesn't overlap it.
                },
                show: false, // We'll show it programatically, so no show event is needed
                style: {
                    classes: 'ui-tooltip-shadow ui-tooltip-tipsy',
                    tip: false // Remove the default tip.
                }
            });
         
            // Bind the plot hover
            elem.on('plothover', function(event, coords, item) {
                // Grab the API reference
                var self = $(this),
                    api = $(this).qtip(),
                    previousPoint, content,
         
                // Setup a visually pleasing rounding function
                round = function(x) { return Math.round(x * 1000) / 1000; };
         
                // If we weren't passed the item object, hide the tooltip and remove cached point data
                if(!item) {
                    api.cache.point = false;
                    return api.hide(event);
                }
         
                // Proceed only if the data point has changed
                previousPoint = api.cache.point;
                if(previousPoint !== item.dataIndex)
                {
                    // Update the cached point data
                    api.cache.point = item.dataIndex;
         
                    // Setup new content
                    content = item.series.label + '(' + round(item.datapoint[0]) + ') = ' + round(item.datapoint[1]);
         
                    // Update the tooltip content
                    api.set('content.text', content);
         
                    // Make sure we don't get problems with animations
                    //api.elements.tooltip.stop(1, 1);
         
                    // Show the tooltip, passing the coordinates
                    api.show(coords);
                }
            });
        }
    };




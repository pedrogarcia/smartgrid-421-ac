<?php 
include 'util/util.php';
show_header(" Análise de energia - Informações Técnicas");
?>

            <!-- main content -->
            <div id="contentwrapper">
                <div class="main_content">
                    <nav>
                        <div id="jCrumbs" class="breadCrumb module">
                            <ul>
                                <li>
                                    <a href="#"><i class="icon-home"></i></a>
                                </li>
                                <li>
                                    <a href="informacoesTecnicasAnaliseEnergia..php">Informações Técnicas</a>
                                </li>
                                <li>
                                    Análise de Energia
                                </li>
                                <?php
                                 if ($tipo_regiao != "") {echo "<li><strong>Região: </strong>" . $tipo_regiao . "</li>";}
                                ?>
                            </ul>

                        </div>

                    </nav>                    

                    <div class="row-fluid" id="caixaPesquisa">                   
                        <div class="span12">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Informações técnicas</h3>
                            </div>     
                            <form >                       
                                <div class="row-fluid">  
                                    <div class="span12 well">
                                        <div>
                                            <p class="f_legend">Selecione o período para análise</p>
                                            <div class="row-fluid">
                                                <div class="span2">
                                                    <span class="help-block">Data início</span>
                                                    <input type="text" class="span12" id="di">
                                                </div>
                                                <div class="span2">
                                                    <span class="help-block">Data fim</span>
                                                    <input type="text" class="span12" id="df">
                                                </div>                                                
                                                <div class="span2 form-inline">
                                                    <span class="help-block">Selecione a fase</span>
                                                        <select id="tipoGranularidade" class="span12">
                                                            <option value="0" selected>Todas</option>
                                                            <option value="1">Fase A</option>
                                                            <option value="2">Fase B</option>
                                                            <option value="3">Fase C</option>
                                                        </select>
                                                </div> 
                                            <!-- <div class="row-fluid"> -->
                                                <div class="span4 form-inline">
                                                   <!--  <span class="help-block">Tipo de energia avaliada</span>
                                                        <label class="uni-radio">
                                                            <input type="checkbox" value="Consumido" id="uni_consumida" name="uni_consumida" class="uni_style" checked="" />
                                                            Consumo
                                                        </label>
                                                        <label class="uni-radio">
                                                            <input type="checkbox" value="Comprado"  id="uni_comprada"  name="uni_comprada" class="uni_style" checked="" />
                                                            Compra
                                                        </label>
                                                        <label class="uni-radio">
                                                            <input type="checkbox" value="Gerado"  id="uni_gerada"  name="uni_gerada" class="uni_style" checked="" />
                                                            Geração
                                                        </label> -->
                                                </div>                                                                                                  
                                            <!-- </div> -->                                                
                                                <div class="span2">
                                                    <span class="help-block"></span>
                                                        <button  class="btn btn-gebo processarRelatorio"  style="float:right; margin-top:18px;">Gerar relatório</button>
                                                </div>                                                                                                                                                        
                                            </div>
                                        </div>                                        
                                    </div>                           
                                </div>
                            </form>
                        </div>                       
                    </div>

                    <div class="area_graficos" style="visibility:hidden; height:10px;">

                        <div class="row-fluid">
                            <div class="span12">
                                <div class="heading clearfix">
                                    <h3 class="pull-left">Tensão e Frequência FASE A <strong></strong></h3>
                                    <button  class="btn btn-gebo btn-small processarTabela pull-right"  style="float:right;" >Visualizar tabela detalhada</button>
                                    <span class="pull-right label label-inverse">Período <span class="prdi">01/03/13</span><span class="pcdi"></span> - <span class="pcdf"><span class="prdf">14/03/13</span>
                                </span></span>
                                </div>
                                <div class="row-fluid"> 
                                    <div class="span7">
                                        <div id="vu_meter_tensao" style="min-width: 100%; height:200px; margin: 0 auto; background:gray"></div>
                                    </div> 
                                    <div class="span5">
                                        <ul class="ov_boxes">
    <!--                                         <li>
                                                <div class="p_bar_anterior_azul p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">20,25,34</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong>5</strong>
                                                    Nª interrupções
                                                </div>
                                            </li>
                                            <li>
                                                <div class="p_bar_atual_azul p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">25,34,12</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong> 26 minutos</strong>
                                                    Tempo interrompido
                                                </div>
                                            </li> -->
                                            <li>
                                                <div class="p_bar_anterior_azul p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">20,25,34</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong>1,01pu (221 V)</strong>
                                                    Tensão média
                                                </div>
                                            </li>
                                            <li>
                                                <div class="p_bar_atual_vermelho p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">20,25,34</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong>1,3pu (240 V)</strong>
                                                    Tensão Máxima
                                                </div>
                                            </li>
                                            <li>
                                                <div class="p_bar_anterior_vermelho p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">25,34,12</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong> 0,97pu (201 V)</strong>
                                                    Tensão Mínima
                                                </div>
                                            </li>   
                                        </ul>
                                                                          
                                    </div>   
                                                                                         
                                </div> 

                               <div class="row-fluid">  
                                    <div class="span7">
                                        <div id="tensao_frequencia_dual" style="min-width: 100%; height:300px; margin: 0 auto; background:gray"></div>
                                    </div> 
                                    <div class="span5">
                                        <ul class="ov_boxes">
                                            <li>
                                                <div class="p_bar_atual_azul p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">25,34,12</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong> 60,2 Hz</strong>
                                                    Frequência média
                                                </div>
                                            </li>                                           
                                            <li>
                                                <div class="p_bar_atual_vermelho p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">20,25,34</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong>55Hz</strong>
                                                    Frequência máxima
                                                </div>
                                            </li>
                                            <li>
                                                <div class="p_bar_anterior_vermelho p_canvas ttip_t" title="Valor da fatura dos últimos 3 meses">25,34,12</div>
                                                <div class="ov_text ov_text_120">
                                                    <strong> 63Hz</strong>
                                                    Frequência mínima
                                                </div>
                                            </li>                                                                                                                     
                                        </ul>                                    
                                    </div>   
                                                                                         
                                </div>                                                      
                            </div>     
                        </div>




                        <div class="row-fluid">                   
                            <div class="span12">
                                <div class="heading clearfix">
                                    <h3 class="pull-left">Fator de potêcia e histórico de potência ativa e reativa FASE A</h3>
                                    <button  class="btn btn-gebo btn-small processarTabela pull-right"  style="float:right;" >Visualizar tabela detalhada</button>
                                    <span class="pull-right label label-inverse">Período <span class="prdi">01/03/13</span><span class="pcdi"></span> - <span class="pcdf"><span class="prdf">14/03/13</span>
                                </span></span>                            
                                </div>            
                                <div class="row-fluid">
                                    <div class="span7">                                  
                                        <div id="fator_potencia" style="min-width: 100%; height:300px; margin: 0 auto; background:gray"></div>
                                    </div>                                  
                                    <div class="span5">
                                        <ul class="ov_boxes" >
                                            <li>
                                                <div class="p_line_up p_canvas ttip_t" title="Energia consumida nos últimos 7 dias">2,4,9,7,12,8,16</div>
                                                <div class="ov_text">
                                                    <strong>235 W</strong>
                                                    Potência ativa média
                                                </div>
                                            </li>
                                            <li>
                                                <div class="p_line_down p_canvas ttip_t" title="Energia gerada nos últimos 7 dias">3,5,9,7,12,8,16</div>
                                                <div class="ov_text">
                                                    <strong>23 W</strong>
                                                    Potência reativa média
                                                </div>
                                            </li>      
                                            <li>
                                                <div class="p_bar_down p_canvas ttip_t" title="Energia gerada nos últimos 7 dias">3,5,9,7,12,8,16</div>
                                                <div class="ov_text">
                                                    <strong>0.9987</strong>
                                                    Fator de potência médio
                                                </div>
                                            </li>                                                                              
                                        </ul>
                                        
                                    </div>                              
                                </div>

                                <div class="row-fluid">                                    
                                    <div class="span12">
                                        <div class="heading clearfix">
                                            <h3 class="pull-left">Potências Ativas e Reativas</h3>
                                        </div>                                     
                                        <div id="potencia_ativa" style="min-width: 100%; height:350px; margin: 0 auto; background:gray"></div>
                                    </div>                                                                
                                </div> 

                                               
                                
                            </div>
                        </div>
                    </div>

                    <div class="area_tabela" style="visibility:hidden; height:10px;">


                        <div class="row-fluid">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Relatório geral do período</h3>
                                <button  class="btn btn-gebo btn-small processarFatura pull-right processarConsulta"  style="float:right;" type="submit">Verificar novo período</button>
                            </div>                          
                            <table class="table table-striped table-bordered table-condensed" >
                                <thead>
                                    <tr>
                                        <th>Data</th>                                            
                                        <th>Hora</th>
                                        <th>Tensão</th>                                            
                                        <th>Corrente</th>     
                                        <th>Frequência</th>                                            
                                        <th>Potência Ativa</th>         
                                        <th>Potência Reativa</th>
                                        <th>Fator de potência</th>                                                                
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr><td>01/01/2013</td><td>00:00</td><td>225</td><td>1,54</td><td>59</td><td>346,5</td><td>0</td><td>1</td></tr>
                                    <tr><td>01/01/2013</td><td>00:01</td><td>220</td><td>1,54</td><td>56</td><td>338,8</td><td>0</td><td>1</td></tr>
                                    <tr><td>01/01/2013</td><td>00:02</td><td>216</td><td>1,54</td><td>59</td><td>332,64</td><td>0</td><td>1</td></tr>
                                    <tr><td>01/01/2013</td><td>00:03</td><td>218</td><td>1,54</td><td>65</td><td>335,72</td><td>0</td><td>1</td></tr>
                                    <tr><td>01/01/2013</td><td>00:04</td><td>218</td><td>1,54</td><td>57</td><td>335,72</td><td>0</td><td>1</td></tr>
                                    <tr><td>01/01/2013</td><td>00:05</td><td>218</td><td>1,54</td><td>59</td><td>335,72</td><td>0,01</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:06</td><td>225</td><td>1,54</td><td>63</td><td>346,5</td><td>0,01</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:07</td><td>216</td><td>1,54</td><td>56</td><td>332,64</td><td>0,01</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:08</td><td>219</td><td>1,54</td><td>55</td><td>337,26</td><td>0,01</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:09</td><td>225</td><td>1,54</td><td>65</td><td>346,5</td><td>0,02</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:10</td><td>220</td><td>1,54</td><td>55</td><td>338,8</td><td>0,02</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:11</td><td>220</td><td>1,54</td><td>60</td><td>338,8</td><td>0,02</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:12</td><td>220</td><td>1,54</td><td>62</td><td>338,8</td><td>0,02</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:13</td><td>216</td><td>1,54</td><td>65</td><td>332,64</td><td>0,03</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:14</td><td>218</td><td>1,54</td><td>60</td><td>335,72</td><td>0,03</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:15</td><td>224</td><td>1,54</td><td>57</td><td>344,96</td><td>0,03</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:16</td><td>220</td><td>1,54</td><td>65</td><td>338,8</td><td>0,03</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:17</td><td>225</td><td>1,54</td><td>61</td><td>346,5</td><td>0,04</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:18</td><td>217</td><td>1,54</td><td>61</td><td>334,18</td><td>0,04</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:19</td><td>217</td><td>1,54</td><td>58</td><td>334,18</td><td>0,04</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:20</td><td>225</td><td>1,54</td><td>57</td><td>346,5</td><td>0,04</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:21</td><td>217</td><td>1,54</td><td>64</td><td>334,18</td><td>0,04</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:22</td><td>223</td><td>1,54</td><td>57</td><td>343,42</td><td>0,05</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:23</td><td>222</td><td>1,54</td><td>62</td><td>341,88</td><td>0,05</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:24</td><td>218</td><td>1,54</td><td>60</td><td>335,72</td><td>0,05</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:25</td><td>217</td><td>1,54</td><td>60</td><td>334,18</td><td>0,05</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:26</td><td>221</td><td>1,54</td><td>65</td><td>340,34</td><td>0,06</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:27</td><td>224</td><td>1,54</td><td>57</td><td>344,96</td><td>0,06</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:28</td><td>222</td><td>1,54</td><td>61</td><td>341,88</td><td>0,06</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:29</td><td>221</td><td>1,54</td><td>60</td><td>340,34</td><td>0,06</td><td>0,99</td></tr>
                                    <tr><td>01/01/2013</td><td>00:30</td><td>218</td><td>1,54</td><td>62</td><td>335,72</td><td>0,06</td><td>0,99</td></tr>
                                    <tr><td>Média</td><td></td><td>220,16</td><td>1,54</td><td>60,09</td><td>10510,5</td><td>0,94</td><td>0,99</td></tr>

                                </tbody>
                            </table>

                        </div>                 
                    </div>
                           
                </div>
            </div>
            
<?php show_sidebar(); ?>
<?php show_footer(); ?>

<!-- datepicker -->
<script src="lib/datepicker/bootstrap-datepicker.min.js"></script>

<!-- specific JSs -->   
<script src="lib/highcharts/highcharts.js"></script>
<script src="lib/highcharts/highcharts-more.js"></script>
<script src="lib/highcharts/modules/exporting.js"></script>          
<script src="js/smart/informacoesTecnicasAnaliseEnergia.js"></script>
<?php 
include 'util/util.php';
show_header("Relatório Compra - Meu consumo");
?>
<style type="text/css">
    .table thead tr th {text-align:center }
    .table tbody tr td {text-align:center }
</style>
<!-- nice form elements -->
<link rel="stylesheet" href="lib/uniform/Aristo/uniform.aristo.css" />

            <!-- main content -->
            <div id="contentwrapper">
                <div class="main_content">
                    <nav>
                        <div id="jCrumbs" class="breadCrumb module">
                            <ul>
                                <li>
                                    <a href="#"><i class="icon-home"></i></a>
                                </li>
                                <li>
                                    <a href="minhaContaFatura.php">MinhaConta</a>
                                </li>
                                <li>
                                    Relatório detalhado de Compra
                                </li>
                                <?php
                                 if ($tipo_regiao != "") {echo "<li><strong>Região: </strong>" . $tipo_regiao . "</li>";}
                                ?>                                
                            </ul>
                        </div>
                    </nav>                    

                    <div class="row-fluid" id="caixaPesquisa">                   
                        <div class="span12">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Relatório detalhado de compra</h3>
                            </div>     
                            <form >                       
                                <div class="row-fluid">  
                                    <div class="span12 well">
                                        <div>
                                            <p class="f_legend">Selecione o período do relatório</p>
                                            <div class="row-fluid">
                                                <div class="span3">
                                                    <span class="help-block">Data início</span>
                                                    <input type="text" class="span12" id="prdi">
                                                </div>
                                                <div class="span3">
                                                    <span class="help-block">Data fim</span>
                                                    <input type="text" class="span12" id="prdf">
                                                </div>
                                                <div class="span3 form-inline">
                                                    <span class="help-block">Granularidade da tabela de dados</span>
                                                        <label class="uni-radio">
                                                            <input type="radio" value="hora" id="uni_hora" name="uni_detalhe" class="uni_style"  disabled="disabled" />
                                                            Hora
                                                        </label>
                                                        <label class="uni-radio">
                                                            <input type="radio" value="dia"  id="uni_dia"  name="uni_detalhe" class="uni_style" checked="" />
                                                            Dia
                                                        </label>
                                                        <label class="uni-radio">
                                                            <input type="radio" value="mes"  id="uni_mes"  name="uni_detalhe" class="uni_style" disabled="disabled" />
                                                            Mês
                                                        </label>
                                                </div>                                                
                                                <div class="span3">
                                                    <span class="help-block"></span>
                                                        <button  class="btn btn-gebo processarFatura"  style="float:right; margin-top:18px;" type="submit">Gerar fatura</button>
                                                </div>                                                          
                                            </div>
                                        </div>                                        
                                    </div>                           
                                </div>
                            </form>
                        </div>                       
                    </div>
                    <div class="row-fluid" id="exibirFatura" style="visibility:hidden; height:10px; overflow:hidden;"  > 


                        <div class="row-fluid">
                            <div class="heading clearfix">
                                <h3 class="pull-left">Relatório Detalhado <strong><span class="prdi"></span> - <span class="prdf"></span></strong> </h3>
                                <button class="btn btn-gebo btn-small pull-right exibirCaixaPesquisa" style="float:right;" type="submit">Gerar nova fatura</button>                                                           
                            </div>  
                            <div class="row-fluid">
                                <div class="span12">
                                    <div id="grafico_detalhamento" style="height: 500px"></div>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span3">  
                                    <table class="table table-striped table-bordered table-condensed">
                                        <thead>
                                            <tr>
                                                <th>Tipo de tarifa</th>                                            
                                                <th>Valor</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Normal</td>                                            
                                                <td>0,35 R$/kWh</td>
                                            </tr>
                                            <tr>
                                                <td>Pico</td>
                                                <td>1,54 R$/kWh</td>
                                            </tr>                                                                                                                         
                                        </tbody>
                                    </table> 
                                </div>

                                <div class="span9">
                                    <table class="table table-striped table-bordered table-condensed" >
                                        <thead>
                                            <tr>
                                                <th colspan="6">Resumo da fatura</th>
                                            </tr>
                                            <tr>
                                                <th>Dia do mês</th>
                                                <th>Energia consumida</th>
                                                <th>Energia gerada</th>
                                                <th>Energia comprada</th>
                                                <th>Valor Energia Comprada </th>
                                                <th>Economia na geração</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <tr><td>Total</td><td>116,7 kWh</td><td>32,22 kWh</td><td>84,48 kWh</td><td>R$ 130,02</td><td>R$ 49,47</td></tr>
                                            </tr>                                                                                                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                        <div class="row-fluid">
                            <div class="span12">                                                   
                                <table class="table table-striped table-bordered table-condensed">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Período</th>
                                            <th colspan="3">Consumo em (kWh)</th>
                                            <th colspan="2">Valores em R$</th>
                                            <th></th>
                                        </tr>                                         
                                        <tr>
                                            <th>Hora</th>
                                            <th>Dia do mês</th>
                                            <th>Energia consumida</th>
                                            <th>Energia gerada</th>
                                            <th>Energia comprada</th>
                                            <th>Valor Energia Comprada </th>
                                            <th>Economia na geração</th>
                                            <th>Tipo de tarifa
                                    </thead>
                                    <tbody>
                                        <tr><td>00:00:00</td><td>1/02/2013 </td><td>7,69</td><td>1,5 </td><td>6,19 </td><td>9,53 </td><td>2,31</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>2/02/2013 </td><td>1,7 </td><td>0,39</td><td>1,31 </td><td>2,01 </td><td>0,6 </td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>3/02/2013 </td><td>2,9 </td><td>0,71</td><td>2,19 </td><td>3,37 </td><td>1,09</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>4/02/2013 </td><td>3,34</td><td>1,26</td><td>2,08 </td><td>3,2  </td><td>1,94</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>5/02/2013 </td><td>9,82</td><td>1,52</td><td>8,3  </td><td>12,78</td><td>2,34</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>6/02/2013 </td><td>3,12</td><td>0,1 </td><td>3,02 </td><td>4,65 </td><td>0,15</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>7/02/2013 </td><td>0,03</td><td>1,81</td><td>-1,78</td><td>-2,74</td><td>2,78</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>8/02/2013 </td><td>0,91</td><td>0,11</td><td>0,8  </td><td>1,23 </td><td>0,16</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>9/02/2013 </td><td>3,5 </td><td>1,93</td><td>1,57 </td><td>2,41 </td><td>2,97</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>10/02/2013</td><td>3,45</td><td>1,17</td><td>2,28 </td><td>3,51 </td><td>1,8 </td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>11/02/2013</td><td>1,85</td><td>0,73</td><td>1,12 </td><td>1,72 </td><td>1,12</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>12/02/2013</td><td>1,56</td><td>1,58</td><td>-0,02</td><td>-0,03</td><td>2,43</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>13/02/2013</td><td>0,34</td><td>0,49</td><td>-0,15</td><td>-0,23</td><td>0,75</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>14/02/2013</td><td>6,75</td><td>0,05</td><td>6,7  </td><td>10,31</td><td>0,07</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>15/02/2013</td><td>5,9 </td><td>0,74</td><td>5,16 </td><td>7,94 </td><td>1,13</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>16/02/2013</td><td>3,17</td><td>0,35</td><td>2,82 </td><td>4,34 </td><td>0,53</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>17/02/2013</td><td>0,98</td><td>0,98</td><td>0    </td><td>0    </td><td>1,5 </td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>18/02/2013</td><td>0,99</td><td>0,77</td><td>0,22 </td><td>0,33 </td><td>1,18</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>19/02/2013</td><td>6,75</td><td>1,22</td><td>5,53 </td><td>8,51 </td><td>1,87</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>20/02/2013</td><td>2,95</td><td>1,79</td><td>1,16 </td><td>1,78 </td><td>2,75</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>21/02/2013</td><td>3,01</td><td>1,84</td><td>1,17 </td><td>1,8  </td><td>2,83</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>22/02/2013</td><td>7,18</td><td>0,45</td><td>6,73 </td><td>10,36</td><td>0,69</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>23/02/2013</td><td>0,51</td><td>0,97</td><td>-0,46</td><td>-0,7 </td><td>1,49</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>24/02/2013</td><td>6,5 </td><td>1,43</td><td>5,07 </td><td>7,8  </td><td>2,2 </td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>25/02/2013</td><td>8,3 </td><td>1,85</td><td>6,45 </td><td>9,93 </td><td>2,84</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>26/02/2013</td><td>1,7 </td><td>0,58</td><td>1,12 </td><td>1,72 </td><td>0,89</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>27/02/2013</td><td>5,79</td><td>1,02</td><td>4,77 </td><td>7,34 </td><td>1,57</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>28/02/2013</td><td>8,18</td><td>1,92</td><td>6,26 </td><td>9,64 </td><td>2,95</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>29/02/2013</td><td>7,62</td><td>1,35</td><td>6,27 </td><td>9,65 </td><td>2,07</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>30/02/2013</td><td>0,13</td><td>1,31</td><td>-1,18</td><td>-1,81</td><td>2,01</td><td>Mista</td></tr>
                                        <tr><td>00:00:00</td><td>31/02/2013</td><td>0,08</td><td>0,3 </td><td>-0,22</td><td>-0,33</td><td>0,46</td><td>Mista</td></tr>
                                    </tbody>
                                    <tfooter>
                                        <tr><td colspan="2">Total</td><td>116,7</td><td>32,22</td><td>84,48</td><td>130,02</td><td>49,47</td><td></td></tr>

                                    </tfooter>
                                </table>
                            </div>
                        </div>
                        
                    </div>  
 
                                                                                                                                       
                                     
                </div>
            </div>
            
<?php show_sidebar(); ?>
<?php show_footer(); ?>
<!-- datepicker -->
<script src="lib/datepicker/bootstrap-datepicker.min.js"></script>
<!-- styled form elements -->
<script src="lib/uniform/jquery.uniform.min.js"></script>

<!-- highstock charts --> 
<script src="lib/highcharts/stock/highstock.js"></script>
<script src="lib/highcharts/stock/highcharts-more.js"></script>
<script src="lib/highcharts/stock/modules/exporting.js"></script>

<!-- specific JSs -->             
<script src="js/smart/minhaContaRelatorioCompra.js"></script>


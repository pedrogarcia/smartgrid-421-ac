--
-- PostgreSQL database dump
--

-- Dumped from database version 9.2.4
-- Dumped by pg_dump version 9.2.4
-- Started on 2013-06-01 06:54:21

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 170 (class 3079 OID 11727)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1929 (class 0 OID 0)
-- Dependencies: 170
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 169 (class 1259 OID 16397)
-- Name: measures; Type: TABLE; Schema: public; Owner: smartgrid; Tablespace: 
--

CREATE TABLE measures (
    id integer NOT NULL,
    local_time character varying,
    meter_name character varying,
    "FatorDePotencia" numeric,
    "Frequencia" numeric,
    "MaximaDemandaAtivaRegistrada" numeric,
    "MaximaDemandaReativaRegistrada" numeric,
    "MeterPotenciaAtivaInstantanea" numeric,
    "PotenciaAparenteInstantanea" numeric,
    "PotenciaAtivaConsumida" numeric,
    "PotenciaReativaConsumida" numeric,
    "PotenciaReativaInstantanea" numeric,
    "PotenciaReativaReversaConsumida" numeric,
    "PotenciaReversaConsumida" numeric,
    "RCorrenteDeFase" numeric,
    "RPotenciaAtiva" numeric,
    "RPotenciaReativa" numeric,
    "RTensaoDeFase" numeric,
    "SCorrenteDeFase" numeric,
    "SPotenciaAtiva" numeric,
    "SPotenciaReativa" numeric,
    "STensaoDeFase" numeric,
    "TCorrenteDeFase" numeric,
    "TPotenciaAtiva" numeric,
    "TPotenciaReativa" numeric,
    "TTensaoDeFase" numeric,
    address character varying
);


ALTER TABLE public.measures OWNER TO smartgrid;

--
-- TOC entry 168 (class 1259 OID 16395)
-- Name: measures_id_seq; Type: SEQUENCE; Schema: public; Owner: smartgrid
--

CREATE SEQUENCE measures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.measures_id_seq OWNER TO smartgrid;

--
-- TOC entry 1930 (class 0 OID 0)
-- Dependencies: 168
-- Name: measures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: smartgrid
--

ALTER SEQUENCE measures_id_seq OWNED BY measures.id;


--
-- TOC entry 1917 (class 2604 OID 16400)
-- Name: id; Type: DEFAULT; Schema: public; Owner: smartgrid
--

ALTER TABLE ONLY measures ALTER COLUMN id SET DEFAULT nextval('measures_id_seq'::regclass);


--
-- TOC entry 1921 (class 0 OID 16397)
-- Dependencies: 169
-- Data for Name: measures; Type: TABLE DATA; Schema: public; Owner: smartgrid
--

COPY measures (id, local_time, meter_name, "FatorDePotencia", "Frequencia", "MaximaDemandaAtivaRegistrada", "MaximaDemandaReativaRegistrada", "MeterPotenciaAtivaInstantanea", "PotenciaAparenteInstantanea", "PotenciaAtivaConsumida", "PotenciaReativaConsumida", "PotenciaReativaInstantanea", "PotenciaReativaReversaConsumida", "PotenciaReversaConsumida", "RCorrenteDeFase", "RPotenciaAtiva", "RPotenciaReativa", "RTensaoDeFase", "SCorrenteDeFase", "SPotenciaAtiva", "SPotenciaReativa", "STensaoDeFase", "TCorrenteDeFase", "TPotenciaAtiva", "TPotenciaReativa", "TTensaoDeFase", address) FROM stdin;
\.


--
-- TOC entry 1931 (class 0 OID 0)
-- Dependencies: 168
-- Name: measures_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smartgrid
--

SELECT pg_catalog.setval('measures_id_seq', 1939, true);


--
-- TOC entry 1919 (class 2606 OID 16402)
-- Name: pk_measures; Type: CONSTRAINT; Schema: public; Owner: smartgrid; Tablespace: 
--

ALTER TABLE ONLY measures
    ADD CONSTRAINT pk_measures PRIMARY KEY (id);


--
-- TOC entry 1928 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2013-06-01 06:54:21

--
-- PostgreSQL database dump complete
--


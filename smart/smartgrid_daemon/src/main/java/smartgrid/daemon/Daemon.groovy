package smartgrid.daemon

import smartgrid.daemon.dao.NodesDAO
import smartgrid.daemon.services.NodeCrawler

/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 5/30/13
 * Time: 7:11 PM
 */
class Daemon {
  public static void main(String[] args) {
    def nodes = NodesDAO.nodesList
    def nodesCrawlers = nodes.collect {
      new NodeCrawler(it)
    }
    nodesCrawlers.each {
      it.run()
    }
  }
}

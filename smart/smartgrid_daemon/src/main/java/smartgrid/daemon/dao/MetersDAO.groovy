package smartgrid.daemon.dao

import smartgrid.daemon.model.Meter

/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 5/31/13
 * Time: 11:40 PM
 */
@Singleton
class MetersDAO extends DAO {
  public static List<Meter> getNodeMetersList(Integer nodeId) {
    def sql = MetersDAO.instance.sql
    def meters = sql.rows("""SELECT * FROM "Meters" WHERE "Node"=${nodeId};""")
    def nodeMeters = meters.collect { meter ->
      new Meter(
          id: meter.id,
          node: meter.Node,
          port: meter.Port,
          name: meter.Name,
          address: meter.Address
      )
    }
    return nodeMeters
  }

  public static Integer getMeterIdByNameAndAddress(String name, String address) {
    def sql = MetersDAO.instance.sql
    def id = sql.firstRow("""SELECT id FROM "Meters" WHERE "Name"=${name} AND "Address"=${address}; """)
    return id.id
  }
}

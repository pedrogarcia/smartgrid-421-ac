package smartgrid.daemon.dao

import groovy.sql.Sql
import smartgrid.daemon.db.SqlInstance

/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 5/31/13
 * Time: 9:31 PM
 */
abstract class DAO {
  Sql sql = SqlInstance.sqlInstance
}

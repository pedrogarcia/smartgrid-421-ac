package smartgrid.daemon.dao

import smartgrid.daemon.model.NodePort

/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 5/31/13
 * Time: 11:28 PM
 */
@Singleton
class NodePortsDAO extends DAO {
  public static List<NodePort> getNodePortsList(Integer nodeId) {
    def sql = NodePortsDAO.instance.sql
    def ports = sql.rows("""SELECT * FROM "Node_Ports" WHERE "Node"=${nodeId};""")
    def nodePorts = ports.collect { port ->
      new NodePort(
          id: port.id,
          portID: port.PortID.trim()
      )
    }
    return nodePorts
  }
}

package smartgrid.daemon.dao

import smartgrid.daemon.model.Node

/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 5/31/13
 * Time: 10:46 PM
 */
@Singleton
class NodesDAO extends DAO {
  public static List<Node> getNodesList() {
    def sql = NodesDAO.instance.sql
    def nodes = sql.rows("""SELECT * FROM "Nodes";""")
    def nodesList = nodes.collect { node ->
      new Node(
          id: node.id,
          ip: node.ip,
          name: node.name,
          ports: NodePortsDAO.getNodePortsList(node.id),
          meters: MetersDAO.getNodeMetersList(node.id)
      )
    }
    return nodesList
  }
}

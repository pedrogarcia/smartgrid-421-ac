package smartgrid.daemon.dao

import smartgrid.daemon.model.Measure

/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 5/31/13
 * Time: 9:31 PM
 */
@Singleton
class MeasuresDAO extends DAO {
  public static void insertMeasure(Measure measure) {
    def sql = MeasuresDAO.instance.sql
    def query = """
      INSERT INTO
        "Measures"
        (
          "Meter",
          "Address",
          "TimeRegisterLocal",
          "TimeRegisterGlobal",
          "FatorDePotencia",
          "Frequencia",
          "MaximaDemandaAtivaRegistrada",
          "MaximaDemandaReativaRegistrada",
          "MeterPotenciaAtivaInstantanea",
          "PotenciaAparenteInstantanea",
          "PotenciaAtivaConsumida",
          "PotenciaReativaConsumida",
          "PotenciaReativaInstantanea",
          "PotenciaReativaReversaConsumida",
          "PotenciaReversaConsumida",
          "RCorrenteDeFase",
          "RPotenciaAtiva",
          "RPotenciaReativa",
          "RTensaoDeFase",
          "SCorrenteDeFase",
          "SPotenciaAtiva",
          "SPotenciaReativa",
          "STensaoDeFase",
          "TCorrenteDeFase",
          "TPotenciaAtiva",
          "TPotenciaReativa",
          "TTensaoDeFase"
        )
      VALUES
        (
          ${measure.meterId},
          ${measure.address},
          ${measure.timeRegisterLocal},
          ${measure.timeRegisterGlobal},
          ${measure.fatorDePotencia},
          ${measure.frequencia},
          ${measure.maximaDemandaAtivaRegistrada},
          ${measure.maximaDemandaReativaRegistrada},
          ${measure.potenciaAparenteInstantanea},
          ${measure.potenciaAtivaConsumida},
          ${measure.potenciaReativaConsumida},
          ${measure.potenciaReativaInstantanea},
          ${measure.potenciaReativaReversaConsumida},
          ${measure.potenciaReativaReversaConsumida},
          ${measure.potenciaReversaConsumida},
          ${measure.rCorrenteDeFase},
          ${measure.rPotenciaAtiva},
          ${measure.rPotenciaReativa},
          ${measure.rTensaoDeFase},
          ${measure.sCorrenteDeFase},
          ${measure.sPotenciaAtiva},
          ${measure.sPotenciaReativa},
          ${measure.sTensaoDeFase},
          ${measure.tCorrenteDeFase},
          ${measure.tPotenciaAtiva},
          ${measure.tPotenciaReativa},
          ${measure.tTensaoDeFase}
        )
    """
    sql.execute(query)
  }

  public static void insertListOfMeasures(List<Measure> measures) {
    measures.each {
      insertMeasure(it)
    }
  }
}

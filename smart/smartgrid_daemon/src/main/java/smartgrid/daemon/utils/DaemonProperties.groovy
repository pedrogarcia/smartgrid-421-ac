package smartgrid.daemon.utils

import groovy.json.JsonSlurper

/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 5/31/13
 * Time: 7:13 PM
 */
@Singleton
class DaemonProperties {
  def properties = readProperties()

  def readProperties() {
    def fileProperties = new File('properties.json').text
    return new JsonSlurper().parseText(fileProperties)
  }

  public static Integer getDaemonDelay() {
    return DaemonProperties.instance.properties.delay
  }

  public static Map getDb() {
    def db = [
        dbName: DaemonProperties.instance.properties.db,
        dbPort: DaemonProperties.instance.properties.dbPort,
        dbUserName: DaemonProperties.instance.properties.dbUserName,
        dbPassword: DaemonProperties.instance.properties.dbPassword,
        dbDriver: DaemonProperties.instance.properties.dbDriver,
        dbHost: DaemonProperties.instance.properties.dbHost
    ]
    return db
  }
}

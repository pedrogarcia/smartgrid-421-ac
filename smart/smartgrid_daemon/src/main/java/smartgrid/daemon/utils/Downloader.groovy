package smartgrid.daemon.utils

import groovyx.net.http.HTTPBuilder

import static groovyx.net.http.ContentType.JSON
import static groovyx.net.http.Method.GET

/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 5/31/13
 * Time: 7:36 PM
 */
class Downloader {
  private static final userAgent = 'Mozilla/5.0 Ubuntu/8.10 Firefox/3.0.4'
  private HTTPBuilder builder

  public Downloader(ip, port, query = "") {
    def link = "http://" + ip + ":" + port
    this.builder = new HTTPBuilder(link)
  }

  def getDownload() {
    this.builder.request(GET, JSON) {
      uri.path = '/status'
      headers.'User-Agent' = userAgent

      response.success = { resp, json ->
        return json
      }
    }
  }
}

package smartgrid.daemon.services

import smartgrid.daemon.dao.MeasuresDAO
import smartgrid.daemon.dao.MetersDAO
import smartgrid.daemon.model.Measure
import smartgrid.daemon.model.Node
import smartgrid.daemon.utils.DaemonProperties
import smartgrid.daemon.utils.Downloader

import java.text.SimpleDateFormat

/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 6/1/13
 * Time: 12:00 AM
 */
class NodeCrawler extends Thread {
  def Node node

  public NodeCrawler(Node n) {
    this.node = n
  }

  public void run() {
    def listenedPorts = node.ports.portID
    while (true) {
      def downloaders = listenedPorts.collect { port ->
        new Downloader(node.ip, port)
      }
      def downloads = downloaders.collect { it.download }
      def means = downloads.collect { port ->
        port.collect { measure ->
          measure
        }
      }.flatten()
      def measurations = means.collect {
        new Measure(
            address: it.address,
            meterId: MetersDAO.getMeterIdByNameAndAddress(it.meter_name, it.address),
            fatorDePotencia: it.FatorDePotencia,
            frequencia: it.Frequencia,
            maximaDemandaAtivaRegistrada: it.MaximaDemandaAtivaRegistrada,
            maximaDemandaReativaRegistrada: it.MaximaDemandaReativaRegistrada,
            potenciaAtivaInstantanea: it.MeterPotenciaAtivaInstantanea,
            potenciaAparenteInstantanea: it.PotenciaAparenteInstantanea,
            potenciaAtivaConsumida: it.PotenciaAtivaConsumida,
            potenciaReativaConsumida: it.PotenciaReativaConsumida,
            potenciaReativaInstantanea: it.PotenciaReativaInstantanea,
            potenciaReativaReversaConsumida: it.PotenciaReativaReversaConsumida,
            potenciaReversaConsumida: it.PotenciaReversaConsumida,
            rCorrenteDeFase: it.RCorrenteDeFase,
            rPotenciaAtiva: it.RPotenciaAtiva,
            rPotenciaReativa: it.RPotenciaReativa,
            rTensaoDeFase: it.RTensaoDeFase,
            sCorrenteDeFase: it.SCorrenteDeFase,
            sPotenciaAtiva: it.SPotenciaAtiva,
            sPotenciaReativa: it.SPotenciaReativa,
            sTensaoDeFase: it.STensaoDeFase,
            tCorrenteDeFase: it.TCorrenteDeFase,
            tPotenciaAtiva: it.TPotenciaAtiva,
            tPotenciaReativa: it.TPotenciaReativa,
            tTensaoDeFase: it.TTensaoDeFase,
            timeRegisterLocal: it.local_time,
            timeRegisterGlobal: getUTCDateString()
        )
      }
      MeasuresDAO.insertListOfMeasures(measurations)
      sleep(DaemonProperties.daemonDelay)
    }
  }

  private static String getUTCDateString() {
    def df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    def today = Calendar.instance.time
    def formattedDate = df.format(today)
    return formattedDate
  }
}

package smartgrid.daemon.model

import groovy.transform.ToString

/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 5/31/13
 * Time: 9:36 PM
 */
@ToString
class Node {
  Integer id
  String ip
  String name
  List<NodePort> ports
  List<Meter> meters
}

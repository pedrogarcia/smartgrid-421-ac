package smartgrid.daemon.model

import groovy.transform.ToString

/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 5/31/13
 * Time: 9:35 PM
 */
@ToString
class Measure {
  Integer id
  String address
  Integer meterId
  Float fatorDePotencia
  Float frequencia
  Float maximaDemandaAtivaRegistrada
  Float maximaDemandaReativaRegistrada
  Float potenciaAtivaInstantanea
  Float potenciaAparenteInstantanea
  Float potenciaAtivaConsumida
  Float potenciaReativaConsumida
  Float potenciaReativaInstantanea
  Float potenciaReativaReversaConsumida
  Float potenciaReversaConsumida
  Float rCorrenteDeFase
  Float rPotenciaAtiva
  Float rPotenciaReativa
  Float rTensaoDeFase
  Float sCorrenteDeFase
  Float sPotenciaAtiva
  Float sPotenciaReativa
  Float sTensaoDeFase
  Float tCorrenteDeFase
  Float tPotenciaAtiva
  Float tPotenciaReativa
  Float tTensaoDeFase
  String timeRegisterLocal
  String timeRegisterGlobal
}

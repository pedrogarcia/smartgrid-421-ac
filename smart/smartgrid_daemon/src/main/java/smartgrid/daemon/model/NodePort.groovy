package smartgrid.daemon.model

import groovy.transform.ToString

/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 5/31/13
 * Time: 9:37 PM
 */
@ToString
class NodePort {
  Integer id
  String portID
}

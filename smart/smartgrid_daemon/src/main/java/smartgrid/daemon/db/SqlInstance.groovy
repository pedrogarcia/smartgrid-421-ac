package smartgrid.daemon.db
/**
 * User: pedro
 * Date: 03/11/12
 * Time: 12:47
 */
import groovy.sql.Sql
import smartgrid.daemon.utils.DaemonProperties;

class SqlInstance {
  private static final INSTANCE = new SqlInstance()

  private static final dbServer = "jdbc:postgresql://${DaemonProperties.db.dbHost}:"
  private static final dbPort = "${DaemonProperties.db.dbPort}"
  private static final db = "${DaemonProperties.db.dbName}"
  private static final dbConn = dbServer + dbPort + "/" + db
  private static final dbUserName = "${DaemonProperties.db.dbUserName}"
  private static final dbPassword = "${DaemonProperties.db.dbPassword}"
  private static final dbDriver = "${DaemonProperties.db.dbDriver}"
  private static Sql sql = Sql.newInstance(dbConn, dbUserName, dbPassword, dbDriver)

  private SqlInstance() {}

  static Sql getSqlInstance() {
    return sql
  }
}

--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.9
-- Dumped by pg_dump version 9.1.9
-- Started on 2013-06-01 07:03:16 BRT

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 1931 (class 1262 OID 33000)
-- Name: smartgrid_central; Type: DATABASE; Schema: -; Owner: smartgrid
--

CREATE DATABASE smartgrid_central WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE smartgrid_central OWNER TO smartgrid;

\connect smartgrid_central

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 169 (class 3079 OID 11676)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1934 (class 0 OID 0)
-- Dependencies: 169
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 168 (class 1259 OID 33042)
-- Dependencies: 5
-- Name: Measures; Type: TABLE; Schema: public; Owner: smartgrid; Tablespace: 
--

CREATE TABLE "Measures" (
    id integer NOT NULL,
    "Meter" integer,
    "TimeRegisterLocal" character varying(45),
    "TimeRegisterGlobal" character varying(45),
    "FatorDePotencia" numeric,
    "Frequencia" numeric,
    "MaximaDemandaAtivaRegistrada" numeric,
    "MaximaDemandaReativaRegistrada" numeric,
    "MeterPotenciaAtivaInstantanea" numeric,
    "PotenciaAparenteInstantanea" numeric,
    "PotenciaAtivaConsumida" numeric,
    "PotenciaReativaConsumida" numeric,
    "PotenciaReativaInstantanea" numeric,
    "PotenciaReativaReversaConsumida" numeric,
    "PotenciaReversaConsumida" numeric,
    "RCorrenteDeFase" numeric,
    "RPotenciaAtiva" numeric,
    "RPotenciaReativa" numeric,
    "RTensaoDeFase" numeric,
    "SCorrenteDeFase" numeric,
    "SPotenciaAtiva" numeric,
    "SPotenciaReativa" numeric,
    "STensaoDeFase" numeric,
    "TCorrenteDeFase" numeric,
    "TPotenciaAtiva" numeric,
    "TPotenciaReativa" numeric,
    "TTensaoDeFase" numeric,
    "Address" character varying
);


ALTER TABLE public."Measures" OWNER TO smartgrid;

--
-- TOC entry 167 (class 1259 OID 33040)
-- Dependencies: 168 5
-- Name: Measures_id_seq; Type: SEQUENCE; Schema: public; Owner: smartgrid
--

CREATE SEQUENCE "Measures_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Measures_id_seq" OWNER TO smartgrid;

--
-- TOC entry 1935 (class 0 OID 0)
-- Dependencies: 167
-- Name: Measures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: smartgrid
--

ALTER SEQUENCE "Measures_id_seq" OWNED BY "Measures".id;


--
-- TOC entry 166 (class 1259 OID 33024)
-- Dependencies: 5
-- Name: Meters; Type: TABLE; Schema: public; Owner: smartgrid; Tablespace: 
--

CREATE TABLE "Meters" (
    id integer NOT NULL,
    "Node" integer,
    "Port" integer,
    "Name" character varying(45),
    "Address" character varying(45)
);


ALTER TABLE public."Meters" OWNER TO smartgrid;

--
-- TOC entry 165 (class 1259 OID 33022)
-- Dependencies: 5 166
-- Name: Meters_id_seq; Type: SEQUENCE; Schema: public; Owner: smartgrid
--

CREATE SEQUENCE "Meters_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Meters_id_seq" OWNER TO smartgrid;

--
-- TOC entry 1936 (class 0 OID 0)
-- Dependencies: 165
-- Name: Meters_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: smartgrid
--

ALTER SEQUENCE "Meters_id_seq" OWNED BY "Meters".id;


--
-- TOC entry 164 (class 1259 OID 33011)
-- Dependencies: 5
-- Name: Node_Ports; Type: TABLE; Schema: public; Owner: smartgrid; Tablespace: 
--

CREATE TABLE "Node_Ports" (
    id integer NOT NULL,
    "Node" integer,
    "PortID" character(45)
);


ALTER TABLE public."Node_Ports" OWNER TO smartgrid;

--
-- TOC entry 163 (class 1259 OID 33009)
-- Dependencies: 5 164
-- Name: Node_Ports_id_seq; Type: SEQUENCE; Schema: public; Owner: smartgrid
--

CREATE SEQUENCE "Node_Ports_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Node_Ports_id_seq" OWNER TO smartgrid;

--
-- TOC entry 1937 (class 0 OID 0)
-- Dependencies: 163
-- Name: Node_Ports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: smartgrid
--

ALTER SEQUENCE "Node_Ports_id_seq" OWNED BY "Node_Ports".id;


--
-- TOC entry 162 (class 1259 OID 33003)
-- Dependencies: 5
-- Name: Nodes; Type: TABLE; Schema: public; Owner: smartgrid; Tablespace: 
--

CREATE TABLE "Nodes" (
    id integer NOT NULL,
    ip character varying(45),
    name character varying(45)
);


ALTER TABLE public."Nodes" OWNER TO smartgrid;

--
-- TOC entry 161 (class 1259 OID 33001)
-- Dependencies: 162 5
-- Name: Nodes_id_seq; Type: SEQUENCE; Schema: public; Owner: smartgrid
--

CREATE SEQUENCE "Nodes_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Nodes_id_seq" OWNER TO smartgrid;

--
-- TOC entry 1938 (class 0 OID 0)
-- Dependencies: 161
-- Name: Nodes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: smartgrid
--

ALTER SEQUENCE "Nodes_id_seq" OWNED BY "Nodes".id;


--
-- TOC entry 1906 (class 2604 OID 33045)
-- Dependencies: 168 167 168
-- Name: id; Type: DEFAULT; Schema: public; Owner: smartgrid
--

ALTER TABLE ONLY "Measures" ALTER COLUMN id SET DEFAULT nextval('"Measures_id_seq"'::regclass);


--
-- TOC entry 1905 (class 2604 OID 33027)
-- Dependencies: 165 166 166
-- Name: id; Type: DEFAULT; Schema: public; Owner: smartgrid
--

ALTER TABLE ONLY "Meters" ALTER COLUMN id SET DEFAULT nextval('"Meters_id_seq"'::regclass);


--
-- TOC entry 1904 (class 2604 OID 33014)
-- Dependencies: 163 164 164
-- Name: id; Type: DEFAULT; Schema: public; Owner: smartgrid
--

ALTER TABLE ONLY "Node_Ports" ALTER COLUMN id SET DEFAULT nextval('"Node_Ports_id_seq"'::regclass);


--
-- TOC entry 1903 (class 2604 OID 33006)
-- Dependencies: 161 162 162
-- Name: id; Type: DEFAULT; Schema: public; Owner: smartgrid
--

ALTER TABLE ONLY "Nodes" ALTER COLUMN id SET DEFAULT nextval('"Nodes_id_seq"'::regclass);


--
-- TOC entry 1926 (class 0 OID 33042)
-- Dependencies: 168 1927
-- Data for Name: Measures; Type: TABLE DATA; Schema: public; Owner: smartgrid
--

COPY "Measures" (id, "Meter", "TimeRegisterLocal", "TimeRegisterGlobal", "FatorDePotencia", "Frequencia", "MaximaDemandaAtivaRegistrada", "MaximaDemandaReativaRegistrada", "MeterPotenciaAtivaInstantanea", "PotenciaAparenteInstantanea", "PotenciaAtivaConsumida", "PotenciaReativaConsumida", "PotenciaReativaInstantanea", "PotenciaReativaReversaConsumida", "PotenciaReversaConsumida", "RCorrenteDeFase", "RPotenciaAtiva", "RPotenciaReativa", "RTensaoDeFase", "SCorrenteDeFase", "SPotenciaAtiva", "SPotenciaReativa", "STensaoDeFase", "TCorrenteDeFase", "TPotenciaAtiva", "TPotenciaReativa", "TTensaoDeFase", "Address") FROM stdin;
1	1	2013-06-01 02:57:00	2013-06-01 02:57:37	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.9	0	0	0	0	0	0	0	0	090832
2	1	2013-06-01 02:57:19	2013-06-01 02:57:37	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221	0	0	0	0	0	0	0	0	090832
3	1	2013-06-01 02:57:38	2013-06-01 02:57:37	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.8	0	0	0	0	0	0	0	0	090832
4	1	2013-06-01 02:57:57	2013-06-01 02:58:26	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.8	0	0	0	0	0	0	0	0	090832
5	1	2013-06-01 02:58:15	2013-06-01 02:58:26	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221.1	0	0	0	0	0	0	0	0	090832
6	1	2013-06-01 02:58:34	2013-06-01 03:07:10	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221	0	0	0	0	0	0	0	0	090832
7	1	2013-06-01 02:58:53	2013-06-01 03:07:10	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.8	0	0	0	0	0	0	0	0	090832
8	1	2013-06-01 02:59:12	2013-06-01 03:07:10	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221	0	0	0	0	0	0	0	0	090832
9	1	2013-06-01 02:59:31	2013-06-01 03:07:10	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221.3	0	0	0	0	0	0	0	0	090832
10	1	2013-06-01 02:59:49	2013-06-01 03:07:10	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221.3	0	0	0	0	0	0	0	0	090832
11	1	2013-06-01 03:00:08	2013-06-01 03:07:10	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221.2	0	0	0	0	0	0	0	0	090832
12	1	2013-06-01 03:00:27	2013-06-01 03:07:10	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221	0	0	0	0	0	0	0	0	090832
13	1	2013-06-01 03:00:46	2013-06-01 03:07:10	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221.1	0	0	0	0	0	0	0	0	090832
14	1	2013-06-01 03:01:04	2013-06-01 03:07:10	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221	0	0	0	0	0	0	0	0	090832
15	1	2013-06-01 03:01:23	2013-06-01 03:07:10	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.8	0	0	0	0	0	0	0	0	090832
16	1	2013-06-01 03:01:42	2013-06-01 03:07:10	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221	0	0	0	0	0	0	0	0	090832
17	1	2013-06-01 03:02:01	2013-06-01 03:07:10	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.6	0	0	0	0	0	0	0	0	090832
18	1	2013-06-01 03:02:20	2013-06-01 03:07:10	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.8	0	0	0	0	0	0	0	0	090832
19	1	2013-06-01 03:02:38	2013-06-01 03:07:10	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.6	0	0	0	0	0	0	0	0	090832
20	1	2013-06-01 03:02:57	2013-06-01 03:07:10	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	219.9	0	0	0	0	0	0	0	0	090832
21	1	2013-06-01 03:03:16	2013-06-01 03:07:10	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220	0	0	0	0	0	0	0	0	090832
22	1	2013-06-01 03:03:35	2013-06-01 03:07:10	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	219.9	0	0	0	0	0	0	0	0	090832
23	1	2013-06-01 03:03:54	2013-06-01 03:07:10	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	219.8	0	0	0	0	0	0	0	0	090832
24	1	2013-06-01 03:04:12	2013-06-01 03:07:10	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	219.7	0	0	0	0	0	0	0	0	090832
25	1	2013-06-01 03:04:31	2013-06-01 03:07:10	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	219.8	0	0	0	0	0	0	0	0	090832
26	1	2013-06-01 03:04:50	2013-06-01 03:07:10	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220	0	0	0	0	0	0	0	0	090832
27	1	2013-06-01 03:05:09	2013-06-01 03:07:10	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.2	0	0	0	0	0	0	0	0	090832
28	1	2013-06-01 03:05:27	2013-06-01 03:07:10	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.4	0	0	0	0	0	0	0	0	090832
29	1	2013-06-01 03:05:46	2013-06-01 03:07:10	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221.1	0	0	0	0	0	0	0	0	090832
30	1	2013-06-01 03:06:05	2013-06-01 03:07:10	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221.2	0	0	0	0	0	0	0	0	090832
31	1	2013-06-01 03:06:24	2013-06-01 03:07:10	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221.2	0	0	0	0	0	0	0	0	090832
32	1	2013-06-01 03:06:43	2013-06-01 03:07:10	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221.1	0	0	0	0	0	0	0	0	090832
33	1	2013-06-01 03:07:01	2013-06-01 03:07:10	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221	0	0	0	0	0	0	0	0	090832
34	1	2013-06-01 03:07:20	2013-06-01 03:07:20	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221	0	0	0	0	0	0	0	0	090832
35	1	2013-06-01 03:07:39	2013-06-01 03:07:40	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221.2	0	0	0	0	0	0	0	0	090832
36	1	2013-06-01 03:07:58	2013-06-01 03:08:22	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221.3	0	0	0	0	0	0	0	0	090832
37	1	2013-06-01 03:08:16	2013-06-01 03:08:22	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221.2	0	0	0	0	0	0	0	0	090832
38	1	2013-06-01 03:08:35	2013-06-01 03:09:22	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.6	0	0	0	0	0	0	0	0	090832
39	1	2013-06-01 03:08:54	2013-06-01 03:09:22	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.7	0	0	0	0	0	0	0	0	090832
40	1	2013-06-01 03:09:13	2013-06-01 03:09:22	0.999	81.91	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.7	0	0	0	0	0	0	0	0	090832
41	1	2013-06-01 03:09:32	2013-06-01 03:10:22	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.7	0	0	0	0	0	0	0	0	090832
42	1	2013-06-01 03:09:50	2013-06-01 03:10:22	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.6	0	0	0	0	0	0	0	0	090832
43	1	2013-06-01 03:10:09	2013-06-01 03:10:22	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.8	0	0	0	0	0	0	0	0	090832
44	1	2013-06-01 03:10:28	2013-06-01 03:10:22	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.9	0	0	0	0	0	0	0	0	090832
45	1	2013-06-01 04:08:58	2013-06-01 04:20:49	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.7	0	0	0	0	0	0	0	0	090832
46	1	2013-06-01 04:09:17	2013-06-01 04:20:49	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.7	0	0	0	0	0	0	0	0	090832
47	1	2013-06-01 04:09:35	2013-06-01 04:20:49	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.6	0	0	0	0	0	0	0	0	090832
48	1	2013-06-01 04:09:54	2013-06-01 04:20:49	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.9	0	0	0	0	0	0	0	0	090832
49	1	2013-06-01 04:10:13	2013-06-01 04:20:49	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.4	0	0	0	0	0	0	0	0	090832
50	1	2013-06-01 04:10:32	2013-06-01 04:20:49	0.999	81.91	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.3	0	0	0	0	0	0	0	0	090832
51	1	2013-06-01 04:10:50	2013-06-01 04:20:49	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.3	0	0	0	0	0	0	0	0	090832
52	1	2013-06-01 04:11:09	2013-06-01 04:20:49	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.6	0	0	0	0	0	0	0	0	090832
53	1	2013-06-01 04:11:29	2013-06-01 04:20:49	0.999	60.02	0	0	0	0	1.6	0	0	0	0	0	0	0	220.7	0	0	0	0	0	0	0	0	090832
54	1	2013-06-01 04:11:48	2013-06-01 04:20:49	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.7	0	0	0	0	0	0	0	0	090832
55	1	2013-06-01 04:12:06	2013-06-01 04:20:49	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.7	0	0	0	0	0	0	0	0	090832
56	1	2013-06-01 04:12:25	2013-06-01 04:20:49	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.8	0	0	0	0	0	0	0	0	090832
57	1	2013-06-01 04:12:44	2013-06-01 04:20:49	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.7	0	0	0	0	0	0	0	0	090832
58	1	2013-06-01 04:13:03	2013-06-01 04:20:49	0.999	59.95	0	0	0	0	1.6	0	0	0	0	0	0	0	220.7	0	0	0	0	0	0	0	0	090832
59	1	2013-06-01 04:13:22	2013-06-01 04:20:49	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221.5	0	0	0	0	0	0	0	0	090832
60	1	2013-06-01 04:13:41	2013-06-01 04:20:49	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221.3	0	0	0	0	0	0	0	0	090832
61	1	2013-06-01 04:14:00	2013-06-01 04:20:49	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.9	0	0	0	0	0	0	0	0	090832
62	1	2013-06-01 04:14:18	2013-06-01 04:20:49	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221	0	0	0	0	0	0	0	0	090832
63	1	2013-06-01 04:14:37	2013-06-01 04:20:49	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221.1	0	0	0	0	0	0	0	0	090832
64	1	2013-06-01 04:14:57	2013-06-01 04:20:49	0.999	60.02	0	0	0	0	1.6	0	0	0	0	0	0	0	220.9	0	0	0	0	0	0	0	0	090832
65	1	2013-06-01 04:15:16	2013-06-01 04:20:49	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221	0	0	0	0	0	0	0	0	090832
66	1	2013-06-01 04:15:34	2013-06-01 04:20:49	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.9	0	0	0	0	0	0	0	0	090832
67	1	2013-06-01 04:15:53	2013-06-01 04:20:49	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.7	0	0	0	0	0	0	0	0	090832
68	1	2013-06-01 04:16:12	2013-06-01 04:20:49	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.4	0	0	0	0	0	0	0	0	090832
69	1	2013-06-01 04:16:32	2013-06-01 04:20:49	0.999	60.02	0	0	0	0	1.6	0	0	0	0	0	0	0	220.4	0	0	0	0	0	0	0	0	090832
70	1	2013-06-01 04:16:51	2013-06-01 04:20:49	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.2	0	0	0	0	0	0	0	0	090832
71	1	2013-06-01 04:17:11	2013-06-01 04:20:49	0.999	59.95	0	0	0	0	1.6	0	0	0	0	0	0	0	220.4	0	0	0	0	0	0	0	0	090832
72	1	2013-06-01 04:17:30	2013-06-01 04:20:49	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.5	0	0	0	0	0	0	0	0	090832
73	1	2013-06-01 04:17:49	2013-06-01 04:20:49	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.7	0	0	0	0	0	0	0	0	090832
74	1	2013-06-01 04:18:08	2013-06-01 04:20:49	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.8	0	0	0	0	0	0	0	0	090832
75	1	2013-06-01 04:18:27	2013-06-01 04:20:49	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.9	0	0	0	0	0	0	0	0	090832
76	1	2013-06-01 04:18:47	2013-06-01 04:20:49	0.999	60.02	0	0	0	0	1.6	0	0	0	0	0	0	0	220.5	0	0	0	0	0	0	0	0	090832
77	1	2013-06-01 04:19:06	2013-06-01 04:20:49	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.9	0	0	0	0	0	0	0	0	090832
78	1	2013-06-01 04:19:24	2013-06-01 04:20:49	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221.3	0	0	0	0	0	0	0	0	090832
79	1	2013-06-01 04:19:44	2013-06-01 04:20:49	0.999	60.02	0	0	0	0	1.6	0	0	0	0	0	0	0	221.2	0	0	0	0	0	0	0	0	090832
80	1	2013-06-01 04:20:04	2013-06-01 04:20:49	0.999	60.02	0	0	0	0	1.6	0	0	0	0	0	0	0	220.9	0	0	0	0	0	0	0	0	090832
81	1	2013-06-01 04:20:24	2013-06-01 04:20:49	0.999	60.02	0	0	0	0	1.6	0	0	0	0	0	0	0	220.9	0	0	0	0	0	0	0	0	090832
82	1	2013-06-01 04:20:44	2013-06-01 04:20:49	0.999	59.95	0	0	0	0	1.6	0	0	0	0	0	0	0	221.2	0	0	0	0	0	0	0	0	090832
83	1	2013-06-01 04:21:04	2013-06-01 04:21:49	0.999	60.02	0	0	0	0	1.6	0	0	0	0	0	0	0	221	0	0	0	0	0	0	0	0	090832
84	1	2013-06-01 04:21:24	2013-06-01 04:21:49	0.999	60.09	0	0	0	0	1.6	0	0	0	0	0	0	0	221.1	0	0	0	0	0	0	0	0	090832
85	1	2013-06-01 04:21:44	2013-06-01 04:21:49	0.999	60.02	0	0	0	0	1.6	0	0	0	0	0	0	0	221	0	0	0	0	0	0	0	0	090832
86	1	2013-06-01 04:22:03	2013-06-01 04:25:21	0.999	59.95	0	0	0	0	1.6	0	0	0	0	0	0	0	220.8	0	0	0	0	0	0	0	0	090832
87	1	2013-06-01 04:22:25	2013-06-01 04:25:21	0.999	60.09	0	0	0	0	0	0	0	0	0	0	0	0	220.7	0	0	0	0	0	0	0	0	090832
88	1	2013-06-01 04:22:45	2013-06-01 04:25:21	0.999	59.95	0	0	0	0	1.6	0	0	0	0	0	0	0	220.8	0	0	0	0	0	0	0	0	090832
89	1	2013-06-01 04:23:04	2013-06-01 04:25:21	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.5	0	0	0	0	0	0	0	0	090832
90	1	2013-06-01 04:23:22	2013-06-01 04:25:21	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.6	0	0	0	0	0	0	0	0	090832
91	1	2013-06-01 04:23:41	2013-06-01 04:25:21	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.6	0	0	0	0	0	0	0	0	090832
92	1	2013-06-01 04:24:00	2013-06-01 04:25:21	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.8	0	0	0	0	0	0	0	0	090832
93	1	2013-06-01 04:24:19	2013-06-01 04:25:21	0.999	59.95	0	0	0	0	1.6	0	0	0	0	0	0	0	220.9	0	0	0	0	0	0	0	0	090832
94	1	2013-06-01 04:24:39	2013-06-01 04:25:21	0.999	60.02	0	0	0	0	1.6	0	0	0	0	0	0	0	220.9	0	0	0	0	0	0	0	0	090832
95	1	2013-06-01 04:24:58	2013-06-01 04:25:21	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	220.6	0	0	0	0	0	0	0	0	090832
96	1	2013-06-01 04:25:17	2013-06-01 04:25:21	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221.1	0	0	0	0	0	0	0	0	090832
97	1	2013-06-01 04:25:36	2013-06-01 04:26:22	0.999	60.02	0	0	0	0	1.6	0	0	0	0	0	0	0	221.4	0	0	0	0	0	0	0	0	090832
98	1	2013-06-01 04:25:55	2013-06-01 04:26:22	0.999	59.95	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221.4	0	0	0	0	0	0	0	0	090832
99	1	2013-06-01 04:26:14	2013-06-01 04:26:22	0.999	60.02	0	0	0	1.1	1.6	0	0	0	0	0	0	0	221.3	0	0	0	0	0	0	0	0	090832
\.


--
-- TOC entry 1939 (class 0 OID 0)
-- Dependencies: 167
-- Name: Measures_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smartgrid
--

SELECT pg_catalog.setval('"Measures_id_seq"', 99, true);


--
-- TOC entry 1924 (class 0 OID 33024)
-- Dependencies: 166 1927
-- Data for Name: Meters; Type: TABLE DATA; Schema: public; Owner: smartgrid
--

COPY "Meters" (id, "Node", "Port", "Name", "Address") FROM stdin;
1	1	1	Test	090832
\.


--
-- TOC entry 1940 (class 0 OID 0)
-- Dependencies: 165
-- Name: Meters_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smartgrid
--

SELECT pg_catalog.setval('"Meters_id_seq"', 1, true);


--
-- TOC entry 1922 (class 0 OID 33011)
-- Dependencies: 164 1927
-- Data for Name: Node_Ports; Type: TABLE DATA; Schema: public; Owner: smartgrid
--

COPY "Node_Ports" (id, "Node", "PortID") FROM stdin;
1	1	8080                                         
\.


--
-- TOC entry 1941 (class 0 OID 0)
-- Dependencies: 163
-- Name: Node_Ports_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smartgrid
--

SELECT pg_catalog.setval('"Node_Ports_id_seq"', 1, true);


--
-- TOC entry 1920 (class 0 OID 33003)
-- Dependencies: 162 1927
-- Data for Name: Nodes; Type: TABLE DATA; Schema: public; Owner: smartgrid
--

COPY "Nodes" (id, ip, name) FROM stdin;
1	164.41.10.172	Test computer
\.


--
-- TOC entry 1942 (class 0 OID 0)
-- Dependencies: 161
-- Name: Nodes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smartgrid
--

SELECT pg_catalog.setval('"Nodes_id_seq"', 1, true);


--
-- TOC entry 1914 (class 2606 OID 33050)
-- Dependencies: 168 168 1928
-- Name: pk_measures; Type: CONSTRAINT; Schema: public; Owner: smartgrid; Tablespace: 
--

ALTER TABLE ONLY "Measures"
    ADD CONSTRAINT pk_measures PRIMARY KEY (id);


--
-- TOC entry 1912 (class 2606 OID 33029)
-- Dependencies: 166 166 1928
-- Name: pk_meters; Type: CONSTRAINT; Schema: public; Owner: smartgrid; Tablespace: 
--

ALTER TABLE ONLY "Meters"
    ADD CONSTRAINT pk_meters PRIMARY KEY (id);


--
-- TOC entry 1910 (class 2606 OID 33016)
-- Dependencies: 164 164 1928
-- Name: pk_node_ports; Type: CONSTRAINT; Schema: public; Owner: smartgrid; Tablespace: 
--

ALTER TABLE ONLY "Node_Ports"
    ADD CONSTRAINT pk_node_ports PRIMARY KEY (id);


--
-- TOC entry 1908 (class 2606 OID 33008)
-- Dependencies: 162 162 1928
-- Name: pk_nodes; Type: CONSTRAINT; Schema: public; Owner: smartgrid; Tablespace: 
--

ALTER TABLE ONLY "Nodes"
    ADD CONSTRAINT pk_nodes PRIMARY KEY (id);


--
-- TOC entry 1918 (class 2606 OID 33064)
-- Dependencies: 1911 168 166 1928
-- Name: fk_measures_meters; Type: FK CONSTRAINT; Schema: public; Owner: smartgrid
--

ALTER TABLE ONLY "Measures"
    ADD CONSTRAINT fk_measures_meters FOREIGN KEY ("Meter") REFERENCES "Meters"(id);


--
-- TOC entry 1916 (class 2606 OID 33030)
-- Dependencies: 1909 166 164 1928
-- Name: fk_meters_node_ports; Type: FK CONSTRAINT; Schema: public; Owner: smartgrid
--

ALTER TABLE ONLY "Meters"
    ADD CONSTRAINT fk_meters_node_ports FOREIGN KEY ("Port") REFERENCES "Node_Ports"(id);


--
-- TOC entry 1917 (class 2606 OID 33035)
-- Dependencies: 166 162 1907 1928
-- Name: fk_meters_nodes; Type: FK CONSTRAINT; Schema: public; Owner: smartgrid
--

ALTER TABLE ONLY "Meters"
    ADD CONSTRAINT fk_meters_nodes FOREIGN KEY ("Node") REFERENCES "Nodes"(id);


--
-- TOC entry 1915 (class 2606 OID 33059)
-- Dependencies: 162 1907 164 1928
-- Name: pk_node_ports_nodes; Type: FK CONSTRAINT; Schema: public; Owner: smartgrid
--

ALTER TABLE ONLY "Node_Ports"
    ADD CONSTRAINT pk_node_ports_nodes FOREIGN KEY ("Node") REFERENCES "Nodes"(id);


--
-- TOC entry 1933 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2013-06-01 07:03:16 BRT

--
-- PostgreSQL database dump complete
--


package app

import Meter.MeterData
import groovy.json.JsonOutput

/**
 User: Pedro Garcia
 Mail: sawp@sawp.com.br
 Date: 18/04/13
 */
class MeterTest {
  public static void main(String[] args) {
    def meterData = new MeterData("Test", "090832")
    def data = meterData.getMeterData()
    println JsonOutput.prettyPrint(JsonOutput.toJson(data))
  }


  private void listPorts() {
    println "Portas disponiveis: " + commSerial.listPorts().join(', ')
  }
}

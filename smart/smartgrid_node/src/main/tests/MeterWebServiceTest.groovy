package app

import groovy.json.JsonOutput
import Meter.MeterData
import org.restlet.Server
import org.restlet.data.Protocol
import org.restlet.resource.Get
import org.restlet.resource.ServerResource
import org.restlet.Component

/**
 User: Pedro Garcia
 Mail: sawp@sawp.com.br
 Date: 22/04/13
 */
class MeterWebServiceTest extends ServerResource {
  public static void main(String[] args) {
    // Create a new Restlet component and add a HTTP server connector to it
    Component component = new Component()
    component.getServers().add(Protocol.HTTP, 8080)

    // Then attach it to the local host
    component.getDefaultHost().attach("/status", MeterWebServiceTest.class)

    // Now, let's start the component!
    // Note that the HTTP server connector is also automatically started.
    component.start()
  }

  @Get
  public String showBrowser() {
    def meterData = new MeterData("Test", "090832")
    def data = meterData.getMeterData()
    return JsonOutput.prettyPrint(JsonOutput.toJson(data))
  }
}

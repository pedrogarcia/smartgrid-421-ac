package app

import daemon.LocalDBDaemon
import groovy.json.JsonOutput
import org.restlet.Component
import org.restlet.data.Protocol
import org.restlet.resource.Get
import org.restlet.resource.ServerResource

/**
 * User: Pedro Garcia
 * Date: 30/05/13
 * Time: 22:13
*/
class NodeService extends ServerResource {
  public static void main(String[] args) {
    LocalDBDaemon.start()
    Component component = new Component()
    component.getServers().add(Protocol.HTTP, 8080)
    component.getDefaultHost().attach("/status", NodeService.class)
    component.start()
  }

  @Get
  public String showBrowser() {
    def measures = getDaemonMeasures()
    flushMeasures()
    return JsonOutput.prettyPrint(JsonOutput.toJson(measures))
  }

  public static List<Map> getDaemonMeasures() {
    return LocalDBDaemon.instance.getDBMeasures()
  }

  public static void flushMeasures() {
    LocalDBDaemon.instance.flushDBMeasures()
  }
}

package RS485

import Meter.*
import groovy.util.logging.Log
import utils.hex.HexConversor
import utils.hex.Hexadecimal

/**
 User: Pedro Garcia
 Mail: sawp@sawp.com.br
 Date: 15/04/13
 */

@Log
public class DLT645 {
  public static final String ERROR_MSG = "ERROR:0RWRsfRwwet0952442423"
  private static final byte START_BYTE = HexConversor.hexCharToByte("FE").byteValue()

  private static final byte STARTFRAME_BYTE = HexConversor.hexCharToByte("68").byteValue()
  private static final byte NORMALRESPONSE_READDATA_BYTE = HexConversor.hexCharToByte("81").byteValue()

  private static final byte MeterPotenciaAtivaConsumida_DI0_BYTE = HexConversor.hexCharToByte("43").byteValue()
  private static final byte MeterPotenciaAtivaConsumida_DI1_BYTE = HexConversor.hexCharToByte("C3").byteValue()

  private static final byte MeterPotenciaReversaConsumida_DI0_BYTE = HexConversor.hexCharToByte("53").byteValue()
  private static final byte MeterPotenciaReversaConsumida_DI1_BYTE = HexConversor.hexCharToByte("C3").byteValue()

  private static final byte MeterPotenciaReativaConsumida_DI0_BYTE = HexConversor.hexCharToByte("43").byteValue()
  private static final byte MeterPotenciaReativaConsumida_DI1_BYTE = HexConversor.hexCharToByte("C4").byteValue()

  private static final byte MeterPotenciaReativaReversaConsumida_DI0_BYTE = HexConversor.hexCharToByte("53").byteValue()
  private static final byte MeterPotenciaReativaReversaConsumida_DI1_BYTE = HexConversor.hexCharToByte("C4").byteValue()

  private static final byte MeterMaximaDemandaAtivaRegistrada_DI0_BYTE = HexConversor.hexCharToByte("43").byteValue()
  private static final byte MeterMaximaDemandaAtivaRegistrada_DI1_BYTE = HexConversor.hexCharToByte("D3").byteValue()

  private static final byte MeterMaximaDemandaReativaRegistrada_DI0_BYTE = HexConversor.hexCharToByte("43").byteValue()
  private static final byte MeterMaximaDemandaReativaRegistrada_DI1_BYTE = HexConversor.hexCharToByte("D4").byteValue()

  private static final byte MeterAddress_DI0_BYTE = HexConversor.hexCharToByte("65").byteValue()
  private static final byte MeterAddress_DI1_BYTE = HexConversor.hexCharToByte("F3").byteValue()

  private static final byte MeterPotenciaAtivaInstantanea_DI0_BYTE = HexConversor.hexCharToByte("63").byteValue()
  private static final byte MeterPotenciaAtivaInstantanea_DI1_BYTE = HexConversor.hexCharToByte("E9").byteValue()

  private static final byte MeterPotenciaReativaInstantanea_DI0_BYTE = HexConversor.hexCharToByte("73").byteValue()
  private static final byte MeterPotenciaReativaInstantanea_DI1_BYTE = HexConversor.hexCharToByte("E9").byteValue()

  private static final byte MeterPotenciaAparenteInstantanea_DI0_BYTE = HexConversor.hexCharToByte("93").byteValue()
  private static final byte MeterPotenciaAparenteInstantanea_DI1_BYTE = HexConversor.hexCharToByte("E9").byteValue()

  private static final byte MeterFatorDePotencia_DI0_BYTE = HexConversor.hexCharToByte("83").byteValue()
  private static final byte MeterFatorDePotencia_DI1_BYTE = HexConversor.hexCharToByte("E9").byteValue()

  private static final byte MeterFrequencia_DI0_BYTE = HexConversor.hexCharToByte("A3").byteValue()
  private static final byte MeterFrequencia_DI1_BYTE = HexConversor.hexCharToByte("E9").byteValue()

  private static final byte MeterRPotenciaAtiva_DI0_BYTE = HexConversor.hexCharToByte("44").byteValue()
  private static final byte MeterRPotenciaAtiva_DI1_BYTE = HexConversor.hexCharToByte("C3").byteValue()

  private static final byte MeterRPotenciaReativa_DI0_BYTE = HexConversor.hexCharToByte("44").byteValue()
  private static final byte MeterRPotenciaReativa_DI1_BYTE = HexConversor.hexCharToByte("C4").byteValue()

  private static final byte MeterRTensaoDeFase_DI0_BYTE = HexConversor.hexCharToByte("44").byteValue()
  private static final byte MeterRTensaoDeFase_DI1_BYTE = HexConversor.hexCharToByte("E9").byteValue()

  private static final byte MeterRCorrenteDeFase_DI0_BYTE = HexConversor.hexCharToByte("54").byteValue()
  private static final byte MeterRCorrenteDeFase_DI1_BYTE = HexConversor.hexCharToByte("E9").byteValue()

  private static final byte MeterSPotenciaAtiva_DI0_BYTE = HexConversor.hexCharToByte("45").byteValue()
  private static final byte MeterSPotenciaAtiva_DI1_BYTE = HexConversor.hexCharToByte("C3").byteValue()

  private static final byte MeterSPotenciaReativa_DI0_BYTE = HexConversor.hexCharToByte("45").byteValue()
  private static final byte MeterSPotenciaReativa_DI1_BYTE = HexConversor.hexCharToByte("C4").byteValue()

  private static final byte MeterSTensaoDeFase_DI0_BYTE = HexConversor.hexCharToByte("45").byteValue()
  private static final byte MeterSTensaoDeFase_DI1_BYTE = HexConversor.hexCharToByte("E9").byteValue()

  private static final byte MeterSCorrenteDeFase_DI0_BYTE = HexConversor.hexCharToByte("55").byteValue()
  private static final byte MeterSCorrenteDeFase_DI1_BYTE = HexConversor.hexCharToByte("E9").byteValue()

  private static final byte MeterTPotenciaAtiva_DI0_BYTE = HexConversor.hexCharToByte("46").byteValue()
  private static final byte MeterTPotenciaAtiva_DI1_BYTE = HexConversor.hexCharToByte("C3").byteValue()

  private static final byte MeterTPotenciaReativa_DI0_BYTE = HexConversor.hexCharToByte("46").byteValue()
  private static final byte MeterTPotenciaReativa_DI1_BYTE = HexConversor.hexCharToByte("C4").byteValue()

  private static final byte MeterTTensaoDeFase_DI0_BYTE = HexConversor.hexCharToByte("46").byteValue()
  private static final byte MeterTTensaoDeFase_DI1_BYTE = HexConversor.hexCharToByte("E9").byteValue()

  private static final byte MeterTCorrenteDeFase_DI0_BYTE = HexConversor.hexCharToByte("56").byteValue()
  private static final byte MeterTCorrenteDeFase_DI1_BYTE = HexConversor.hexCharToByte("E9").byteValue()

  private static final byte END_BYTE = HexConversor.hexCharToByte("16").byteValue()
  private static final byte ZERO_BYTE = HexConversor.hexCharToByte("00").byteValue()
  private static final byte CONTROLCODE_BYTE = HexConversor.hexCharToByte("01").byteValue()
  private static final byte CONTROLCODE_RESPOSTA_BYTE = HexConversor.hexCharToByte("81").byteValue()
  public static final int TIPO_MeterPotenciaAtivaConsumida = 4
  public static final int TIPO_MeterPotenciaReversaConsumida = 5
  public static final int TIPO_MeterPotenciaReativaConsumida = 6
  public static final int TIPO_MeterPotenciaReativaReversaConsumida = 7
  public static final int TIPO_MeterMaximaDemandaAtivaRegistrada = 8
  public static final int TIPO_MeterMaximaDemandaReativaRegistrada = 9
  public static final int TIPO_MeterAddress = 10
  public static final int TIPO_MeterPotenciaAtivaInstantanea = 11
  public static final int TIPO_MeterPotenciaReativaInstantanea = 12
  public static final int TIPO_MeterPotenciaAparenteInstantanea = 13
  public static final int TIPO_MeterFatorDePotencia = 14
  public static final int TIPO_MeterFrequencia = 15
  public static final int TIPO_MeterRPotenciaAtiva = 16
  public static final int TIPO_MeterRPotenciaReativa = 17
  public static final int TIPO_MeterRTensaoDeFase = 18
  public static final int TIPO_MeterRCorrenteDeFase = 19
  public static final int TIPO_MeterSPotenciaAtiva = 20
  public static final int TIPO_MeterSPotenciaReativa = 21
  public static final int TIPO_MeterSTensaoDeFase = 22
  public static final int TIPO_MeterSCorrenteDeFase = 23
  public static final int TIPO_MeterTPotenciaAtiva = 24
  public static final int TIPO_MeterTPotenciaReativa = 25
  public static final int TIPO_MeterTTensaoDeFase = 26
  public static final int TIPO_MeterTCorrenteDeFase = 27

  public static String sendManualCommand(String COMMAND, CommSerial CM) {
    if (CM == null) {
      return null
    }
    if (!CM.isInitOK()) {
      return null
    }
    if (COMMAND == null)
      return null
    try {
      CM.writeData(HexConversor.hexStringToBytes(COMMAND))
    } catch (Exception e) {
      log.info "[DLT645.sendManualCommand]"
      log.info e
      return null
    }
    return COMMAND
  }

  public static String sendSingleAutoCommand(int TIPO, String ENDERECO, CommSerial CM) {
    if (CM == null) {
      return null
    }
    if (!CM.isInitOK()) {
      return null
    }
    if ((ENDERECO == null) || (ENDERECO.length() != 6)) {
      return null
    }
    ArrayList retorno = new ArrayList()
    try {
      ArrayList meterNumber = HexConversor.meterNumberToByte(ENDERECO)
      retorno.add(Byte.valueOf(START_BYTE))
      retorno.add(Byte.valueOf(START_BYTE))
      retorno.add(Byte.valueOf(STARTFRAME_BYTE))

      retorno.add(meterNumber.get(0))

      retorno.add(meterNumber.get(1))

      retorno.add(meterNumber.get(2))
      retorno.add(Byte.valueOf(ZERO_BYTE))
      retorno.add(Byte.valueOf(ZERO_BYTE))
      retorno.add(Byte.valueOf(ZERO_BYTE))
      retorno.add(Byte.valueOf(STARTFRAME_BYTE))
      retorno.add(Byte.valueOf(CONTROLCODE_BYTE))

      retorno.add(HexConversor.hexCharToByte("02"))

      switch (TIPO) {
        case 4:
          retorno.add(Byte.valueOf(MeterPotenciaAtivaConsumida_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterPotenciaAtivaConsumida_DI1_BYTE))
          break
        case 5:
          retorno.add(Byte.valueOf(MeterPotenciaReversaConsumida_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterPotenciaReversaConsumida_DI1_BYTE))
          break
        case 6:
          retorno.add(Byte.valueOf(MeterPotenciaReativaConsumida_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterPotenciaReativaConsumida_DI1_BYTE))
          break
        case 7:
          retorno.add(Byte.valueOf(MeterPotenciaReativaReversaConsumida_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterPotenciaReativaReversaConsumida_DI1_BYTE))
          break
        case 8:
          retorno.add(Byte.valueOf(MeterMaximaDemandaAtivaRegistrada_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterMaximaDemandaAtivaRegistrada_DI1_BYTE))
          break
        case 9:
          retorno.add(Byte.valueOf(MeterMaximaDemandaReativaRegistrada_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterMaximaDemandaReativaRegistrada_DI1_BYTE))
          break
        case 10:
          retorno.add(Byte.valueOf(MeterAddress_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterAddress_DI1_BYTE))
          break
        case 11:
          retorno.add(Byte.valueOf(MeterPotenciaAtivaInstantanea_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterPotenciaAtivaInstantanea_DI1_BYTE))
          break
        case 12:
          retorno.add(Byte.valueOf(MeterPotenciaReativaInstantanea_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterPotenciaReativaInstantanea_DI1_BYTE))
          break
        case 13:
          retorno.add(Byte.valueOf(MeterPotenciaAparenteInstantanea_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterPotenciaAparenteInstantanea_DI1_BYTE))
          break
        case 14:
          retorno.add(Byte.valueOf(MeterFatorDePotencia_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterFatorDePotencia_DI1_BYTE))
          break
        case 15:
          retorno.add(Byte.valueOf(MeterFrequencia_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterFrequencia_DI1_BYTE))
          break
        case 16:
          retorno.add(Byte.valueOf(MeterRPotenciaAtiva_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterRPotenciaAtiva_DI1_BYTE))
          break
        case 17:
          retorno.add(Byte.valueOf(MeterRPotenciaReativa_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterRPotenciaReativa_DI1_BYTE))
          break
        case 18:
          retorno.add(Byte.valueOf(MeterRTensaoDeFase_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterRTensaoDeFase_DI1_BYTE))
          break
        case 19:
          retorno.add(Byte.valueOf(MeterRCorrenteDeFase_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterRCorrenteDeFase_DI1_BYTE))
          break
        case 20:
          retorno.add(Byte.valueOf(MeterSPotenciaAtiva_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterSPotenciaAtiva_DI1_BYTE))
          break
        case 21:
          retorno.add(Byte.valueOf(MeterSPotenciaReativa_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterSPotenciaReativa_DI1_BYTE));
          break
        case 22:
          retorno.add(Byte.valueOf(MeterSTensaoDeFase_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterSTensaoDeFase_DI1_BYTE))
          break
        case 23:
          retorno.add(Byte.valueOf(MeterSCorrenteDeFase_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterSCorrenteDeFase_DI1_BYTE))
          break
        case 24:
          retorno.add(Byte.valueOf(MeterTPotenciaAtiva_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterTPotenciaAtiva_DI1_BYTE))
          break
        case 25:
          retorno.add(Byte.valueOf(MeterTPotenciaReativa_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterTPotenciaReativa_DI1_BYTE))
          break
        case 26:
          retorno.add(Byte.valueOf(MeterTTensaoDeFase_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterTTensaoDeFase_DI1_BYTE))
          break
        case 27:
          retorno.add(Byte.valueOf(MeterTCorrenteDeFase_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterTCorrenteDeFase_DI1_BYTE))
          break
        default:
          retorno.add(Byte.valueOf(MeterPotenciaAtivaConsumida_DI0_BYTE))
          retorno.add(Byte.valueOf(MeterPotenciaAtivaConsumida_DI1_BYTE))
      }

      retorno.add(calcSendCS(retorno))
      retorno.add(Byte.valueOf(END_BYTE))
      CM.writeData(retorno)
    } catch (Exception e) {
      log.info "[DLT645.sendManualCommand()]"
      log.info e
      return "ERROR:0RWRsfRwwet0952442423"
    }
    return HexConversor.byteToHexString(retorno)
  }

  public static final String receiveData(CommSerial CM, long TIMEOUT) {
    ArrayList bytes = new ArrayList()

    long start = System.currentTimeMillis()
    long stop = System.currentTimeMillis()
    while (stop - start < TIMEOUT) {
      ArrayList tBytes = CM.readData(10L)
      if (tBytes != null) {
        bytes.addAll(tBytes)
        if (checkReceiveCS(bytes)) {
          bytes.add(0, Byte.valueOf(START_BYTE))
          return HexConversor.byteToHexString(bytes)
        }
      }
      stop = System.currentTimeMillis()
    }
    log.info "[DLT645.receiveData] Error: No valid data."
    log.info bytes.toString()
    return null
  }

  private static final Byte calcSendCS(ArrayList<Byte> BYTES) {
    int soma = 0

    int i = 0
    for (i = 0; (i < BYTES.size()) && (!((Byte) BYTES.get(i)).equals(Byte.valueOf(STARTFRAME_BYTE))); i++);
    for (; i < BYTES.size(); i++) {
      soma += ((Byte) BYTES.get(i)).byteValue()
    }
    Integer sss = new Integer(soma)
    return Byte.valueOf(sss.byteValue())
  }

  private static boolean checkReceiveCS(ArrayList<Byte> BYTES) {
    Integer soma = Integer.valueOf(0)
    int max = -1
    int min = -1
    for (int i = 0; i < BYTES.size(); i++) {
      if ((((Byte) BYTES.get(i)).equals(Byte.valueOf(STARTFRAME_BYTE))) && (min == -1)) {
        min = i
      }
      if ((((Byte) BYTES.get(i)).equals(Byte.valueOf(END_BYTE))) && (max == -1)) {
        max = i - 2
      }
    }
    for (int i = min; (min != -1) && (max != -1) && (i <= max); i++) {
      soma = Integer.valueOf(soma.intValue() + ((Byte) BYTES.get(i)).intValue())
    }
    return soma.byteValue() == ((Byte) BYTES.get(max + 1)).intValue()
  }

  public static int checkType(String IN) {
    if (IN == null) {
      return -1
    }
    int comp = 0
    ArrayList bytes = HexConversor.hexStringToBytes(IN)
    if (((Byte) bytes.get(0)).byteValue() != START_BYTE) {
      return -1
    }
    if (((Byte) bytes.get(1)).byteValue() != STARTFRAME_BYTE) {
      return -1
    }
    if (((Byte) bytes.get(8)).byteValue() != STARTFRAME_BYTE) {
      return -1
    }
    if (((Byte) bytes.get(9)).byteValue() != CONTROLCODE_RESPOSTA_BYTE) {
      return -1
    }
    comp = HexConversor.byteToInteger((Byte) bytes.get(10))
    if (comp <= 0) {
      return -1
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterPotenciaAtivaConsumida_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterPotenciaAtivaConsumida_DI1_BYTE)) {
      return 4
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterPotenciaReversaConsumida_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterPotenciaReversaConsumida_DI1_BYTE)) {
      return 5
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterPotenciaReativaConsumida_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterPotenciaReativaConsumida_DI1_BYTE)) {
      return 6
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterPotenciaReativaReversaConsumida_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterPotenciaReativaReversaConsumida_DI1_BYTE)) {
      return 7
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterMaximaDemandaAtivaRegistrada_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterMaximaDemandaAtivaRegistrada_DI1_BYTE)) {
      return 8
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterMaximaDemandaReativaRegistrada_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterMaximaDemandaReativaRegistrada_DI1_BYTE)) {
      return 9
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterAddress_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterAddress_DI1_BYTE)) {
      return 10
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterPotenciaAtivaInstantanea_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterPotenciaAtivaInstantanea_DI1_BYTE)) {
      return 11
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterPotenciaReativaInstantanea_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterPotenciaReativaInstantanea_DI1_BYTE)) {
      return 12
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterPotenciaAparenteInstantanea_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterPotenciaAparenteInstantanea_DI1_BYTE)) {
      return 13
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterFatorDePotencia_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterFatorDePotencia_DI1_BYTE)) {
      return 14
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterFrequencia_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterFrequencia_DI1_BYTE)) {
      return 15
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterRPotenciaAtiva_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterRPotenciaAtiva_DI1_BYTE)) {
      return 16
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterRPotenciaReativa_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterRPotenciaReativa_DI1_BYTE)) {
      return 17
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterRTensaoDeFase_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterRTensaoDeFase_DI1_BYTE)) {
      return 18
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterRCorrenteDeFase_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterRCorrenteDeFase_DI1_BYTE)) {
      return 19
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterSPotenciaAtiva_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterSPotenciaAtiva_DI1_BYTE)) {
      return 20
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterSPotenciaReativa_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterSPotenciaReativa_DI1_BYTE)) {
      return 21
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterSTensaoDeFase_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterSTensaoDeFase_DI1_BYTE)) {
      return 22
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterSCorrenteDeFase_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterSCorrenteDeFase_DI1_BYTE)) {
      return 23
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterTPotenciaAtiva_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterTPotenciaAtiva_DI1_BYTE)) {
      return 24
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterTPotenciaReativa_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterTPotenciaReativa_DI1_BYTE)) {
      return 25
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterTTensaoDeFase_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterTTensaoDeFase_DI1_BYTE)) {
      return 26
    }
    if ((((Byte) bytes.get(11)).byteValue() == MeterTCorrenteDeFase_DI0_BYTE) && (((Byte) bytes.get(12)).byteValue() == MeterTCorrenteDeFase_DI1_BYTE)) {
      return 27
    }

    return -2
  }

  public static MeterBytes getMeter4Bytes(String IN) {
    if (IN == null) {
      return null
    }
    ArrayList data = new ArrayList()
    ArrayList bytes = HexConversor.hexStringToBytes(IN)
    data.add(Integer.valueOf(HexConversor.byteToInteger((Byte) bytes.get(13))))
    data.add(Integer.valueOf(HexConversor.byteToInteger((Byte) bytes.get(14))))
    data.add(Integer.valueOf(HexConversor.byteToInteger((Byte) bytes.get(15))))
    data.add(Integer.valueOf(HexConversor.byteToInteger((Byte) bytes.get(16))))

    data.set(0, Integer.valueOf(((Integer) data.get(0)).intValue() - 51))
    ((Integer) data.get(0))
    data.set(0, Integer.valueOf(Integer.toString(((Integer) data.get(0)).intValue(), 16)))

    data.set(1, Integer.valueOf(((Integer) data.get(1)).intValue() - 51))
    ((Integer) data.get(1))
    data.set(1, Integer.valueOf(Integer.toString(((Integer) data.get(1)).intValue(), 16)))

    data.set(2, Integer.valueOf(((Integer) data.get(2)).intValue() - 51))
    ((Integer) data.get(2))
    data.set(2, Integer.valueOf(Integer.toString(((Integer) data.get(2)).intValue(), 16)))

    data.set(3, Integer.valueOf(((Integer) data.get(3)).intValue() - 51))
    ((Integer) data.get(3))
    data.set(3, Integer.valueOf(Integer.toString(((Integer) data.get(3)).intValue(), 16)))

    return new MeterBytes((Integer) data.get(3), (Integer) data.get(2), (Integer) data.get(1), (Integer) data.get(0))
  }

  public static RSTFases getMeter3BytesFases(String IN) {
    if (IN == null) {
      return null
    }
    ArrayList data = new ArrayList()
    ArrayList bytes = HexConversor.hexStringToBytes(IN)
    data.add(Integer.valueOf(HexConversor.byteToInteger((Byte) bytes.get(13))))
    data.add(Integer.valueOf(HexConversor.byteToInteger((Byte) bytes.get(14))))
    data.add(Integer.valueOf(HexConversor.byteToInteger((Byte) bytes.get(15))))

    data.set(0, Integer.valueOf(((Integer) data.get(0)).intValue() - 51))
    ((Integer) data.get(0))
    data.set(0, Integer.valueOf(Integer.toString(((Integer) data.get(0)).intValue(), 16)))

    data.set(1, Integer.valueOf(((Integer) data.get(1)).intValue() - 51))
    ((Integer) data.get(1))
    data.set(1, Integer.valueOf(Integer.toString(((Integer) data.get(1)).intValue(), 16)))

    data.set(2, Integer.valueOf(((Integer) data.get(2)).intValue() - 51))
    ((Integer) data.get(2))
    data.set(2, Integer.valueOf(Integer.toString(((Integer) data.get(2)).intValue(), 16)))

    return new RSTFases((Integer) data.get(2), (Integer) data.get(1), (Integer) data.get(0))
  }

  public static BytesGeral getMeter3BytesGeral(String IN) {
    if (IN == null) {
      return null
    }
    ArrayList data = new ArrayList()
    ArrayList bytes = HexConversor.hexStringToBytes(IN)
    data.add(Integer.valueOf(HexConversor.byteToInteger((Byte) bytes.get(13))))
    data.add(Integer.valueOf(HexConversor.byteToInteger((Byte) bytes.get(14))))
    data.add(Integer.valueOf(HexConversor.byteToInteger((Byte) bytes.get(15))))

    data.set(0, Integer.valueOf(((Integer) data.get(0)).intValue() - 51))
    ((Integer) data.get(0))
    data.set(0, Integer.valueOf(Integer.toString(((Integer) data.get(0)).intValue(), 16)))

    data.set(1, Integer.valueOf(((Integer) data.get(1)).intValue() - 51))
    ((Integer) data.get(1))
    data.set(1, Integer.valueOf(Integer.toString(((Integer) data.get(1)).intValue(), 16)))

    data.set(2, Integer.valueOf(((Integer) data.get(2)).intValue() - 51))
    ((Integer) data.get(2))
    data.set(2, Integer.valueOf(Integer.toString(((Integer) data.get(2)).intValue(), 16)))

    return new BytesGeral((Integer) data.get(2), (Integer) data.get(1), (Integer) data.get(0))
  }

  public static FatorPotencia getMeter2BytesFatorPotencia(String IN) {
    if (IN == null) {
      return null
    }
    ArrayList data = new ArrayList()
    ArrayList bytes = HexConversor.hexStringToBytes(IN)
    data.add(Integer.valueOf(HexConversor.byteToInteger((Byte) bytes.get(13))))
    data.add(Integer.valueOf(HexConversor.byteToInteger((Byte) bytes.get(14))))

    data.set(0, Integer.valueOf(((Integer) data.get(0)).intValue() - 51))
    ((Integer) data.get(0))
    data.set(0, Integer.valueOf(Integer.toString(((Integer) data.get(0)).intValue(), 16)))

    data.set(1, Integer.valueOf(((Integer) data.get(1)).intValue() - 51))
    ((Integer) data.get(1))
    data.set(1, Integer.valueOf(Integer.toString(((Integer) data.get(1)).intValue(), 16)))

    return new FatorPotencia((Integer) data.get(1), (Integer) data.get(0))
  }

  public static Frequencia getMeter2BytesFrequencia(String IN) {
    if (IN == null) {
      return null
    }
    ArrayList data = new ArrayList()
    ArrayList bytes = HexConversor.hexStringToBytes(IN)
    data.add(Integer.valueOf(HexConversor.byteToInteger((Byte) bytes.get(13))))
    data.add(Integer.valueOf(HexConversor.byteToInteger((Byte) bytes.get(14))))

    data.set(0, Integer.valueOf(((Integer) data.get(0)).intValue() - 51))
    ((Integer) data.get(0))
    data.set(0, Integer.valueOf(Integer.toString(((Integer) data.get(0)).intValue(), 16)))

    data.set(1, Integer.valueOf(((Integer) data.get(1)).intValue() - 51))
    ((Integer) data.get(1))
    data.set(1, Integer.valueOf(Integer.toString(((Integer) data.get(1)).intValue(), 16)))

    return new Frequencia((Integer) data.get(1), (Integer) data.get(0))
  }

  public static TensaoDeFase getMeter2BytesTensaoDeFase(String IN) {
    if (IN == null) {
      return null
    }
    ArrayList data = new ArrayList()
    ArrayList bytes = HexConversor.hexStringToBytes(IN)
    data.add(Integer.valueOf(HexConversor.byteToInteger((Byte) bytes.get(13))))
    data.add(Integer.valueOf(HexConversor.byteToInteger((Byte) bytes.get(14))))

    data.set(0, Integer.valueOf(((Integer) data.get(0)).intValue() - 51))
    ((Integer) data.get(0))
    data.set(0, Integer.valueOf(Integer.toString(((Integer) data.get(0)).intValue(), 16)))

    data.set(1, Integer.valueOf(((Integer) data.get(1)).intValue() - 51))
    ((Integer) data.get(1))
    data.set(1, Integer.valueOf(Integer.toString(((Integer) data.get(1)).intValue(), 16)))

    return new TensaoDeFase((Integer) data.get(1), (Integer) data.get(0))
  }

  public static String getMeterAddress(String IN) {
    if (IN == null) {
      return null
    }

    ArrayList ints = HexConversor.hexStringToIntegers(IN)

    Integer aux = (Integer) ints.get(13)
    ints.set(13, ints.get(15))
    ints.set(15, aux)

    ints.set(13, Integer.valueOf(((Integer) ints.get(13)).intValue() - 51))
    ints.set(14, Integer.valueOf(((Integer) ints.get(14)).intValue() - 51))
    ints.set(15, Integer.valueOf(((Integer) ints.get(15)).intValue() - 51))

    ArrayList hexDecimal = HexConversor.hexByteToDecimal(ints)
    StringBuilder str = new StringBuilder()

    for (int i = 0; i < hexDecimal.size(); i++) {
      str.append(((Hexadecimal) hexDecimal.get(i)).get())
    }

    return str.toString().substring(26, 32)
  }

  public static String getMeterNumber(String IN) {
    if (IN == null) {
      return null
    }

    ArrayList ints = HexConversor.hexStringToIntegers(IN)
    Integer aux = (Integer) ints.get(2)
    ints.set(2, ints.get(4))
    ints.set(4, aux)
    ArrayList hexDecimal = HexConversor.hexByteToDecimal(ints)
    StringBuilder str = new StringBuilder()

    for (int i = 0; i < hexDecimal.size(); i++) {
      str.append(((Hexadecimal) hexDecimal.get(i)).get())
    }

    return str.toString().substring(4, 10)
  }
}
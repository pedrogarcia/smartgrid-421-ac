package RS485

import gnu.io.CommPortIdentifier
import gnu.io.PortInUseException
import gnu.io.SerialPort
import gnu.io.UnsupportedCommOperationException
import groovy.util.logging.Log

/**
 User: Pedro Garcia
 Mail: sawp@sawp.com.br
 Date: 15/04/13
 */

@Log
public class CommSerial {
  private CommPortIdentifier portId
  private SerialPort serialPort
  private OutputStream outputStream
  private InputStream inputStream
  private boolean uUSB
  private int bitRate
  private boolean portOK
  private boolean initOK
  private String portName

  public boolean openComm(String portName, int bitRate, boolean uUSB) {
    this.bitRate = bitRate
    this.uUSB = uUSB

    initComm(portName)

    return this.initOK
  }

  private void errorComm() {
    log.info "[CommSerial.errorComm] Error: disconnecting meter."
    this.initOK = false
    closeComm()
  }

  public void writeData(ArrayList<Byte> data) {
    if (this.initOK) {
      if (data == null) {
        log.info "[CommSerial.writeData] Error: data null"
        return
      }
      try {
        for (int i = 0; i < data.size(); i++)
          this.outputStream.write(((Byte) data.get(i)).byteValue())
      }
      catch (IOException e) {
        log.info "[CommSerial.writeData] Error: command can not be sent to meter."
        log.info e
        errorComm()
      }
    } else {
      log.info "[CommSerial.writeData] Error: unknown error."
      errorComm()
    }
  }

  public ArrayList<Byte> readData(long DELAY) {
    ArrayList bytes = new ArrayList()
    if ((this.initOK) && (this.inputStream != null)) {
      try {
        long start = System.currentTimeMillis()
        long stop = System.currentTimeMillis()
        while (stop - start < DELAY) {
          while (this.inputStream.available() > 0) {
            start = System.currentTimeMillis()
            bytes.add(Byte.valueOf((byte) this.inputStream.read()))
          }
          stop = System.currentTimeMillis()
        }
        if (bytes.isEmpty()) {
          return null
        }
        return bytes
      } catch (Exception e) {
        log.info "[CommSerial.readData] Error: " + e.getMessage()
        log.info e
        errorComm()
        return null
      }
    }
    log.info "[CommSerial.readData] Error: lost connection to the meter."
    try {
      Thread.sleep(1000L)
    } catch (InterruptedException e) {
    }
    this.initOK = false
    errorComm()
    return null
  }

  public void closeComm() {
    try {
      if (this.inputStream != null) {
        this.inputStream.close()
        this.inputStream = null
        log.info "[CommSerial.closeComm] Closed inputStream."
      }
      if (this.outputStream != null) {
        this.outputStream.close()
        this.outputStream = null
        log.info "[CommSerial.closeComm] Closed outputStream"
      }
      if (this.serialPort != null) {
        this.serialPort.close()
        this.serialPort = null
      }
      this.portOK = false
      this.initOK = false
      log.info "[CommSerial.closeComm] Port closed successfully."
    } catch (Exception e) {
      log.info "[CommSerial.closeComm] Error: Fails when disconnecting."
      log.info e
    }
  }

  private void initComm(String portName) {
    this.initOK = true
    this.portName = portName
    if (this.portName != null) {
      this.portOK = false
      Enumeration portList = CommPortIdentifier.getPortIdentifiers()
      while (portList.hasMoreElements()) {
        this.portId = ((CommPortIdentifier) portList.nextElement())
        if ((this.portId.getPortType() == 1) &&
            (this.portId.getName().equals(this.portName))) {
          try {
            this.serialPort = ((SerialPort) this.portId.open(getClass().getName(), 2000))
            this.portOK = true
          }
          catch (PortInUseException e) {
            this.portName = null
            this.initOK = false
            log.info "[CommSerial.initComm] Error: Port in use."
            log.info e
            errorComm()
            return
          }
        }
      }
      try {
        this.inputStream = this.serialPort.getInputStream()
        this.outputStream = this.serialPort.getOutputStream()
        this.serialPort.setSerialPortParams(this.bitRate, 8, 1, 2)
      }
      catch (IOException e) {
        this.initOK = false
        log.info "[CommSerial.initComm] Error: IO error."
        log.info e
        errorComm()
        return
      } catch (UnsupportedCommOperationException e) {
        log.info "[CommSerial.initComm] Error: unsupported operation."
        log.info e
        errorComm()
        this.initOK = false
        return
      } catch (NullPointerException e) {
        log.info "[CommSerial.initComm] Error: NullPointerException"
        log.info e
        errorComm()
        this.initOK = false
        return
      }
      try {
        this.serialPort.notifyOnOutputEmpty(true)
      } catch (Exception e) {
        log.info "[CommSerial.initComm] Error: " + e.message
        log.info e
        this.initOK = false
        errorComm()
        return
      }
    } else {
      this.initOK = false
      this.portOK = false
    }
  }

  public ArrayList<String> listPorts() {
    ArrayList array = new ArrayList()

    Enumeration portList = CommPortIdentifier.getPortIdentifiers()
    while (portList.hasMoreElements()) {
      CommPortIdentifier ports = (CommPortIdentifier) portList.nextElement()
      if (ports.getPortType() == 1) {
        array.add(ports.getName())
      }
    }
    return array
  }

  public boolean isPortOK() {
    return this.portOK
  }

  public boolean isInitOK() {
    try {
      if (this.inputStream != null)
        this.inputStream.available()
      else {
        return this.initOK = 0
      }
      return this.initOK
    } catch (IOException e) {
      log.info "[CommSerial.isInitOK] Error: Communication lost (" + e.message + ")"
      log.info e
      errorComm()
    }
    return false
  }

  public String getPortName() {
    return this.portName
  }
}
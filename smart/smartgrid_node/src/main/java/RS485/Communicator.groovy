package RS485

import utils.MeterProperties
import groovy.util.logging.Log

/**
 User: Pedro Garcia
 Mail: sawp@sawp.com.br
 Date: 22/04/13
 */
@Log
class Communicator {
  private static CommSerial commSerial = new CommSerial()
  private static final INSTANCE = new Communicator()
  private Boolean isConected = false

  private Communicator() {
    connect(MeterProperties.metersProperties.Port)
  }

  static getInstance() {
    return INSTANCE
  }

  public CommSerial getCommSerial() {
    return this.commSerial
  }

  public static String sendSingleAutoCommand(int type, String address) {
    return DLT645.sendSingleAutoCommand(type, address, Communicator.instance.commSerial)
  }

  public static String receiveData(long timeout) {
    return DLT645.receiveData(Communicator.instance.commSerial, timeout)
  }

  public void connect(String port) {
    try {
      "[Communicator.connect] Trying to connect at port: " + port
      def test = commSerial.openComm(port, 1200, true)
      if (test)
        println "[Communicator.connect] Established connection at port: " + commSerial.getPortName()
    } catch(NullPointerException e) {
      println "[Communicator.connect] Connection failed at port: " + port
      println e
    } catch(Exception e) {
      println "[Communicator.connect] Error: " + e.message
    }
  }

  public void disconnect() {
    commSerial.closeComm()
    if (commSerial.isInitOK())
      log.info "[Communicator.disconnect] Connection failed to disconnect."
    else
      log.info "[Communicator.disconnect] Connection disconnected successfully."
  }
}

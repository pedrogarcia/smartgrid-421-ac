package Meter

/**
 User: Pedro Garcia
 Mail: sawp@sawp.com.br
 Date: 15/04/13
 */
class FatorPotencia {
  private ArrayList<Integer> data = new ArrayList()
  private Double paInDouble

  public FatorPotencia(Integer d1, Integer d2) {
    this.data.add(d1)
    this.data.add(d2)
    this.paInDouble = Double.valueOf(10.0D)
  }

  public FatorPotencia(Double PAD) {
    Double pad = PAD
    this.paInDouble = PAD
    if (PAD == null) {
      this.paInDouble = Double.valueOf(-1.0D)
    }
    pad = Double.valueOf(pad.doubleValue() * 10.0D)
    long ix = pad.longValue()
    this.data.add(Integer.valueOf((int) ix))
    long ix2 = (long) (pad.doubleValue() * 100.0D) - ix * 100L
    this.data.add(Integer.valueOf((int) ix2))
  }

  public Integer getData(int INDEX) {
    return (Integer) this.data.get(INDEX)
  }

  public Double getFloat() {
    def v1 = ((Integer) this.data.get(0)).intValue() * 0.1D
    def v2 = ((Integer) this.data.get(1)).intValue() * 0.001D
    return Double.valueOf(v1 + v2)
  }

  public void setData(Integer index, Integer content) {
    this.data.set(index, content)
  }

  public String print() {
    StringBuilder str = new StringBuilder()
    if (((Integer) this.data.get(0)).intValue() > 0) {
      if (((Integer) this.data.get(0)).intValue() < 10) {
        str.append("0")
      }
      str.append(this.data.get(0))
    }

    if (str.toString().length() == 0) {
      str.append("00")
    }

    if (((Integer) this.data.get(1)).intValue() > 0) {
      if (((Integer) this.data.get(1)).intValue() < 10) {
        str.append("0")
      }
      str.append(this.data.get(1))
    } else {
      str.append("00")
    }

    str.insert(1, '.')

    return str.toString()
  }
}

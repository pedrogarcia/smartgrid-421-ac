package Meter

/**
 User: Pedro Garcia
 Mail: sawp@sawp.com.br
 Date: 15/04/13
 */
public class Frequencia {
  private ArrayList<Integer> data = new ArrayList()
  private Double paInDouble = null

  public Frequencia(Integer d1, Integer d2) {
    this.data.add(d1)
    this.data.add(d2)
    this.paInDouble = Double.valueOf(10.0D)
  }

  public Frequencia(Double PAD) {
    Double pad = PAD
    this.paInDouble = PAD
    if (PAD == null) {
      this.paInDouble = Double.valueOf(-1.0D)
    }
    long ix = pad.longValue()
    this.data.add(Integer.valueOf((int) ix))
    long ix2 = (long) (pad.doubleValue() * 100.0D) - ix * 100L
    this.data.add(Integer.valueOf((int) ix2))
  }

  public Integer getData(int INDEX) {
    return (Integer) this.data.get(INDEX)
  }

  public Double getFloat() {
    return Double.valueOf(((Integer) this.data.get(0)).intValue() + ((Integer) this.data.get(1)).intValue() * 0.01D)
  }

  public void setData(int INDEX, Integer DATA) {
    this.data.set(INDEX, DATA)
  }

  public String print() {
    StringBuilder str = new StringBuilder()
    if (((Integer) this.data.get(0)).intValue() > 0) {
      if (((Integer) this.data.get(0)).intValue() < 10) {
        str.append("0")
      }
      str.append(this.data.get(0))
    }

    if (str.toString().length() == 0) {
      str.append("0")
    }
    str.append(".")
    if (((Integer) this.data.get(1)).intValue() > 0) {
      if (((Integer) this.data.get(1)).intValue() < 10) {
        str.append("0")
      }
      str.append(this.data.get(1))
    }
    if (str.toString().equals("0.")) {
      str.append("00")
    }
    return str.toString()
  }
}

package Meter

import RS485.Communicator
import RS485.DLT645
import groovy.util.logging.Log

/**
 User: Pedro Garcia
 Mail: sawp@sawp.com.br
 Date: 22/04/13
 */
@Log
class MeterData {
  private MeterName meter

  MeterData(String meterName, String meterAddress) {
    this.meter = new MeterName(meterName, meterAddress)
  }

  public Map<String, String> getMeterData() {
    def rawMeterData = ler(this.meter)
    return rawMeterData
  }

  private Map<String, String> ler(MeterName meterName) {
    def address = meterName.address
    def meterTypes = createMeterTypes()
    ArrayList<String> inBuffer = []
    for (Integer type : meterTypes) {
      String out = Communicator.sendSingleAutoCommand(type, address)
      inBuffer.add(Communicator.receiveData(1000L))
    }
    def out = processBuffer(inBuffer, meterName)
    return out
  }

  private Map processBuffer(ArrayList<String> inBuffer, MeterName meterName) {
    def IN = inBuffer
    def medidas = new TreeMap<String, String>()

    while (IN.size() > 0) {
      String ins = (String) IN.get(0)

      switch (DLT645.checkType(ins)) {
        case 4:
          medidas["PotenciaAtivaConsumida"] = DLT645.getMeter4Bytes(ins).print()
          break
        case 5:
          medidas["PotenciaReversaConsumida"] = DLT645.getMeter4Bytes(ins).print()
          break
        case 6:
          medidas["PotenciaReativaConsumida"] = DLT645.getMeter4Bytes(ins).print()
          break
        case 7:
          medidas["PotenciaReativaReversaConsumida"] = DLT645.getMeter4Bytes(ins).print()
          break
        case 8:
          medidas["MaximaDemandaAtivaRegistrada"] = DLT645.getMeter3BytesGeral(ins).print()
          break
        case 9:
          medidas["MaximaDemandaReativaRegistrada"] = DLT645.getMeter3BytesGeral(ins).print()
          break
        case 10:
          medidas["Address"] = DLT645.getMeterAddress(ins)
          break
        case 11:
          medidas["MeterPotenciaAtivaInstantanea"] = DLT645.getMeter3BytesGeral(ins).print()
          break
        case 12:
          medidas["PotenciaReativaInstantanea"] = DLT645.getMeter3BytesGeral(ins).print()
          break
        case 13:
          medidas["PotenciaAparenteInstantanea"] = DLT645.getMeter3BytesGeral(ins).print()
          break
        case 14:
          medidas["FatorDePotencia"] = DLT645.getMeter2BytesFatorPotencia(ins).print()
          break
        case 15:
          medidas["Frequencia"] = DLT645.getMeter2BytesFrequencia(ins).print()
          break
        case 16:
          medidas["RPotenciaAtiva"] = DLT645.getMeter4Bytes(ins).print()
          break
        case 17:
          medidas["RPotenciaReativa"] = DLT645.getMeter4Bytes(ins).print()
          break
        case 18:
          medidas["RTensaoDeFase"] = DLT645.getMeter2BytesTensaoDeFase(ins).print()
          break
        case 19:
          medidas["RCorrenteDeFase"] = DLT645.getMeter3BytesFases(ins).print()
          break
        case 20:
          medidas["SPotenciaAtiva"] = DLT645.getMeter4Bytes(ins).print()
          break
        case 21:
          medidas["SPotenciaReativa"] = DLT645.getMeter4Bytes(ins).print()
          break
        case 22:
          medidas["STensaoDeFase"] = DLT645.getMeter2BytesTensaoDeFase(ins).print()
          break
        case 23:
          medidas["SCorrenteDeFase"] = DLT645.getMeter3BytesFases(ins).print()
          break
        case 24:
          medidas["TPotenciaAtiva"] = DLT645.getMeter4Bytes(ins).print()
          break
        case 25:
          medidas["TPotenciaReativa"] = DLT645.getMeter4Bytes(ins).print()
          break
        case 26:
          medidas["TTensaoDeFase"] = DLT645.getMeter2BytesTensaoDeFase(ins).print()
          break
        case 27:
          medidas["TCorrenteDeFase"] = DLT645.getMeter3BytesFases(ins).print()
          break
        default:
          log.info "[MeterData.processBuffer], Error: unknown type."
      }
      IN.remove(0)
    }
    def measure = ["Name": meterName.name, "Address": meterName.address, "Measures": medidas]
    return measure
  }

  private ArrayList<Integer> createMeterTypes() {
    ArrayList meter = new ArrayList()
    for (int i = 4; i < 28; i++)
      meter.add(Integer.valueOf(i))
    return meter
  }
}

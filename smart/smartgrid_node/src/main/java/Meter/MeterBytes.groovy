package Meter

/**
 User: Pedro Garcia
 Mail: sawp@sawp.com.br
 Date: 15/04/13
 */
public class MeterBytes {
  private ArrayList<Integer> data = new ArrayList()
  private Double paInDouble = null

  public MeterBytes(Integer d1, Integer d2, Integer d3, Integer d4) {
    this.data.add(d1)
    this.data.add(d2)
    this.data.add(d3)
    this.data.add(d4)
    this.paInDouble = Double.valueOf(10.0D)
  }

  public MeterBytes(Double PAD) {
    Double pad = PAD
    this.paInDouble = PAD
    if (PAD == null) {
      this.paInDouble = Double.valueOf(-1.0D)
    }
    pad = Double.valueOf(pad.doubleValue() * 10.0D)
    long ix = pad.longValue()
    this.data.add(Integer.valueOf((int) (ix / 1000000L)))
    ix %= 1000000L
    this.data.add(Integer.valueOf((int) (ix / 10000L)))
    ix %= 10000L
    this.data.add(Integer.valueOf((int) ix / 100))
    this.data.add(Integer.valueOf((int) ix % 100))
  }

  public Integer getData(int INDEX) {
    return (Integer) this.data.get(INDEX)
  }

  public Double getFloat() {
    def v1 = ((Integer) this.data.get(0)).intValue() * 100000
    def v2 = ((Integer) this.data.get(1)).intValue() * 1000
    def v3 = ((Integer) this.data.get(2)).intValue() * 10
    def v4 = ((Integer) this.data.get(3)).intValue() * 0.1D
    return Double.valueOf(v1 + v2 + v3 + v4)
  }

  public void setData(int INDEX, Integer DATA) {
    this.data.set(INDEX, DATA)
  }

  public String print() {
    StringBuilder str = new StringBuilder()
    if (((Integer) this.data.get(0)).intValue() > 0) {
      if (((Integer) this.data.get(0)).intValue() < 10) {
        str.append("0")
      }
      str.append(this.data.get(0))
    } else {
      str.append("00")
    }

    if (((Integer) this.data.get(1)).intValue() > 0) {
      if (((Integer) this.data.get(1)).intValue() < 10) {
        str.append("0")
      }
      str.append(this.data.get(1))
    } else {
      str.append("00")
    }

    if (((Integer) this.data.get(2)).intValue() > 0) {
      if (((Integer) this.data.get(2)).intValue() < 10) {
        str.append("0")
      }
      str.append(this.data.get(2))
    } else {
      str.append("00")
    }

    if (((Integer) this.data.get(3)).intValue() > 0) {
      if (((Integer) this.data.get(3)).intValue() < 10) {
        str.append("0")
      }
      str.append(this.data.get(3))
    } else {
      str.append("00")
    }
    str.insert(str.length() - 1, ".")
    return str.toString()
  }
}

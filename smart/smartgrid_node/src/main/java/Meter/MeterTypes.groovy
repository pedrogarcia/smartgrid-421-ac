package Meter

/**
 User: Pedro Garcia
 Mail: sawp@sawp.com.br
 Date: 15/04/13
 */
public class MeterTypes {
  private Boolean meterPotenciaAtivaConsumida = Boolean.valueOf(false)
  private Boolean meterPotenciaReversaConsumida = Boolean.valueOf(false)
  private Boolean meterPotenciaReativaConsumida = Boolean.valueOf(false)
  private Boolean meterPotenciaReativaReversaConsumida = Boolean.valueOf(false)
  private Boolean meterMaximaDemandaAtivaRegistrada = Boolean.valueOf(false)
  private Boolean meterMaximaDemandaReativaRegistrada = Boolean.valueOf(false)
  private Boolean meterAddress = Boolean.valueOf(false)
  private Boolean meterPotenciaAtivaInstantanea = Boolean.valueOf(false)
  private Boolean meterPotenciaReativaInstantanea = Boolean.valueOf(false)
  private Boolean meterPotenciaAparenteInstantanea = Boolean.valueOf(false)
  private Boolean meterFatorDePotencia = Boolean.valueOf(false)
  private Boolean meterFrequencia = Boolean.valueOf(false)
  private Boolean meterRPotenciaAtiva = Boolean.valueOf(false)
  private Boolean meterRPotenciaReativa = Boolean.valueOf(false)
  private Boolean meterRTensaoDeFase = Boolean.valueOf(false)
  private Boolean meterRCorrenteDeFase = Boolean.valueOf(false)
  private Boolean meterSPotenciaAtiva = Boolean.valueOf(false)
  private Boolean meterSPotenciaReativa = Boolean.valueOf(false)
  private Boolean meterSTensaoDeFase = Boolean.valueOf(false)
  private Boolean meterSCorrenteDeFase = Boolean.valueOf(false)
  private Boolean meterTPotenciaAtiva = Boolean.valueOf(false)
  private Boolean meterTPotenciaReativa = Boolean.valueOf(false)
  private Boolean meterTTensaoDeFase = Boolean.valueOf(false)
  private Boolean meterTCorrenteDeFase = Boolean.valueOf(false)

  public Boolean getMeterAddress() {
    return this.meterAddress
  }

  public void setMeterAddress(Boolean meterAddress) {
    this.meterAddress = meterAddress
  }

  public Boolean getMeterFatorDePotencia() {
    return this.meterFatorDePotencia
  }

  public void setMeterFatorDePotencia(Boolean meterFatorDePotencia) {
    this.meterFatorDePotencia = meterFatorDePotencia
  }

  public Boolean getMeterFrequencia() {
    return this.meterFrequencia
  }

  public void setMeterFrequencia(Boolean meterFrequencia) {
    this.meterFrequencia = meterFrequencia
  }

  public Boolean getMeterMaximaDemandaAtivaRegistrada() {
    return this.meterMaximaDemandaAtivaRegistrada
  }

  public void setMeterMaximaDemandaAtivaRegistrada(Boolean meterMaximaDemandaAtivaRegistrada) {
    this.meterMaximaDemandaAtivaRegistrada = meterMaximaDemandaAtivaRegistrada
  }

  public Boolean getMeterMaximaDemandaReativaRegistrada() {
    return this.meterMaximaDemandaReativaRegistrada
  }

  public void setMeterMaximaDemandaReativaRegistrada(Boolean meterMaximaDemandaReativaRegistrada) {
    this.meterMaximaDemandaReativaRegistrada = meterMaximaDemandaReativaRegistrada
  }

  public Boolean getMeterPotenciaAparenteInstantanea() {
    return this.meterPotenciaAparenteInstantanea
  }

  public void setMeterPotenciaAparenteInstantanea(Boolean meterPotenciaAparenteInstantanea) {
    this.meterPotenciaAparenteInstantanea = meterPotenciaAparenteInstantanea
  }

  public Boolean getMeterPotenciaAtivaConsumida() {
    return this.meterPotenciaAtivaConsumida
  }

  public void setMeterPotenciaAtivaConsumida(Boolean meterPotenciaAtivaConsumida) {
    this.meterPotenciaAtivaConsumida = meterPotenciaAtivaConsumida
  }

  public Boolean getMeterPotenciaAtivaInstantanea() {
    return this.meterPotenciaAtivaInstantanea
  }

  public void setMeterPotenciaAtivaInstantanea(Boolean meterPotenciaAtivaInstantanea) {
    this.meterPotenciaAtivaInstantanea = meterPotenciaAtivaInstantanea
  }

  public Boolean getMeterPotenciaReativaConsumida() {
    return this.meterPotenciaReativaConsumida
  }

  public void setMeterPotenciaReativaConsumida(Boolean meterPotenciaReativaConsumida) {
    this.meterPotenciaReativaConsumida = meterPotenciaReativaConsumida
  }

  public Boolean getMeterPotenciaReativaInstantanea() {
    return this.meterPotenciaReativaInstantanea
  }

  public void setMeterPotenciaReativaInstantanea(Boolean meterPotenciaReativaInstantanea) {
    this.meterPotenciaReativaInstantanea = meterPotenciaReativaInstantanea
  }

  public Boolean getMeterPotenciaReativaReversaConsumida() {
    return this.meterPotenciaReativaReversaConsumida
  }

  public void setMeterPotenciaReativaReversaConsumida(Boolean meterPotenciaReativaReversaConsumida) {
    this.meterPotenciaReativaReversaConsumida = meterPotenciaReativaReversaConsumida
  }

  public Boolean getMeterPotenciaReversaConsumida() {
    return this.meterPotenciaReversaConsumida
  }

  public void setMeterPotenciaReversaConsumida(Boolean meterPotenciaReversaConsumida) {
    this.meterPotenciaReversaConsumida = meterPotenciaReversaConsumida
  }

  public Boolean getMeterRCorrenteDeFase() {
    return this.meterRCorrenteDeFase
  }

  public void setMeterRCorrenteDeFase(Boolean meterRCorrenteDeFase) {
    this.meterRCorrenteDeFase = meterRCorrenteDeFase
  }

  public Boolean getMeterRPotenciaAtiva() {
    return this.meterRPotenciaAtiva
  }

  public void setMeterRPotenciaAtiva(Boolean meterRPotenciaAtiva) {
    this.meterRPotenciaAtiva = meterRPotenciaAtiva
  }

  public Boolean getMeterRPotenciaReativa() {
    return this.meterRPotenciaReativa
  }

  public void setMeterRPotenciaReativa(Boolean meterRPotenciaReativa) {
    this.meterRPotenciaReativa = meterRPotenciaReativa
  }

  public Boolean getMeterRTensaoDeFase() {
    return this.meterRTensaoDeFase
  }

  public void setMeterRTensaoDeFase(Boolean meterRTensaoDeFase) {
    this.meterRTensaoDeFase = meterRTensaoDeFase
  }

  public Boolean getMeterSCorrenteDeFase() {
    return this.meterSCorrenteDeFase
  }

  public void setMeterSCorrenteDeFase(Boolean meterSCorrenteDeFase) {
    this.meterSCorrenteDeFase = meterSCorrenteDeFase
  }

  public Boolean getMeterSPotenciaAtiva() {
    return this.meterSPotenciaAtiva
  }

  public void setMeterSPotenciaAtiva(Boolean meterSPotenciaAtiva) {
    this.meterSPotenciaAtiva = meterSPotenciaAtiva
  }

  public Boolean getMeterSPotenciaReativa() {
    return this.meterSPotenciaReativa
  }

  public void setMeterSPotenciaReativa(Boolean meterSPotenciaReativa) {
    this.meterSPotenciaReativa = meterSPotenciaReativa
  }

  public Boolean getMeterSTensaoDeFase() {
    return this.meterSTensaoDeFase
  }

  public void setMeterSTensaoDeFase(Boolean meterSTensaoDeFase) {
    this.meterSTensaoDeFase = meterSTensaoDeFase
  }

  public Boolean getMeterTCorrenteDeFase() {
    return this.meterTCorrenteDeFase
  }

  public void setMeterTCorrenteDeFase(Boolean meterTCorrenteDeFase) {
    this.meterTCorrenteDeFase = meterTCorrenteDeFase
  }

  public Boolean getMeterTPotenciaAtiva() {
    return this.meterTPotenciaAtiva
  }

  public void setMeterTPotenciaAtiva(Boolean meterTPotenciaAtiva) {
    this.meterTPotenciaAtiva = meterTPotenciaAtiva
  }

  public Boolean getMeterTPotenciaReativa() {
    return this.meterTPotenciaReativa
  }

  public void setMeterTPotenciaReativa(Boolean meterTPotenciaReativa) {
    this.meterTPotenciaReativa = meterTPotenciaReativa
  }

  public Boolean getMeterTTensaoDeFase() {
    return this.meterTTensaoDeFase
  }

  public void setMeterTTensaoDeFase(Boolean meterTTensaoDeFase) {
    this.meterTTensaoDeFase = meterTTensaoDeFase
  }
}

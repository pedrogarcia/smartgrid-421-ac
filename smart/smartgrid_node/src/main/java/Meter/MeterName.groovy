package Meter

/**
 User: Pedro Garcia
 Mail: sawp@sawp.com.br
 Date: 15/04/13
 */
public class MeterName {
  private String name
  private String address

  public MeterName(String NAME, String ADDRESS) {
    this.name = NAME
    this.address = ADDRESS
  }

  public String getAddress() {
    return this.address
  }

  public void setAddress(String address) {
    this.address = address
  }

  public String getName() {
    return this.name
  }

  public void setName(String name) {
    this.name = name
  }
}

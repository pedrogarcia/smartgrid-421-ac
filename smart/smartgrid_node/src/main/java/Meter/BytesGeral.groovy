package Meter

/**
 User: Pedro Garcia
 Mail: sawp@sawp.com.br
 Date: 15/04/13
 */
public class BytesGeral {
  private ArrayList<Integer> data = new ArrayList()
  private Double paInDouble = null

  public BytesGeral(Integer d1, Integer d2, Integer d3) {
    this.data.add(d1)
    this.data.add(d2)
    this.data.add(d3)
    this.paInDouble = Double.valueOf(10.0D)
  }

  public BytesGeral(Double PAD) {
    Double pad = PAD
    this.paInDouble = PAD
    if (PAD == null) {
      this.paInDouble = Double.valueOf(-1.0D)
    }
    long ix = pad.longValue()
    ix /= 100L
    this.data.add(Integer.valueOf((int) ix))
    long ix2 = (long) (pad.doubleValue() % 100.0D)
    this.data.add(Integer.valueOf((int) ix2))
    long ix3 = (long) (pad.doubleValue() * 100.0D - pad.longValue() * 100L)
    this.data.add(Integer.valueOf((int) ix3))
  }

  public Integer getData(int INDEX) {
    return (Integer) this.data.get(INDEX)
  }

  public Double getFloat() {
    def v1 = ((Integer) this.data.get(0)).intValue() * 100
    def v2 = ((Integer) this.data.get(1)).intValue()
    def v3 = ((Integer) this.data.get(2)).intValue() * 0.01D
    return Double.valueOf(v1 + v2 + v3)
  }

  public void setData(int index, Integer content) {
    this.data.set(index, content)
  }

  public String print() {
    StringBuilder str = new StringBuilder()
    if (((Integer) this.data.get(0)).intValue() > 0) {
      if (((Integer) this.data.get(0)).intValue() < 10) {
        str.append("0")
      }
      str.append(this.data.get(0))
    } else {
      str.append("00")
    }

    if (((Integer) this.data.get(1)).intValue() > 0) {
      if (((Integer) this.data.get(1)).intValue() < 10) {
        str.append("0")
      }
      str.append(this.data.get(1))
    } else {
      str.append("00")
    }
    str.append(".")
    if (((Integer) this.data.get(2)).intValue() > 0) {
      if (((Integer) this.data.get(2)).intValue() < 10) {
        str.append("0")
      }
      str.append(this.data.get(2))
    } else {
      str.append("00")
    }

    return str.toString()
  }
}
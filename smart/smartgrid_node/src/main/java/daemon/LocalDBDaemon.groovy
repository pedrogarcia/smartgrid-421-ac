package daemon

import dao.MeasureDAO
import groovy.util.logging.Log
import Meter.MeterData
import utils.MeterProperties

/**
 * User: Pedro Garcia
 * Date: 30/05/13
 * Time: 22:27
 */
@Singleton
@Log
class LocalDBDaemon {
  private def reader = Thread.start {
    while (true) {
      def mp = MeterProperties.metersProperties
      mp.Meters.each { meter ->
        def meterData = new MeterData(meter.meter_name, meter.address)
        def data = meterData.getMeterData()
        MeasureDAO.insertMeasuration(data)
        log.info "Added new measure"
        sleep(mp.meter_delay)
      }
      sleep(1000)
    }
  }

  static start() {
    if (!LocalDBDaemon.instance.reader.isAlive())
      LocalDBDaemon.instance.reader.start()
  }

  def flushDBMeasures() {
    MeasureDAO.truncate()
  }

  def getDBMeasures() {
    reader.suspend()
    def ret = MeasureDAO.selectAllColumnsMeasuration()
    reader.resume()
    return ret
  }
}

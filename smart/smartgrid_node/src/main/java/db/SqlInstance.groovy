package db

import utils.MeterProperties

/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 5/6/13
 * Time: 7:21 PM
 */
import groovy.sql.Sql

class SqlInstance {
  private static final INSTANCE = new SqlInstance()
  private static final dbServer = "jdbc:postgresql://${MeterProperties.db.dbHost}:"
  private static final dbPort = "${MeterProperties.db.dbPort}"
  private static final db = "${MeterProperties.db.dbName}"
  private static final dbConn = dbServer + dbPort + "/" + db
  private static final dbUserName = "${MeterProperties.db.dbUserName}"
  private static final dbPassword = "${MeterProperties.db.dbPassword}"
  private static final dbDriver = "${MeterProperties.db.dbDriver}"
  private static Sql sql = Sql.newInstance(dbConn, dbUserName, dbPassword, dbDriver)

  private SqlInstance() {}

  public static Sql getSqlInstance() {
    return sql
  }
}

package utils

import groovy.json.JsonSlurper

/**
 * Created with IntelliJ IDEA.
 * User: Administrador
 * Date: 31/05/13
 * Time: 16:10
 * To change this template use File | Settings | File Templates.
 */
@Singleton
class MeterProperties {
  def properties = readProperties()

  def readProperties() {
    def properties = new File('properties.json').text
    def slupper = new JsonSlurper()
    def result = slupper.parseText(properties)
    return result
  }

  public static Map getDb() {
    def db = [
        dbName: MeterProperties.instance.properties.db,
        dbPort: MeterProperties.instance.properties.dbPort,
        dbUserName: MeterProperties.instance.properties.dbUserName,
        dbPassword: MeterProperties.instance.properties.dbPassword,
        dbDriver: MeterProperties.instance.properties.dbDriver,
        dbHost: MeterProperties.instance.properties.dbHost
    ]
    return db
  }


  public static Map getMetersProperties() {
    return MeterProperties.instance.properties
  }
}

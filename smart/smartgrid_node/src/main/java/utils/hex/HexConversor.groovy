package utils.hex

public class HexConversor {
  static final String HEX_CHARS = "0123456789ABCDEF"

  public static final ArrayList<Byte> hexStringToBytes(String hexstr) {
    if (hexstr == null) {
      return null
    }
    ArrayList bts = new ArrayList()

    for (int i = 0; i < hexstr.length(); i += 2) {
      def codedStringBuilder = new StringBuilder()
      codedStringBuilder.append("0x")
      codedStringBuilder.append(hexstr.charAt(i))
      codedStringBuilder.append(hexstr.charAt(i + 1))
      def codedString = codedStringBuilder.toString()
      bts.add(Byte.valueOf(Integer.decode(codedString).byteValue()))
    }
    return bts
  }

  public static ArrayList<Integer> bytesToInteger(ArrayList<Byte> bytes) {
    ArrayList bts = new ArrayList()
    for (int i = 0; i < bytes.size(); i++) {
      if (((Byte) bytes.get(i)).byteValue() < 0)
        bts.add(Integer.valueOf(((Byte) bytes.get(i)).intValue() + 256))
      else {
        bts.add(Integer.valueOf(((Byte) bytes.get(i)).intValue()))
      }
    }
    return bts
  }

  public static ArrayList<Integer> hexStringToIntegers(String hexstr) {
    if (hexstr == null) {
      return null
    }
    return bytesToInteger(hexStringToBytes(hexstr))
  }

  public static final int unsignedByteToInt(Byte b) {
    return b.byteValue() & 0xFF
  }

  public static final String byteToHexString(ArrayList<Byte> bytes) {
    if (bytes == null) {
      return null
    }

    StringBuilder hex = new StringBuilder()

    for (Iterator i$ = bytes.iterator(); i$.hasNext();) {
      byte i = ((Byte) i$.next()).byteValue()
      hex.append(HEX_CHARS.charAt((i & 0xF0) >> 4)).append(HEX_CHARS.charAt(i & 0xF))
    }

    return hex.toString()
  }

  public static final Byte hexCharToByte(String hexchar) {
    if (hexchar == null) {
      return null
    }
    if (hexchar.length() != 2) {
      return null
    }
    return Byte.valueOf((byte) Integer.parseInt(hexchar.substring(0, 2), 16))
  }

  public static final ArrayList<Byte> meterNumberToByte(String meternumber) {
    if ((meternumber == null) || (meternumber.length() != 6)) {
      return null
    }
    ArrayList bts = new ArrayList()
    for (int i = 0; i < 3; i++) {
      bts.add(Byte.valueOf((byte) Integer.parseInt(meternumber.substring(2 * i, 2 * i + 2), 16)))
    }

    Byte temp = (Byte) bts.get(2)
    bts.set(2, bts.get(0))
    bts.set(0, temp)
    return bts
  }

  public static final int byteToInteger(Byte hexbyte) {
    if (hexbyte == null) {
      return 0
    }
    StringBuilder hexStr = new StringBuilder()
    hexStr.append(HEX_CHARS.charAt((hexbyte.byteValue() & 0xF0) >> 4))
    hexStr.append(HEX_CHARS.charAt(hexbyte.byteValue() & 0xF))
    int val = 0
    for (int i = 0; i < hexStr.length(); i++) {
      char c = hexStr.charAt(i)
      int d = HEX_CHARS.indexOf((int) c)
      val = 16 * val + d
    }
    return val
  }

  public static final ArrayList<Hexadecimal> hexByteToDecimal(ArrayList<Integer> hexbyte) {
    if (hexbyte == null) {
      return null
    }
    ArrayList aux = hexbyte
    ArrayList retValue = new ArrayList()
    for (int i = 0; i < hexbyte.size(); i++) {
      Integer auxI = Integer.valueOf(((Integer) aux.get(i)).intValue())
      retValue.add(new Hexadecimal())
      while (auxI.intValue() > 0) {
        ((Hexadecimal) retValue.get(i)).add(Integer.valueOf(auxI.intValue() % 16))
        int a = auxI.intValue() / 16
        auxI = Integer.valueOf(a)
      }
    }
    return retValue
  }
}
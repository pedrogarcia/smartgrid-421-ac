package utils.hex

/**
 User: Pedro Garcia
 Mail: sawp@sawp.com.br
 Date: 15/04/13
 */
class Hexadecimal {
  private String num
  private static final String HEX_CHARS = "ABCDEF"

  public Hexadecimal() {
    this.num = new String()
  }

  public void add(Integer NUM) {
    if (NUM.intValue() < 10)
      this.num = NUM.toString() + this.num
    else {
      int nval = NUM.intValue() - 10
      Character c = HEX_CHARS.charAt(nval)
      this.num = (c.toString() + this.num)
    }
  }

  public String get() {
    if (this.num.length() == 0)
      return "00"
    if (this.num.length() < 2)
      return "0" + this.num
    return this.num
  }
}

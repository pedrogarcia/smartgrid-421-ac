package dao

import db.SqlInstance
import groovy.sql.Sql

import java.text.SimpleDateFormat

/**
 * User: Pedro Garcia
 * Date: 30/05/13
 * Time: 22:54
 */
@Singleton
class MeasureDAO {
  private Sql sql = SqlInstance.getSqlInstance()

  static void truncate() {
    def sql = MeasureDAO.instance.sql
    sql.execute("TRUNCATE TABLE measures CASCADE")
  }

  static ArrayList<Map> selectAllColumnsMeasuration() {
    def sql = MeasureDAO.instance.sql
    def rows = sql.rows("SELECT * FROM measures")
    return rows
  }

  static String insertMeasuration(Map meterData) {
    def sql = MeasureDAO.instance.sql
    def params = getDataValues(meterData)
    def query = """
        INSERT INTO
          measures(
            meter_name,
            address,
            "FatorDePotencia",
            "Frequencia",
            "MaximaDemandaAtivaRegistrada",
            "MaximaDemandaReativaRegistrada",
            "MeterPotenciaAtivaInstantanea",
            "PotenciaAparenteInstantanea",
            "PotenciaAtivaConsumida",
            "PotenciaReativaConsumida",
            "PotenciaReativaInstantanea",
            "PotenciaReativaReversaConsumida",
            "PotenciaReversaConsumida",
            "RCorrenteDeFase",
            "RPotenciaAtiva",
            "RPotenciaReativa",
            "RTensaoDeFase",
            "SCorrenteDeFase",
            "SPotenciaAtiva",
            "SPotenciaReativa",
            "STensaoDeFase",
            "TCorrenteDeFase",
            "TPotenciaAtiva",
            "TPotenciaReativa",
            "TTensaoDeFase",
            local_time
          )
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"""
    def result = sql.executeInsert(query, params)
    return result.toString()
  }

  private static String getUTCDateString() {
    def df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    def today = Calendar.instance.time
    def formattedDate = df.format(today)
    return formattedDate
  }

  private static ArrayList getDataValues(Map meterData) {
    def localDate = getUTCDateString()
    def list =
      [
          meterData.Name,
          meterData.Address,
          convertToFloat(meterData.Measures.FatorDePotencia),
          convertToFloat(meterData.Measures.Frequencia),
          convertToFloat(meterData.Measures.MaximaDemandaAtivaRegistrada),
          convertToFloat(meterData.Measures.MaximaDemandaReativaRegistrada),
          convertToFloat(meterData.Measures.MeterPotenciaAtivaInstantanea),
          convertToFloat(meterData.Measures.PotenciaAparenteInstantanea),
          convertToFloat(meterData.Measures.PotenciaAtivaConsumida),
          convertToFloat(meterData.Measures.PotenciaReativaConsumida),
          convertToFloat(meterData.Measures.PotenciaReativaInstantanea),
          convertToFloat(meterData.Measures.PotenciaReativaReversaConsumida),
          convertToFloat(meterData.Measures.PotenciaReversaConsumida),
          convertToFloat(meterData.Measures.RCorrenteDeFase),
          convertToFloat(meterData.Measures.RPotenciaAtiva),
          convertToFloat(meterData.Measures.RPotenciaReativa),
          convertToFloat(meterData.Measures.RTensaoDeFase),
          convertToFloat(meterData.Measures.SCorrenteDeFase),
          convertToFloat(meterData.Measures.SPotenciaAtiva),
          convertToFloat(meterData.Measures.SPotenciaReativa),
          convertToFloat(meterData.Measures.STensaoDeFase),
          convertToFloat(meterData.Measures.TCorrenteDeFase),
          convertToFloat(meterData.Measures.TPotenciaAtiva),
          convertToFloat(meterData.Measures.TPotenciaReativa),
          convertToFloat(meterData.Measures.TTensaoDeFase),
          localDate
      ]
    return list
  }

  private static Float convertToFloat(data) {
    return (data != null) ? data.toFloat() : 0.0
  }
}

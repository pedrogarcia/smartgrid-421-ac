package smartgrid.dataserver

import org.restlet.Server
import org.restlet.data.Protocol
import org.restlet.resource.Get
import org.restlet.resource.ServerResource
import org.restlet.Component
import smartgrid.dataserver.WebServices.WebService
import smartgrid.dataserver.utils.DataServerProperties

/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 6/14/13
 * Time: 4:31 PM
 */
class DataServer {
  public static void main(String[] args) {
    Component component = new Component()
    component.getServers().add(Protocol.HTTP, DataServerProperties.db.serverPort)
    component.getDefaultHost().attach("/meterinfo", WebService.class)
    component.start()
  }
}

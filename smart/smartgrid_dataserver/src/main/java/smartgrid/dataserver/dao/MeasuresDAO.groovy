package smartgrid.dataserver.dao

import smartgrid.dataserver.model.Measure

/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 6/14/13
 * Time: 12:05 PM
 */
@Singleton
class MeasuresDAO extends DAO {
  public static Measure getMeasureById(Long id) {
    def sql = MeasuresDAO.instance.sql
    def measure = sql.firstRow("""
      SELECT * FROM "Measures" WHERE id=${id}
    """)
    new Measure(
        id: measure.id,
        address: measure.Address,
        meterId: measure.Meter,
        fatorDePotencia: measure.FatorDePotencia,
        frequencia: measure.Frequencia,
        maximaDemandaAtivaRegistrada: measure.MaximaDemandaAtivaRegistrada,
        maximaDemandaReativaRegistrada: measure.MaximaDemandaReativaRegistrada,
        potenciaAtivaInstantanea: measure.MeterPotenciaAtivaInstantanea,
        potenciaAparenteInstantanea: measure.PotenciaAparenteInstantanea,
        potenciaAtivaConsumida: measure.PotenciaAtivaConsumida,
        potenciaReativaConsumida: measure.PotenciaReativaConsumida,
        potenciaReativaInstantanea: measure.PotenciaReativaInstantanea,
        potenciaReativaReversaConsumida: measure.PotenciaReativaReversaConsumida,
        potenciaReversaConsumida: measure.PotenciaReversaConsumida,
        rCorrenteDeFase: measure.RCorrenteDeFase,
        rPotenciaAtiva: measure.RPotenciaAtiva,
        rPotenciaReativa: measure.RPotenciaReativa,
        rTensaoDeFase: measure.RTensaoDeFase,
        sCorrenteDeFase: measure.SCorrenteDeFase,
        sPotenciaAtiva: measure.SPotenciaAtiva,
        sPotenciaReativa: measure.SPotenciaReativa,
        sTensaoDeFase: measure.STensaoDeFase,
        tCorrenteDeFase: measure.TCorrenteDeFase,
        tPotenciaAtiva: measure.TPotenciaAtiva,
        tPotenciaReativa: measure.TPotenciaReativa,
        tTensaoDeFase: measure.TTensaoDeFase,
        timeRegisterLocal: measure.TimeRegisterLocal,
        timeRegisterGlobal: measure.TimeRegisterGlobal
    )
  }

  public static List<Measure> getMeasuresByMeterId(Long id, Long limit = 0) {
    def sql = MeasuresDAO.instance.sql
    def nonLimitedQuery = """SELECT * FROM "Measures" WHERE "Meter"=${id} ORDER BY "TimeRegisterLocal" DESC"""
    def limitedQuery = nonLimitedQuery + " LIMIT ${limit};"
    def query = (limit == 0) ? nonLimitedQuery : limitedQuery
    def measuresAsMap = sql.rows(query)
    def measures = measuresAsMap.collect { measure ->
      new Measure(
          id: measure.id,
          address: measure.Address,
          meterId: measure.Meter,
          fatorDePotencia: measure.FatorDePotencia,
          frequencia: measure.Frequencia,
          maximaDemandaAtivaRegistrada: measure.MaximaDemandaAtivaRegistrada,
          maximaDemandaReativaRegistrada: measure.MaximaDemandaReativaRegistrada,
          potenciaAtivaInstantanea: measure.MeterPotenciaAtivaInstantanea,
          potenciaAparenteInstantanea: measure.PotenciaAparenteInstantanea,
          potenciaAtivaConsumida: measure.PotenciaAtivaConsumida,
          potenciaReativaConsumida: measure.PotenciaReativaConsumida,
          potenciaReativaInstantanea: measure.PotenciaReativaInstantanea,
          potenciaReativaReversaConsumida: measure.PotenciaReativaReversaConsumida,
          potenciaReversaConsumida: measure.PotenciaReversaConsumida,
          rCorrenteDeFase: measure.RCorrenteDeFase,
          rPotenciaAtiva: measure.RPotenciaAtiva,
          rPotenciaReativa: measure.RPotenciaReativa,
          rTensaoDeFase: measure.RTensaoDeFase,
          sCorrenteDeFase: measure.SCorrenteDeFase,
          sPotenciaAtiva: measure.SPotenciaAtiva,
          sPotenciaReativa: measure.SPotenciaReativa,
          sTensaoDeFase: measure.STensaoDeFase,
          tCorrenteDeFase: measure.TCorrenteDeFase,
          tPotenciaAtiva: measure.TPotenciaAtiva,
          tPotenciaReativa: measure.TPotenciaReativa,
          tTensaoDeFase: measure.TTensaoDeFase,
          timeRegisterLocal: measure.TimeRegisterLocal,
          timeRegisterGlobal: measure.TimeRegisterGlobal
      )
    }
    measures
  }
}

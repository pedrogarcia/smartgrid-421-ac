package smartgrid.dataserver.dao

import groovy.sql.Sql
import smartgrid.dataserver.db.SqlInstance

/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 6/14/13
 * Time: 11:53 AM
 */
abstract class DAO {
  Sql sql = SqlInstance.sqlInstance
}

package smartgrid.dataserver.dao

/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 6/14/13
 * Time: 3:10 PM
 */
@Singleton
class MetersDAO extends DAO {
  public static Long getMeterIdByNodeIPAndMeterAddress(String nodeIP, String meterAddress) {
    def sql = MetersDAO.instance.sql
    def nodeId = sql.firstRow("""SELECT id FROM "Nodes" WHERE ip=${nodeIP} """).id
    def meterId = sql.firstRow("""SELECT id FROM "Meters" WHERE "Node"=${nodeId} AND "Address"=${meterAddress} """).id
    return meterId
  }
}

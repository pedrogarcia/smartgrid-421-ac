package smartgrid.dataserver.db

import groovy.sql.Sql
import smartgrid.dataserver.utils.DataServerProperties

/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 6/14/13
 * Time: 11:45 AM
 */
@Singleton
class SqlInstance {
  private static final dbServer = "jdbc:postgresql://${DataServerProperties.db.dbHost}:"
  private static final dbPort = "${DataServerProperties.db.dbPort}"
  private static final db = "${DataServerProperties.db.dbName}"
  private static final dbConn = dbServer + dbPort + "/" + db
  private static final dbUserName = "${DataServerProperties.db.dbUserName}"
  private static final dbPassword = "${DataServerProperties.db.dbPassword}"
  private static final dbDriver = "${DataServerProperties.db.dbDriver}"
  private static Sql sql = Sql.newInstance(dbConn, dbUserName, dbPassword, dbDriver)

  private SqlInstance() {}

  static Sql getSqlInstance() {
    return sql
  }
}

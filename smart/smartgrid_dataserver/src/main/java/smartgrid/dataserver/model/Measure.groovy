package smartgrid.dataserver.model

import groovy.transform.ToString
/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 6/14/13
 * Time: 12:23 PM
 */
@ToString(includeNames=true)
class Measure {
  Integer id
  String address
  Integer meterId
  Float fatorDePotencia
  Float frequencia
  Float maximaDemandaAtivaRegistrada
  Float maximaDemandaReativaRegistrada
  Float potenciaAtivaInstantanea
  Float potenciaAparenteInstantanea
  Float potenciaAtivaConsumida
  Float potenciaReativaConsumida
  Float potenciaReativaInstantanea
  Float potenciaReativaReversaConsumida
  Float potenciaReversaConsumida
  Float rCorrenteDeFase
  Float rPotenciaAtiva
  Float rPotenciaReativa
  Float rTensaoDeFase
  Float sCorrenteDeFase
  Float sPotenciaAtiva
  Float sPotenciaReativa
  Float sTensaoDeFase
  Float tCorrenteDeFase
  Float tPotenciaAtiva
  Float tPotenciaReativa
  Float tTensaoDeFase
  String timeRegisterLocal
  String timeRegisterGlobal
}

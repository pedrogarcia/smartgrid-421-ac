package smartgrid.dataserver.model

import groovy.transform.ToString

import java.text.SimpleDateFormat

/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 6/14/13
 * Time: 7:09 PM
 */
@ToString(includeNames = true)
class MeasureInfo {
  List<Float> fatorDePotencia = []
  List<Float> frequencia = []
  List<Float> maximaDemandaAtivaRegistrada = []
  List<Float> maximaDemandaReativaRegistrada = []
  List<Float> potenciaAtivaInstantanea = []
  List<Float> potenciaAparenteInstantanea = []
  List<Float> potenciaAtivaConsumida = []
  List<Float> potenciaReativaConsumida = []
  List<Float> potenciaReativaInstantanea = []
  List<Float> potenciaReativaReversaConsumida = []
  List<Float> potenciaReversaConsumida = []
  List<Float> rCorrenteDeFase = []
  List<Float> rPotenciaAtiva = []
  List<Float> rPotenciaReativa = []
  List<Float> rTensaoDeFase = []
  List<Float> sCorrenteDeFase = []
  List<Float> sPotenciaAtiva = []
  List<Float> sPotenciaReativa = []
  List<Float> sTensaoDeFase = []
  List<Float> tCorrenteDeFase = []
  List<Float> tPotenciaAtiva = []
  List<Float> tPotenciaReativa = []
  List<Float> tTensaoDeFase = []
  List<String> data = []
  List<String> hora = []

  MeasureInfo(List<Measure> measures) {
    measures.each {
      addMeasure(it)
    }
  }

  private void addMeasure(Measure measure) {
    this.fatorDePotencia.add(measure.fatorDePotencia)
    this.frequencia.add(measure.frequencia)
    this.maximaDemandaAtivaRegistrada.add(measure.maximaDemandaAtivaRegistrada)
    this.maximaDemandaReativaRegistrada.add(measure.maximaDemandaReativaRegistrada)
    this.potenciaAtivaInstantanea.add(measure.potenciaAtivaInstantanea)
    this.potenciaAparenteInstantanea.add(measure.potenciaAparenteInstantanea)
    this.potenciaReativaConsumida.add(measure.potenciaReativaConsumida)
    this.potenciaAtivaConsumida.add(measure.potenciaAtivaConsumida)
    this.potenciaReativaInstantanea.add(measure.potenciaReativaInstantanea)
    this.potenciaReativaReversaConsumida.add(measure.potenciaReativaReversaConsumida)
    this.potenciaReversaConsumida.add(measure.potenciaReversaConsumida)
    this.rCorrenteDeFase.add(measure.rCorrenteDeFase)
    this.rPotenciaAtiva.add(measure.rPotenciaAtiva)
    this.rPotenciaReativa.add(measure.rPotenciaReativa)
    this.rTensaoDeFase.add(measure.rTensaoDeFase)
    this.sCorrenteDeFase.add(measure.sCorrenteDeFase)
    this.sPotenciaAtiva.add(measure.sPotenciaAtiva)
    this.sPotenciaReativa.add(measure.sPotenciaReativa)
    this.sTensaoDeFase.add(measure.sTensaoDeFase)
    this.tCorrenteDeFase.add(measure.tCorrenteDeFase)
    this.tPotenciaAtiva.add(measure.tPotenciaAtiva)
    this.tPotenciaReativa.add(measure.tPotenciaReativa)
    this.tTensaoDeFase.add(measure.tTensaoDeFase)
    def date = extractDateTime(measure.timeRegisterLocal)
    this.data.add(date.date)
    this.hora.add(date.hour)
  }

  private Map extractDateTime(String date) {
    Date d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date)
    def day = d.getAt(Calendar.DAY_OF_MONTH)
    def month = d.getAt(Calendar.MONTH)
    def year = d.getAt(Calendar.YEAR)
    def hour = d.getAt(Calendar.HOUR)
    def minute = d.getAt(Calendar.MINUTE)
    def fmt = { x -> (x <= 9) ? "0${x}" : "${x}" }
    def fmtDate = fmt(day) + "/" + fmt(month) + "/" + fmt(year)
    def fmtHour = fmt(hour) + ":" + fmt(minute)
    [date: fmtDate, hour: fmtHour]
  }
}

package smartgrid.dataserver.WebServices

import groovy.json.JsonOutput
import org.restlet.representation.Representation
import org.restlet.resource.Get
import org.restlet.resource.Put
import org.restlet.resource.ServerResource
import smartgrid.dataserver.dao.MeasuresDAO
import smartgrid.dataserver.dao.MetersDAO
import smartgrid.dataserver.model.MeasureInfo
import groovy.util.logging.Log

/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 6/14/13
 * Time: 4:21 PM
 */
@Log
class WebService extends ServerResource {
  @Get("json")
  public String showMeterMeasures() {
    def urlQuery = getQuery()
    def nodeHost = urlQuery.getValues("host")
    def meterAddress = urlQuery.getValues("meteraddress")
    def limit = urlQuery.getValues("limit")
    limit = (limit == null) ? 0 : limit.toLong()
    def meterId = MetersDAO.getMeterIdByNodeIPAndMeterAddress(nodeHost, meterAddress)
    def measures = MeasuresDAO.getMeasuresByMeterId(meterId, limit)
    def measuresInfo = new MeasureInfo(measures)
    def json = JsonOutput.prettyPrint(JsonOutput.toJson(measuresInfo))
    log.info "WebService: request send"
    return json
  }
}

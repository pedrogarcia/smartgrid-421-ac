package smartgrid.dataserver.utils

import groovy.json.JsonSlurper

/**
 * User: Pedro Garcia <sawp@sawp.com.br>
 * Date: 6/14/13
 * Time: 11:37 AM
 */
@Singleton
class DataServerProperties {
  def properties = readProperties()

  def readProperties() {
    def fileProperties = new File('properties.json').text
    return new JsonSlurper().parseText(fileProperties)
  }

  public static Map getDb() {
    def db = [
        dbName: DataServerProperties.instance.properties.db,
        dbPort: DataServerProperties.instance.properties.dbPort,
        dbUserName: DataServerProperties.instance.properties.dbUserName,
        dbPassword: DataServerProperties.instance.properties.dbPassword,
        dbDriver: DataServerProperties.instance.properties.dbDriver,
        dbHost: DataServerProperties.instance.properties.dbHost,
        serverPort: DataServerProperties.instance.properties.serverPort
    ]
    return db
  }
}
